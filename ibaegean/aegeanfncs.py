import subprocess
import os
from pprint import pprint

def aegean_command(inImage, pars=None):
    if pars is None:
        pars = {}
    aegcommand = [
        "aegean",
        inImage
        ]
    for par, val in pars.items():
        aegcommand.append('{}={}'.format(par, val))

    return aegcommand

def bane_command(inImage, pars=None):
    if pars is None:
        pars = {}
    banecommand = [
        "BANE",
        inImage
        ]
    for par, val in pars.items():
        banecommand.append('{}={}'.format(par, val))

    return banecommand

def mimas_command(outfile, pars=None):
    if pars is None:
        pars = {}
    mimascommand = [
        "MIMAS",
        "-o",
        outfile
        ]
    for par, val in pars.items():
        mimascommand.append(par)
        if not isinstance(val, list):
            val = [val]
        mimascommand.extend(val)

    return mimascommand


def aeres_command(catalogue_file, fits_file, residual_file, pars=None):
    if pars is None:
        pars = {}
    aerescommand = [
        "AeRes",
        "-c",
        catalogue_file,
        "-f",
        fits_file,
        "-r",
        residual_file
        ]
    for par, val in pars.items():
        aerescommand.append('{} {}'.format(par, val))

    return aerescommand

    # TODO check output


def run_command(command, output_file=None, timeout=20):
    subprocess.check_output(command, timeout=timeout)
    if output_file is not None:
        if not os.path.exists(output_file):
            return False
    return True
