from reproject import reproject_interp
from astropy.io import fits
from astropy.stats import SigmaClip
from photutils import StdBackgroundRMS
import shutil
from astropy.io import fits
from astropy import wcs
from astropy.nddata import Cutout2D
from astropy.coordinates import SkyCoord
from astropy import units as u


import re
import math
from ibcommon.strings import id_generator
# from qb_common.strings import id_generator
from scipy import stats

import operator
import os
import traceback
# from shutil import rmtree
from shutil import copyfile

# import MySQLdb

# import astrolib.coords as coords
import numpy as np
import scipy.ndimage
import matplotlib.pyplot as plt
from regions import read_ds9, write_ds9
# np.set_printoptions(threshold=np.nan)
from photutils.psf import IterativelySubtractedPSFPhotometry
from photutils.psf import IntegratedGaussianPRF, DAOGroup
from astropy.stats import gaussian_sigma_to_fwhm
from astropy.table import Table
from photutils.psf import (IterativelySubtractedPSFPhotometry,
                           BasicPSFPhotometry)
from astropy.modeling.fitting import LevMarLSQFitter
from photutils.background import MMMBackground, MADStdBackgroundRMS
from astropy.wcs import WCS
from astropy.wcs.utils import skycoord_to_pixel
from astropy.stats import SigmaClip
from photutils import MedianBackground
# import qb_common.mysqls as qbmysqls
from ibastro.coordtrans import pix2coord, coord2pix, natural_coords
from astropy.io.fits import getheader, getdata, writeto, setval, PrimaryHDU, getval
# from pyraf import iraf
from scipy import spatial
from scipy.stats import norm

from regions import DS9Parser

# from scipy import stats
# import montage_wrapper as montage
# from montages import rebinImage

__author__ = "ibojicic"
__date__ = "$Oct 26, 2015 2:52:09 PM$"

def mask_circle(inimage, outimage, xpix, ypix, radpix, newval, reverse_axis=False):
    hdu = fits.open(inimage)
    data = hdu[0].data
    xaxis = np.arange(0,data.shape[0])
    yaxis = np.arange(0,data.shape[1])
    if reverse_axis:
        xaxis = np.arange(0,data.shape[1])
        yaxis = np.arange(0,data.shape[0])
    mask = (xaxis[np.newaxis, :] - xpix) ** 2 + (yaxis[:, np.newaxis] - ypix) ** 2 < radpix ** 2
    hdu[0].data[mask] = newval

    hdu.writeto(outimage, overwrite=True)


def mask_fromds9(inimage, ds9_file, outimage, newval, cdelt1 = 'CD1_1', cdelt2 = 'CD2_2', reverse_axis=False):
    regions = read_ds9(ds9_file)
    w = WCS(inimage)

    pixsize = getHeaderItems(inimage, [cdelt1,cdelt2])

    tempimage = "starextracttemp.fits"

    for region in regions:
        radius = region.radius.value

        radpix = (radius / 3600)/np.sqrt(abs(pixsize[cdelt1] * pixsize[cdelt2]))

        coords_radec = region.center.fk5
        coords_pix = skycoord_to_pixel(coords_radec, w)
        xpix = int(coords_pix[0])
        ypix = int(coords_pix[1])

        mask_circle(inimage, tempimage, xpix, ypix, radpix, newval, reverse_axis)
        inimage = tempimage
    shutil.move(tempimage,outimage)

# TODO
def remove_star(inimage, ds9_file, outimage):
    regions = read_ds9(ds9_file)
    w = WCS(inimage)

    tempimage = "starextracttemp.fits"

    for region in regions:
        radius = region.radius
        coords_radec = region.center.fk5
        coords_pix = skycoord_to_pixel(coords_radec, w)

        sigma_psf = 2.0
        # mmm_bkg = MMMBackground()
        sigma_clip = SigmaClip(sigma=1)
        mmm_bkg = MedianBackground(sigma_clip)

        daogroup = DAOGroup(2.0 * sigma_psf * gaussian_sigma_to_fwhm)

        psf_model = IntegratedGaussianPRF(sigma=sigma_psf)

        psf_model.x_0.fixed = True
        psf_model.y_0.fixed = True

        pos = Table(names=['x_0', 'y_0'], data=[[int(coords_pix[0])], [int(coords_pix[1])]])

        photometry = BasicPSFPhotometry(group_maker=daogroup,
                                        bkg_estimator = mmm_bkg,
                                        psf_model = psf_model,
                                        fitter = LevMarLSQFitter(),
                                        fitshape = (11, 11)
                                        )
        hdu = fits.open(inimage)
        data = hdu[0].data

        result_tab = photometry(image=data, init_guesses=pos)
        hdu[0].data = photometry.get_residual_image()
        hdu.writeto(tempimage, overwrite=True)
        inimage = tempimage

    shutil.move(tempimage,outimage)

def get_images_data(image_list):
    if len(image_list) == 0:
        return False
    images_data = [getdata(image) for image in image_list]
    return images_data


def quotientImage(image_list, out_image, scale_to_noise = True):
    final_header = getheader(image_list[0],0)
    concat_images = get_images_data(image_list)

    if scale_to_noise:
        concat_images = scaleToNoise(concat_images)

    final_image = concat_images[0]
    concat_images.pop(0)
    for image in concat_images:
        final_image = final_image / image


    hdu = PrimaryHDU(final_image,final_header)
    hdu.writeto(out_image, overwrite=True)


def scaleToNoise(images_data):
    result = []
    result.append(images_data[0])
    images_data.pop(0)
    for image in images_data:
        ratio_image = image / result[0]
        ratio_image = ratio_image.flatten()
        ratio_image = np.ma.array(ratio_image,mask=np.isnan(ratio_image))
        mu = np.nanmean(ratio_image)
        result.append(mu * image)

    return result

def spindexImage(image_list, freq_list, out_image, image_factors = None):
    final_header = getheader(image_list[0],0)
    concat_images = get_images_data(image_list)

    # concat_images = scaleToNoise(concat_images)

    spindex_images = []
    for indx1, image1 in enumerate(concat_images):
        for indx2, image2 in enumerate(concat_images):
            if indx1 == indx2 or indx1 > indx2:
                continue
            ratio_image = np.where(image1 > 0, image1, np.nan) /  np.where(image2 > 0, image2, np.nan)
            spindex_images.append(np.log10(ratio_image) / math.log10(freq_list[indx1]/freq_list[indx2]))


    final_image = np.zeros(shape=spindex_images[0].shape)
    for image in spindex_images:
        final_image += image

    final_image = final_image / len(spindex_images)

    hdu = PrimaryHDU(final_image,final_header)
    hdu.writeto(out_image, overwrite=True)

def add_beam_pars(infile, beam_pars = None):
    if beam_pars is None:
        beam_pars = get_vla_beam_pars(infile)
    updateHeader(infile,beam_pars)


# get beam parameters from NVAS images
def get_vla_beam_pars(infile):
    hist_part = getHeaderItems(infile, ['HISTORY'])
    try:
        beam_pars = {
            'BMIN':float(re.search("(?<=BMIN=)\s+[+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?", str(hist_part['HISTORY'])).group(0)),
            'BMAJ':float(re.search("(?<=BMAJ=)\s+[+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?", str(hist_part['HISTORY'])).group(0)),
            'BPA':float(re.search("(?<=BPA=)\s+[+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?", str(hist_part['HISTORY'])).group(0))
        }
    except AttributeError:
        beam_pars = {
            'BMAJ':None,
            'BMIN':None,
            'BPA':None
            }
    return beam_pars


def delete_extra_header(inImage):
    delitems = ['CTYPE3', 'CRVAL3', 'CRPIX3', 'CDELT3', 'CUNIT3',
                'CTYPE4', 'CRVAL4', 'CRPIX4', 'CDELT4', 'CUNIT4',
                'NAXIS3', 'NAXIS4', 'CROTA3', 'CROTA4',
                'PC3_1', 'PC4_1', 'PC3_2', 'PC4_2', 'PC1_3', 'PC2_3',
                'PC3_3', 'PC4_3', 'PC1_4', 'PC2_4', 'PC3_4', 'PC4_4',
                'PC03_01', 'PC04_01', 'PC03_02', 'PC04_02', 'PC01_03', 'PC02_03',
                'PC03_03', 'PC04_03', 'PC01_04', 'PC02_04', 'PC03_04', 'PC04_04']

    delete_header_item(inImage, delitems)


def askap_delete_extra_header(inImage, outImage = None, hdrNo=0):
    delitems = ['CTYPE3', 'CRVAL3', 'CRPIX3', 'CDELT3', 'CUNIT3',
                'CTYPE4', 'CRVAL4', 'CRPIX4', 'CDELT4', 'CUNIT4',
                'NAXIS3', 'NAXIS4', 'CROTA3', 'CROTA4',
                'PC3_1', 'PC4_1', 'PC3_2', 'PC4_2', 'PC1_3', 'PC2_3',
                'PC3_3', 'PC4_3', 'PC1_4', 'PC2_4', 'PC3_4', 'PC4_4',
                'PC03_01', 'PC04_01', 'PC03_02', 'PC04_02', 'PC01_03', 'PC02_03',
                'PC03_03', 'PC04_03', 'PC01_04', 'PC02_04', 'PC03_04', 'PC04_04']
                #'PV2_1','PV2_2',
                #'PC1_1','PC1_2','PC2_1','PC2_2']
    hdu = fits.open(inImage)
    header = hdu[hdrNo].header
    data = hdu[hdrNo].data
    for item in delitems:
        try:
            del header[item]
        except:
            print("item ", item, ' not in the header')
    header['NAXIS'] = 2

    data = data[0,0,:,:]
    hdu_new = fits.PrimaryHDU(data)
    hdul = fits.HDUList([hdu_new])
    hdul[0].header = header

    if outImage is None:
        hdul.writeto(inImage, overwrite=True)
    else:
        hdul.writeto(outImage, overwrite=True)


def reprojectOnTemplate(inImage, outImage, templateImage):
    """
    Reproject one image based on image template
    :param templateImage:
    :param inImage: full path to the input image (fits)
    :param outImage: full path to the output image (fits)
    """
    hdu1 = fits.open(templateImage)[0]
    hdu2 = fits.open(inImage)[0]
    array, footprint = reproject_interp(hdu2, hdu1.header)
    fits.writeto(outImage, array, hdu1.header, clobber=True)
    return True


def doSubtraction(leftimage, rightmage, subimage, ratio=-1, lefthdrNo=0, righthdrNo=0, mindist=4, alignment=False):
    rnd = id_generator()
    ## TODO create temp files to be destroyed in case script dies
    temp_left = "/tmp/temp_left" + rnd + ".fits"
    temp_right = "/tmp/temp_right" + rnd + ".fits"

    if alignment:
        align_images(rightmage, leftimage, temp_right, temp_left)
    else:
        shiftto0header(leftimage, temp_left, lefthdrNo)
        shiftto0header(rightmage, temp_right, righthdrNo)

    tempimage = "/tmp/tmprc" + rnd + ".fits"
    temp_sub = "/tmp/temp_sub" + rnd + ".fits"

    if os.path.exists(tempimage):
        os.remove(tempimage)
    if os.path.exists(temp_sub):
        os.remove(temp_sub)

    if ratio == -1:
        ratios = findRatioForSubtracion(temp_left, temp_right, mindist)
        ratio = ratios['AVG']

    correctImageByFactor(temp_right, tempimage, ratio)
    contsubstrImage(temp_left, tempimage, subimage, 'y')

    if os.path.exists(tempimage):
        os.remove(tempimage)
    if os.path.exists(temp_left):
        os.remove(temp_left)
    if os.path.exists(temp_right):
        os.remove(temp_right)

    return True

def shiftto0header(inImage, outImage, hdrNo):
    hdr_new = getheader(inImage, hdrNo)
    data_new = getdata(inImage, hdrNo)
    writeto(outImage, data_new, hdr_new, overwrite=True)
    return

def getCubeSlice(inImage, outImage, sliceNo, hdrNo=0):
    hdr_new = getheader(inImage, hdrNo)
    data_cube = getdata(inImage, hdrNo)
    data_new = data_cube[:,sliceNo,:,:]
    writeto(outImage, data_new[0], hdr_new, overwrite=True)
    return

def valueAtCoord(inImage, X, Y):
    hdu = fits.open(inImage)
    data = hdu[0].data
    return data[:, Y, X]
    # return data[Y, X]

def imageStats(inImage, overback=None):
    hdu = fits.open(inImage)
    data = hdu[0].data

    nan_array = np.isnan(data)
    not_nan_array = ~ nan_array
    non_nan_data = data[not_nan_array]

    sigma_clip = SigmaClip(sigma=3.0)
    bkgrms = StdBackgroundRMS(sigma_clip)

    background = bkgrms.calc_background_rms(non_nan_data)

    if overback is not None:
        non_nan_data =  non_nan_data[non_nan_data>overback*background]

    res = {
        'min':np.min(non_nan_data),
        'max':np.max(non_nan_data),
        'mean':np.mean(non_nan_data),
        'std':np.std(non_nan_data),
        'bkgrms':background,
        'perc1': np.percentile(non_nan_data, 1),
        'perc5': np.percentile(non_nan_data, 5),
        'perc10': np.percentile(non_nan_data, 10),
        'perc25':np.percentile(non_nan_data,25),
        'perc50':np.percentile(non_nan_data,50),
        'perc75':np.percentile(non_nan_data,75),
        'perc90':np.percentile(non_nan_data,90),
        'perc95':np.percentile(non_nan_data,95),
        'perc99':np.percentile(non_nan_data,99),
        'perc99.5':np.percentile(non_nan_data,99.5)
    }
    return res


def updateHeader(inImage, newvals):
    hdu = fits.open(inImage)
    header = hdu[0].header
    for key, val in newvals.items():
        if val is not None:
            header[key] = val
    hdu.writeto(inImage, overwrite=True)

def getHeaderItems(inImage, items, hdrNo=0):
    result = {}
    for key in items:
        try:
            value = getval(inImage,key,hdrNo)
        except KeyError:
            value = None
        result[key] = value
    return result

def delete_header_item(inImage, hdrKey, outImage = None, hdrNo = 0):
    hdu = fits.open(inImage)
    header = hdu[hdrNo].header
    for item in hdrKey:
        try:
            del header[item]
        except:
            print("item ", item, ' not in the header')
    if outImage is None:
        hdu.writeto(inImage, overwrite=True)
    else:
        hdu.writeto(outImage, overwrite=True)

def fits_projection(fileName,x_start,y_start,x_end,y_end):

    hdu1 = fits.open(fileName)
    data = hdu1[0].data

    naxis = getHeaderItems(fileName,['NAXIS1','NAXIS2'])

    x, y = np.mgrid[0:naxis['NAXIS2']:1, 0:naxis['NAXIS1']:1]
    data_xy = data[0,0]

    # Coordinates of the line we'd like to sample along
    line = [(x_start, y_start), (x_end, y_end)]

    # Convert the line to pixel/index coordinates
    x_world, y_world = np.array(zip(*line))
    col = data_xy.shape[1] * (x_world - x.min()) / x.ptp()
    row = data_xy.shape[0] * (y_world - y.min()) / y.ptp()

    # Interpolate the line at "num" points...
    num = 1000
    row, col = [np.linspace(item[0], item[1], num) for item in [row, col]]

    # Extract the values along the line, using cubic interpolation
    zi = scipy.ndimage.map_coordinates(data_xy, np.vstack((row, col)), order=1)

    return zi

def random_on_model(inImage, outImage, mean, std, hdrNo = 0):
    hdr_old = getheader(inImage, hdrNo)
    data_old= getdata(inImage, hdrNo)
    data_new = np.random.normal(mean, std, data_old.shape)
    writeto(outImage, data_new, hdr_old)

def FITScutout_astropy(fitsfile, outfile, RA, DEC, xsize, ysize):
    position = SkyCoord(RA, DEC, frame='icrs', unit='deg')
    hdu = fits.open(fitsfile)
    w = wcs.WCS(hdu[0].header)
    size = u.Quantity((xsize, ysize), u.arcsec)
    cutout = Cutout2D(hdu[0].data, position, size, wcs=w)
    hdu[0].data = cutout.data
    hdu[0].header.update(cutout.wcs.to_header())
    hdu[0].writeto(outfile, overwrite=True)


def checkValidVal(inImage, X, Y, minval, maxval, boxsize=3):
    return False

# def checkValidVal(inImage, X, Y, minval, maxval, boxsize=3):
#     box = range(0 - boxsize, 1 + boxsize)
#     totpix = 0
#     try:
#         for xtmp in box:
#             for ytmp in box:
#                 listline = "[%s:%s,%s:%s]" % (int(X + xtmp), int(X + xtmp), int(Y + ytmp), int(Y + ytmp))
#                 checkpixtmp = iraf.listpix(inImage + listline, Stdout=1)
#                 checkpix = checkpixtmp[0].split()
#                 totpix += float(checkpix[1])
#         result = (minval < (totpix / pow(2 * boxsize + 1, 2)) < maxval) and totpix != 0.0
#     except Exception:
#         # traceback.print_exc()  # print full exception
#         result = False
#     return result

def align_images(inImage_1, inImage_2, outImage_1, outImage_2):
    return False

# def align_images(inImage_1, inImage_2, outImage_1, outImage_2):
#     rnd_1 = id_generator()
#     rnd_2 = id_generator()
#
#     corr_image_1 = "/tmp/corr_image" + rnd_1 + ".fits"
#     corr_image_2 = "/tmp/corr_image" + rnd_2 + ".fits"
#     try:
#         shiftto0header(inImage_1, corr_image_1, 1)
#         shiftto0header(inImage_2, corr_image_2, 1)
#     except:
#         corr_image_1 = inImage_1
#         corr_image_2 = inImage_2
#
#     blank_image_1 = "/tmp/temp_image" + rnd_1 + ".fits"
#     blank_image_2 = "/tmp/temp_image" + rnd_2 + ".fits"
#
#     iraf.imarith(operand1=corr_image_1, operand2=0, op='*', result=blank_image_1)
#     iraf.imarith(operand1=corr_image_2, operand2=0, op='*', result=blank_image_2)
#
#     set_1 = "{},{}".format(corr_image_1, blank_image_2)
#     set_2 = "{},{}".format(corr_image_2, blank_image_1)
#
#     iraf.imcombine(set_1, outImage_1, combine='sum', offset='wcs')
#     iraf.imcombine(set_2, outImage_2, combine='sum', offset='wcs')
#
#     if os.path.exists(corr_image_1):
#         os.remove(corr_image_1)
#     if os.path.exists(corr_image_2):
#         os.remove(corr_image_2)
#
#     if os.path.exists(blank_image_1):
#         os.remove(blank_image_1)
#     if os.path.exists(blank_image_2):
#         os.remove(blank_image_2)



def findRatioForSubtracion(haimage, rimage, mindist=4.):
    return False
# def findRatioForSubtracion(haimage, rimage, mindist=4.):
#     ha_sex = runSex(haimage)
#     r_sex = runSex(rimage)
#
#     average_array = np.array([])
#     positions_array = []
#
#     ha_positions = [[ha.get('X_IMAGE'), ha.get('Y_IMAGE')] for ha in ha_sex]
#     r_positions = [[r.get('X_IMAGE'), r.get('Y_IMAGE')] for r in r_sex]
#     results = spatial.KDTree(ha_positions).query_ball_tree(spatial.KDTree(r_positions), mindist)
#
#     for key, ha_res in enumerate(ha_sex):
#         if results[key]:
#             for r_res_key in results[key]:
#                 r_res = r_sex[r_res_key]
#                 try:
#                     ratio = ha_res['FLUX_BEST'] / r_res['FLUX_BEST']
#                     average_array = np.insert(average_array, 0, ratio)
#                     pair = {'ha': ha_res, 'r': r_res}
#                     # makeKernel(haimage,rimage,pair)
#                     positions_array.append(pair)
#                 except Exception:
#                     continue
#
#     avg = np.average(average_array)
#     med = np.median(average_array)
#     std = np.std(average_array)
#
#     return {'AVG': avg, 'STD': std, 'MED': med, 'positions': positions_array}

def correctImageByFactor(inImage, outImage, factor, hdrNo=0, operator = "*"):
    return False

# def correctImageByFactor(inImage, outImage, factor, hdrNo=0, operator = "*"):
#     rnd = id_generator()
#     temp_image = "/tmp/temp_image" + rnd + ".fits"
#     if hdrNo != 0:
#         shiftto0header(inImage, temp_image, hdrNo)
#     else:
#         os.rename(inImage,temp_image)
#
#     if os.path.exists(outImage):
#         os.remove(outImage)
#     iraf.imarith(operand1=temp_image, operand2=factor, op=operator, result=outImage)
#     if os.path.exists(temp_image):
#         os.remove(temp_image)
#
#     return

def contsubstrImage(left, right, result, allowshift='y', lefthdrNo=0, righthdrNo=0):
    return False

# def contsubstrImage(left, right, result, allowshift='y', lefthdrNo=0, righthdrNo=0):
#     if os.path.exists(result):
#         os.remove(result)
#
#     hdr_dvd = getheader(left, lefthdrNo)
#     maxX_dvd = int(hdr_dvd['NAXIS1'])
#     maxY_dvd = int(hdr_dvd['NAXIS2'])
#
#     hdr_dvs = getheader(right, righthdrNo)
#     maxX_dvs = int(hdr_dvs['NAXIS1'])
#     maxY_dvs = int(hdr_dvs['NAXIS2'])
#
#     if maxX_dvd == maxX_dvs and maxY_dvd == maxY_dvs:
#         iraf.imarith(operand1=left, operand2=right, op='-', result=result)
#     elif allowshift == 'y':
#         maxX = min(maxX_dvd, maxX_dvs)
#         maxY = min(maxY_dvd, maxY_dvs)
#         cutout = '[1:%s,1:%s]' % (maxX, maxY)
#         iraf.imarith(operand1=left + cutout, operand2=right + cutout, op='-', result=result)
#     return

# def FITScutout(inImage, outImage, Xcoord, Ycoord, box_x_coord, box_y_coord = None, incoords='', hdrNo=0, extracomm="",
#                epoch="J2000"):
#     return False
# input Xcoord and Ycoord RA (degrees) and DEC (degrees)
# boxCoords in arcsec



def FITScutout_iraf(inImage, outImage, Xcoord, Ycoord, box_x_coord, box_y_coord = None, incoords='', hdrNo=0, extracomm="",
               epoch="J2000"):
    return True
    # if os.path.exists(outImage):
    #     os.remove(outImage)
    # if box_y_coord is None:
    #     box_y_coord = box_x_coord
    #
    # if incoords == '':
    #
    #     hdr = getheader(inImage, hdrNo)
    #     max_x = int(hdr['NAXIS1'])
    #     max_y = int(hdr['NAXIS2'])
    #
    #     pos_x, pos_y = natural_coords(inImage, Xcoord, Ycoord)
    #
    #     #TODO
    #     # if epoch == "1950":
    #     #     temp_coord = coords.Position((pos_x, pos_y))  # ,equinox='b1950')
    #     #     calc_coord = temp_coord.b1950()
    #     #     pos_x = calc_coord[0]
    #     #     pos_y = calc_coord[1]
    #
    #     temp_offset_x_1, temp_offset_y_1 = coord2pix(inImage, pos_x + float(box_x_coord) / 3600., pos_y)
    #     temp_offset_x_2, temp_offset_y_2 = coord2pix(inImage, pos_x, float(box_y_coord) / 3600. + pos_y)
    #
    #     coord_x_center, coord_y_center = coord2pix(inImage, pos_x, pos_y)
    #
    #     if extracomm == 'msxcheckoffset':
    #         if coord_x_center > 216000:
    #             coord_x_center = coord_x_center - 216000
    #
    #     coord_offset = max(abs(coord_x_center - temp_offset_x_1), abs(coord_y_center - temp_offset_y_1),
    #                        abs(coord_x_center - temp_offset_x_2), abs(coord_y_center - temp_offset_y_2))
    #
    #     coord_x_min = coord_x_center - coord_offset
    #     coord_y_min = coord_y_center - coord_offset
    #
    #     coord_x_max = coord_x_center + coord_offset
    #     coord_y_max = coord_y_center + coord_offset
    #
    #     if coord_x_center < 0 or coord_x_center > max_x or coord_y_center < 0 or coord_y_center > max_y:
    #         return 0
    #
    #     if coord_x_max >= max_x:
    #         coord_x_max = max_x - 1
    #     if coord_y_max >= max_y:
    #         coord_y_max = max_y - 1
    #     if coord_x_min <= 0:
    #         coord_x_min = 1
    #     if coord_y_min <= 0:
    #         coord_y_min = 1
    #
    #     imcopy_coords = '[{}:{},{}:{}]'.format(int(coord_x_min), int(coord_x_max), int(coord_y_min), int(coord_y_max))
    # else:
    #     imcopy_coords = incoords
    #
    # imcopy_line = inImage + imcopy_coords
    # try:
    #     iraf.imcopy(imcopy_line, outImage, verbose="no")
    #     new_hdr = getheader(outImage, hdrNo)
    #     if 'CRVAL1' not in new_hdr:
    #         iraf.hedit(outImage, fields='CRVAL1', value=0., add='yes', verify='no')
    #     if 'CRVAL2' not in new_hdr:
    #         iraf.hedit(outImage, fields='CRVAL2', value=0., add='yes', verify='no')
    # except:
    #     traceback.print_exc()  # print full exception
    #     print('Prooblem')
    #     exit()
    #
    # return imcopy_coords




def findPercentile(inImage, levels, dataset='all'):
    return False
# def findPercentile(inImage, levels, dataset='all'):
#     result = {}
#     data_point = 2
#     if dataset == 'nvss': data_point = 4
#     try:
#         A = iraf.listpixels(inImage, Stdout=1)
#         B = [float(i.split()[data_point]) for i in A if
#              (float(levels['v']['min']) < float(i.split()[data_point]) < float(
#                  levels['v']['max']))]
#         B.sort()
#         result['min'] = stats.scoreatpercentile(B, float(levels['r']['min']))
#         result['max'] = stats.scoreatpercentile(B, float(levels['r']['max']))
#         result['max_amp'] = max(B)
#         result['min_amp'] = min(B)
#         result['n_points'] = len(B)
#         result['description'] = stats.describe(B)
#     except Exception as e:
#         return False
#
#     return result

def correct_header(inImage, tmpImage, hdrKey, hdrVal, hdrNo = 0, hdrDel = False):
    return False
# def correct_header(inImage, tmpImage, hdrKey, hdrVal, hdrNo = 0, hdrDel = False):
#     imHeader = getheader(inImage, hdrNo)
#
#     if hdrKey in imHeader:
#         copyfile(inImage,tmpImage)
#         iraf.hedit(tmpImage, fields= hdrKey, value=hdrVal, add='yes', verify='no')
#         return tmpImage
#     return inImage
#
# def delete_header_item(inImage, tmpImage, hdrKey, hdrVal = None, hdrNo = 0, hdrDel = False):
#     imHeader = getheader(inImage, hdrNo)
#     if hdrKey in imHeader:
#         copyfile(inImage,tmpImage)
#         iraf.hedit(tmpImage, fields= hdrKey, delete='yes', verify='no')
#         copyfile(tmpImage,inImage)
#         os.remove(tmpImage)
#     return True



#
#
#
#
# # def regridMIR(inimage_reg, inimage_mod, outimage):
# #
# #     from mirpy import miriad
# #     tmp_reg = "/tmp/" + id_generator() + 'tmpreg.mir'
# #     tmp_mod = "/tmp/" + id_generator() + 'tmpmod.mir'
# #     tmp_out = "/tmp/" + id_generator() + 'tmpout.mir'
# #
# #     miriad.fits(IN=inimage_reg, out=tmp_reg, op='xyin')
# #     miriad.fits(IN=inimage_mod, out=tmp_mod, op='xyin')
# #     miriad.regrid(IN=tmp_reg, axes='1,2', out=tmp_out, tin=tmp_mod)
# #     miriad.fits(IN=tmp_out, out=outimage, op='xyout')
# #
# #     if (os.path.exists(tmp_reg)): rmtree(tmp_reg)
# #     if (os.path.exists(tmp_mod)): rmtree(tmp_mod)
# #     if (os.path.exists(tmp_out)): rmtree(tmp_out)
#
#
# def reprojectMIR(inimage_reg, outimage):
#     from mirpy import miriad
#
#     tmp_reg = "/tmp/" + id_generator() + 'tmpreg.mir'
#     tmp_out = "/tmp/" + id_generator() + 'tmpout.mir'
#     miriad.fits(IN=inimage_reg, out=tmp_reg, op='xyin')
#     miriad.regrid(IN=tmp_reg, axes='1,2', out=tmp_out, project='SIN')
#     miriad.fits(IN=tmp_out, out=outimage, op='xyout')
#     if (os.path.exists(tmp_reg)): rmtree(tmp_reg)
#     if (os.path.exists(tmp_out)): rmtree(tmp_out)
#
#
# # COPIED CHECK AND DELETE
#
#
# def cutout_size(inImage, hdrNo=0):
#     hdr = getheader(inImage, hdrNo)
#
#     coord_type = hdr['CTYPE1'].split('-')[0]
#
#     pix1 = int(round(float(hdr['NAXIS1'])))
#     pix2 = int(round(float(hdr['NAXIS2'])))
#
#     maxX, maxY = pix2coord(inImage, pix1, pix2)
#
#     minX, minY = pix2coord(inImage, 0, 0)
#
#     if coord_type == 'GLON':
#         newMax = coordConv(maxX, maxY, 'galactic j2000', 'fk5 j2000', 'glonglat', 'radec')
#         maxX = float(newMax[0])
#         maxY = float(newMax[1])
#
#         newMin = coordConv(minX, minY, 'galactic j2000', 'fk5 j2000', 'glonglat', 'radec')
#         minX = float(newMin[0])
#         minY = float(newMin[1])
#
#     pos2 = coords.Position((minX, minY))
#     pos3 = coords.Position((maxX, minY))
#     pos4 = coords.Position((minX, maxY))
#
#     Xsep = str(pos2.angsep(pos3)).split(' ')
#     Ysep = str(pos2.angsep(pos4)).split(' ')
#
#     if Xsep[1] == 'degrees':
#         X = float(Xsep[0]) * 3600.
#         Y = float(Ysep[0]) * 3600.
#     else:
#         X = -1.
#         Y = -1.
#
#     return X, Y
#
#
# def doGalexG(ndimage, fdimage, outimage):
#     if (os.path.exists(outimage)): os.remove(outimage)
#     tmpim1 = "/tmp/tmpsmooth" + id_generator() + ".fits"
#     if (os.path.exists(tmpim1)): os.remove(tmpim1)
#     iraf.gauss(input=fdimage, output=tmpim1, sigma=5)
#     contAvgImage(ndimage, tmpim1, outimage)
#     if (os.path.exists(tmpim1)): os.remove(tmpim1)
#
#
# def contAvgImage(left, right, result, allowshift='y', lefthdrNo=0, righthdrNo=0):
#     if (os.path.exists(result)): os.remove(result)
#     tmpim1 = "/tmp/tmpsqrt1" + id_generator() + ".fits"
#     if (os.path.exists(tmpim1)): os.remove(tmpim1)
#
#     hdr_dvd = getheader(left, lefthdrNo)
#     maxX_dvd = int(hdr_dvd['NAXIS1'])
#     maxY_dvd = int(hdr_dvd['NAXIS2'])
#
#     hdr_dvs = getheader(right, righthdrNo)
#     maxX_dvs = int(hdr_dvs['NAXIS1'])
#     maxY_dvs = int(hdr_dvs['NAXIS2'])
#     if maxX_dvd == maxX_dvs and maxY_dvd == maxY_dvs:
#         iraf.imarith(operand1=left, operand2=right, op='+', result=tmpim1)
#         iraf.imarith(operand1=tmpim1, operand2="2", op='/', result=result)
#     elif allowshift == 'y':
#         maxX = min(maxX_dvd, maxX_dvs)
#         maxY = min(maxY_dvd, maxY_dvs)
#         cutout = '[1:%s,1:%s]' % (maxX, maxY)
#         iraf.imarith(operand1=left + cutout, operand2=right + cutout, op='-', result=result)
#     if (os.path.exists(tmpim1)): os.remove(tmpim1)
#
#     return
#
#
#
#
#
# def correctMissAllign(inImage, outImage, positionsArray):
#     window_size = 10
#
#     hdulist = fits.open(inImage)
#     scidata = hdulist[0].data
#
#     for found_stars in positionsArray:
#
#         ha_star = found_stars['ha']
#
#         window = scidata[
#                  int(ha_star['Y_IMAGE']) - window_size: int(ha_star['Y_IMAGE']) + window_size,
#                  int(ha_star['X_IMAGE']) - window_size: int(ha_star['X_IMAGE']) + window_size
#                  ]
#
#         avg = np.average(window)
#         med = np.median(window)
#         std = np.std(window)
#
#         print avg, med, std
#
#         window_shape = window.shape
#
#         for i in range(window_size * window_size / 2):
#             try:
#                 max_key = np.argmax(window)
#                 max_index = np.unravel_index(max_key, window_shape)
#                 max_val = np.amax(window)
#                 min_key = np.argmin(window)
#                 min_index = np.unravel_index(min_key, window_shape)
#                 min_val = np.amin(window)
#
#                 if min_val > 0. or max_val < 0.:
#                     break
#                 new_val = max_val + min_val
#                 window[min_index[0]][min_index[1]] = new_val
#                 window[max_index[0]][max_index[1]] = new_val
#             except:
#                 continue
#
#         scidata[
#         int(ha_star['Y_IMAGE']) - 10: int(ha_star['Y_IMAGE']) + 10,
#         int(ha_star['X_IMAGE']) - 10: int(ha_star['X_IMAGE']) + 10
#         ] = window
#
#     hdulist.writeto(outImage)
#
#
#
#
#
#
# def addImageByFactor(inImage, outImage, factor, hdrNo):
#     # rnd = id_generator()
#     # temp_image = "/tmp/temp_image" + rnd + ".fits"
#     # shiftto0header(inImage, temp_image, hdrNo)
#
#     if (os.path.exists(outImage)): os.remove(outImage)
#     iraf.imarith(operand1=inImage, operand2=factor, op='+', result=outImage)
#     return
#
#
#
#
# def contsqrtImage(left, right, result, allowshift='y', lefthdrNo=0, righthdrNo=0):
#     if (os.path.exists(result)): os.remove(result)
#     tmpim1 = "/tmp/tmpsqrt1" + id_generator() + ".fits"
#     if (os.path.exists(tmpim1)): os.remove(tmpim1)
#
#     hdr_dvd = getheader(left, lefthdrNo)
#     maxX_dvd = int(hdr_dvd['NAXIS1'])
#     maxY_dvd = int(hdr_dvd['NAXIS2'])
#
#     hdr_dvs = getheader(right, righthdrNo)
#     maxX_dvs = int(hdr_dvs['NAXIS1'])
#     maxY_dvs = int(hdr_dvs['NAXIS2'])
#
#     if maxX_dvd == maxX_dvs and maxY_dvd == maxY_dvs:
#         iraf.imarith(operand1=left, operand2=right, op='*', result=tmpim1)
#         iraf.imfunction(input=tmpim1, output=result, function="sqrt")
#     elif allowshift == 'y':
#         maxX = min(maxX_dvd, maxX_dvs)
#         maxY = min(maxY_dvd, maxY_dvs)
#         cutout = '[1:%s,1:%s]' % (maxX, maxY)
#         iraf.imarith(operand1=left + cutout, operand2=right + cutout, op='-', result=result)
#     if (os.path.exists(tmpim1)): os.remove(tmpim1)
#
#     return
#
#
# def addIntImage(left, right, result, allowshift='y', lefthdrNo=0, righthdrNo=0):
#     if (os.path.exists(result)): os.remove(result)
#     tmpim1 = "/tmp/tmpsqrt1" + id_generator() + ".fits"
#     if (os.path.exists(tmpim1)): os.remove(tmpim1)
#
#     hdr_dvd = getheader(left, lefthdrNo)
#     maxX_dvd = int(hdr_dvd['NAXIS1'])
#     maxY_dvd = int(hdr_dvd['NAXIS2'])
#
#     hdr_dvs = getheader(right, righthdrNo)
#     maxX_dvs = int(hdr_dvs['NAXIS1'])
#     maxY_dvs = int(hdr_dvs['NAXIS2'])
#
#     if maxX_dvd == maxX_dvs and maxY_dvd == maxY_dvs:
#         iraf.imarith(operand1=left, operand2=right, op='*', result=tmpim1)
#         iraf.imfunction(input=tmpim1, output=result, function="sqrt")
#     elif allowshift == 'y':
#         maxX = min(maxX_dvd, maxX_dvs)
#         maxY = min(maxY_dvd, maxY_dvs)
#         cutout = '[1:%s,1:%s]' % (maxX, maxY)
#         iraf.imarith(operand1=left + cutout, operand2=right + cutout, op='*', result=tmpim1)
#         iraf.imfunction(input=tmpim1, output=result, function="sqrt")
#     if (os.path.exists(tmpim1)): os.remove(tmpim1)
#
#     return
#
#
# def decompressFitsImage(inFile, outPath, compressMethod, outPref='decom'):
#     newfile = "%s_%s.fits" % (outPref, id_generator())
#     command = "%s -O %s/%s %s" % (compressMethod, outPath.rstrip("/"), newfile, inFile)
#     os.system(command)
#     return newfile
#
#
# def compressFitsImage(inFile, outFile, outPath, compressMethod, outPref='decom'):
#     command = "%s -O %s/%s %s" % (compressMethod, outPath.rstrip("/"), newfile, inFile)
#     os.system(command)
#     return newfile
#
#
# def parse_header(in_image, return_fields, header_no=0):
#     return_list = {}
#     header_no = int(header_no)
#     if header_no == -1:
#         header_no = 0
#     for i in range(header_no + 1):
#         try:
#             hdr = getheader(in_image, i)
#         except:
#             return return_list
#         for ind in return_fields:
#             if ind in hdr:
#                 return_list[ind] = hdr[ind]
#             elif ind not in return_list:
#                 return_list[ind] = "null"
#
#     return return_list
#
#
# def find_band(filename, bands, headers, band_header=False):
#     if len(bands) == 1: return bands[0]
#     if band_header != "null":
#         if band_header in headers: return headers[band_header].replace("#", "")
#     for band in bands:
#         if band in filename: return band
#     return "null"
#
#
# def corect_VISTA_headers(in_image, out_image, hdr_no=0):
#     # try:
#     hdr_new = getheader(in_image, int(hdr_no))
#     data_new = getdata(in_image, int(hdr_no))
#     writeto(out_image, data_new, hdr_new)
#     pix1 = int(round(float(hdr_new['NAXIS1']) / 2.))
#     pix2 = int(round(float(hdr_new['NAXIS2']) / 2.))
#     RA, DEC = pix2coord(out_image, pix1, pix2)
#     iraf.hedit(out_image, fields='RA_CENT', value=RA, add='yes', verify='no')
#     iraf.hedit(out_image, fields='DEC_CENT', value=DEC, add='yes', verify='no')
#     if os.path.exists(out_image): return 1
#     # except:
#     #    return 0
#
#
# def check_UKIDSS(in_image, hdr_no=0):
#     # database = MySQLdb.connect(host="niksicko", user="gpneadmin", passwd="(g.pne.admin)")
#     database = qbmysqls.hash_database()
#     cursor = database.cursor()
#     hdr = getheader(in_image, int(hdr_no))
#     sdsuid = str(hdr['SDSUID'])
#     runid = str(hdr['RUNID'])
#     ra = str(hdr['RA_CENT'])
#     dec = str(hdr['DEC_CENT'])
#     sql = "SELECT * FROM `ImagesSources`.`UKIDSS` \
#             WHERE `MainGPN`.`GPNspherDist_ib`(`RA_CENT`,`DEC_CENT`," + ra + "," + dec + ") * 3600 < 60 \
#             AND `RUNID` = '" + runid + "' AND `SDSUID` = '" + sdsuid + "';"
#     cursor.execute(sql)
#     ids = cursor.fetchall()
#     if ids:
#         return True
#     else:
#         return False
#
#
# def check_VVVE(in_image, in_hdr=0):
#     # database = MySQLdb.connect(host="niksicko", user="gpneadmin", passwd="(g.pne.admin)")
#     database = qbmysqls.hash_database()
#     cursor = database.cursor()
#     hdr = getheader(in_image, int(in_hdr))
#     this_object = str(hdr['OBJECT'])
#     vsa_mfid = str(hdr['VSA_MFID'])
#     ra = str(hdr['RA_CENT'])
#     dec = str(hdr['DEC_CENT'])
#     sql = "SELECT * FROM `ImagesSources`.`VVVE` \
#             WHERE `MainGPN`.`GPNspherDist_ib`(`RA_CENT`,`DEC_CENT`," + ra + "," + dec + ") * 3600 < 60 \
#             AND `OBJECT` = '" + this_object + "' AND `VSA_MFID` = '" + vsa_mfid + "';"
#     cursor.execute(sql)
#     ids = cursor.fetchall()
#     if ids:
#         return True
#     else:
#         return False
#
#
#
#
#
#
#
#
#
#
#
# def checkCoverage(inImage, Xcoord, Ycoord, box_x_coord, box_y_coord, hdrNo=0, extracomm="",
#                   epoch="J2000"):
#     hdr = getheader(inImage, hdrNo)
#     min_x = 0
#     max_x = int(hdr['NAXIS1'])
#     min_y = 0
#     max_y = int(hdr['NAXIS2'])
#
#     box_x_coord = float(box_x_coord) / 3600.
#     box_y_coord = float(box_y_coord) / 3600.
#
#     nat_cords = natCoords(inImage, Xcoord, Ycoord)
#     pos_x = nat_cords[0]
#     pos_y = nat_cords[1]
#
#     if epoch == "1950":
#         temp_coord = coords.Position((pos_x, pos_y))  # ,equinox='b1950')
#         calc_coord = temp_coord.b1950()
#         pos_x = calc_coord[0]
#         pos_y = calc_coord[1]
#
#     temp_offset_x_1, temp_offset_y_1 = coord2pix(inImage, pos_x + box_x_coord, pos_y)
#     temp_offset_x_2, temp_offset_y_2 = coord2pix(inImage, pos_x, box_y_coord + pos_y)
#
#     coord_x_center, coord_y_center = coord2pix(inImage, pos_x, pos_y)
#
#     if extracomm == 'msxcheckoffset':
#         if coord_x_center > 216000: coord_x_center = coord_x_center - 216000
#
#     coordoffset = max(abs(coord_x_center - temp_offset_x_1), abs(coord_y_center - temp_offset_y_1),
#                       abs(coord_x_center - temp_offset_x_2),
#                       abs(coord_y_center - temp_offset_y_2))
#
#     coordXmin = coord_x_center - coordoffset
#     coordYmin = coord_y_center - coordoffset
#
#     coordXmax = coord_x_center + coordoffset
#     coordYmax = coord_y_center + coordoffset
#
#     if coord_x_center < 0 or coord_x_center > max_x or coord_y_center < 0 or coord_y_center > max_y:
#         return 'n'
#
#     if coordXmax >= max_x or coordYmax >= max_y or coordXmin <= min_x or coordYmin <= min_y:
#         return 'n'
#
#     return 'y'
#
#
# # def findRatioForSubtracion(haimage, rimage, mindist = 4, maxobjects = 100):
# #     ha_sex = runSex(haimage)
# #     r_sex = runSex(rimage)
# #
# #     avarray = np.array([])
# #
# #     no_step = ceil(len(ha_sex) / maxobjects)
# #     avg = 2.
# #     med = 1
# #     std = 0.
# #     k = 6
# #     flagpass = -1
# #
# #     while (abs(avg - med) > std or flagpass == -1) and k > 5:
# #         # while (std > avg / 10. or flagpass == -1) and k > 5:
# #         k = 0
# # #        for ha_star in ha_sex:
# #         for ha_step in range(0,len(ha_sex),int(no_step)):
# #             ha_star = ha_sex[ha_step]
# #             if ha_star['FWHM_IMAGE'] > 3 and ha_star['FWHM_IMAGE'] < 7 and ha_star['ELLIPTICITY'] < 0.2 and ha_star[
# #                 'FLAGS'] == 0:
# #                 ha_x = ha_star['X_IMAGE']
# #                 ha_y = ha_star['Y_IMAGE']
# #                 for r_star in r_sex:
# #                     if r_star['FWHM_IMAGE'] > 3 and r_star['FWHM_IMAGE'] < 7 and r_star['ELLIPTICITY'] < 0.2 and r_star[
# #                         'FLAGS'] == 0:
# #                         r_x = r_star['X_IMAGE']
# #                         r_y = r_star['Y_IMAGE']
# #
# #                         dist = sqrt(pow(ha_x - r_x, 2) + pow(ha_y - r_y, 2))
# #
# #                         if dist < mindist:
# #
# #                             ratio = ha_star['FLUX_BEST'] / r_star['FLUX_BEST']
# #                             if flagpass == -1 or abs(ratio - avg) < std:
# #                                 k = k + 1
# #                                 avarray = np.insert(avarray, 0, ratio)
# #         flagpass = 1
# #         avg = np.average(avarray)
# #         med = np.median(avarray)
# #         std = np.std(avarray)
# #
# #     avg = {'AVG': avg, 'STD': std, 'MED': med}
# #     return avg
#
#
#
#
# def makeKernel(haimage, rimage, results, mindist=4):
#     print haimage
#     print rimage
#     exit()
#
#     window_size = 10
#
#     hdulist_ha = fits.open(haimage)
#     scidata_ha = hdulist_ha[0].data
#
#     hdulist_r = fits.open(rimage)
#     scidata_r = hdulist_r[0].data
#
#     window_ha = scidata_ha[
#                 int(results['ha']['Y_IMAGE']) - window_size: int(results['ha']['Y_IMAGE']) + window_size,
#                 int(results['ha']['X_IMAGE']) - window_size: int(results['ha']['X_IMAGE']) + window_size
#                 ]
#
#     window_r = scidata_r[
#                int(results['ha']['Y_IMAGE']) - window_size: int(results['ha']['Y_IMAGE']) + window_size,
#                int(results['ha']['X_IMAGE']) - window_size: int(results['ha']['X_IMAGE']) + window_size
#                ]
#
#     subtract = np.subtract(window_ha, window_r)
#
#     avg = np.average(subtract)
#     med = np.median(subtract)
#     std = np.std(subtract)
#
#     print 'kernel', avg, med, std
#     shift_window_ha = window_ha
#     shift_window_r = window_r
#
#     for axis in range(2):
#         for dist in range(mindist):
#             shift_window_ha = np.delete(shift_window_ha, 0, axis)
#
#             # ha_star = found_stars['ha']
#             #
#             # window = scidata[
#             #       int(ha_star['Y_IMAGE']) - window_size: int(ha_star['Y_IMAGE']) + window_size,
#             #       int(ha_star['X_IMAGE']) - window_size: int(ha_star['X_IMAGE']) + window_size
#             #       ]
#             #
#             # avg = np.average(window)
#             # med = np.median(window)
#             # std = np.std(window)
#             #
#             # print avg, med, std
#             #
#             # window_shape = window.shape
#             #
#             # for i in range(window_size * window_size / 2):
#             #     try:
#             #         max_key = np.argmax(window)
#             #         max_index = np.unravel_index(max_key,window_shape)
#             #         max_val = np.amax(window)
#             #         min_key = np.argmin(window)
#             #         min_index = np.unravel_index(min_key,window_shape)
#             #         min_val = np.amin(window)
#             #
#             #         if min_val > 0. or max_val < 0.:
#             #             break
#             #         new_val = max_val + min_val
#             #         window[min_index[0]][min_index[1]] = new_val
#             #         window[max_index[0]][max_index[1]] = new_val
#             #     except:
#             #         continue
#             #
#             # scidata[
#             # int(ha_star['Y_IMAGE']) - 10: int(ha_star['Y_IMAGE']) + 10,
#             # int(ha_star['X_IMAGE']) - 10: int(ha_star['X_IMAGE']) + 10
#             # ] = window
#
#             # hdulist.writeto(outImage)
#
#

# def correct_iphas_corner(inImage):
#     rnd = id_generator()
#     corner = "/tmp/corner" + rnd + ".fits"
#     iraf.imcopy("{}:[1:300,1:300] {}".format(inImage, corner))
#
# def correct_header(inImage, tmpImage, hdrKey, hdrVal, hdrNo = 0, hdrDel = False):
#     imHeader = getheader(inImage, hdrNo)
#
#     if hdrKey in imHeader:
#         copyfile(inImage,tmpImage)
#         iraf.hedit(tmpImage, fields= hdrKey, value=hdrVal, add='yes', verify='no')
#         return tmpImage
#     return inImage
#
#
# def correct_shs_WATs(inFile):
#     setval(inFile, 'WAT0_001', value='system=image')
#     setval(inFile, 'WAT1_001', value='wtype=tan axtype=ra')
#     setval(inFile, 'WAT2_001', value='wtype=tan axtype=dec')
#
#
# def rebinOnTemplate(inImage,outImage,templateImage,templateFolder = '/tmp/'):
#
#     tempF = "{}{}/".format(templateFolder,id_generator())
#     if not os.path.exists(tempF):
#         os.makedirs(tempF)
#     else:
#         return False
#     copyfile(templateImage,tempF + "template.fits")
#     tmpltbl = 'templatetable' + id_generator() + ".tbl"
#     montage.mImgtbl(tempF,tmpltbl)
#     tmplhdr = 'templatehdt' + id_generator() + ".hdr"
#     montage.mMakeHdr(tmpltbl,tmplhdr)
#     montage.mProject(inImage,outImage,tmplhdr)
#     if (os.path.exists(tempF)): rmtree(tempF)
#     if (os.path.exists(tmpltbl)): os.remove(tmpltbl)
#     if (os.path.exists(tmplhdr)): os.remove(tmplhdr)
#

# # input Xcoord and Ycoord RA (degrees) and DEC (degrees)
# # boxCoords in arcsec
# def FITScutoutMir(inImage, outImage, Xcoord, Ycoord, boxXcoord, boxYcoord, incoords='', hdrNo=0):
#     from mirpy import miriad
#
#     tmpnvssmir = "/tmp/" + id_generator() + 'tmpnvss.mir'
#     tmpnvsssub = "/tmp/" + id_generator() + 'tmpnvss_sub.mir'
#
#     if (os.path.exists(outImage)): os.remove(outImage)
#
#     hdr = getheader(inImage, hdrNo)
#
#     minX = 0
#     maxX = int(hdr['NAXIS1'])
#     minY = 0
#     maxY = int(hdr['NAXIS2'])
#
#     boxXcoord = float(boxXcoord) / 3600.
#     boxYcoord = float(boxYcoord) / 3600.
#
#     ntcords = natCoords(inImage, Xcoord, Ycoord)
#     decpos1 = ntcords[0]
#     decpos2 = ntcords[1]
#
#     offsetPixX, offsetPixY = coord2pix(inImage, decpos1, decpos2 + boxYcoord)
#
#     coordXcent, coordYcent = coord2pix(inImage, decpos1, decpos2)
#
#     coordoffset = abs(coordYcent - abs(offsetPixY))
#
#     coordXmin = coordXcent - coordoffset
#     coordYmin = coordYcent - coordoffset
#
#     coordXmax = coordXcent + coordoffset
#     coordYmax = coordYcent + coordoffset
#
#     if coordXcent < 0 or coordXcent > maxX or coordYcent < 0 or coordYcent > maxY:
#         print "outside of the image?"
#         return 0
#
#     if coordXmax >= maxX: coordXmax = maxX - 1
#     if coordYmax >= maxY: coordYmax = maxY - 1
#     if coordXmin <= minX: coordXmin = minX + 1
#     if coordYmin <= minY: coordYmin = minY + 1
#
#     imsubregion = 'boxes(%s,%s,%s,%s)' % (coordXmin, coordYmin, coordXmax, coordYmax)
#
#     miriad.fits(IN=inImage, out=tmpnvssmir, op='xyin')
#     miriad.imsub(IN=tmpnvssmir, out=tmpnvsssub, region=imsubregion)
#     miriad.fits(IN=tmpnvsssub, out=outImage, op='xyout')
#
#     if (os.path.exists(tmpnvssmir)): rmtree(tmpnvssmir)
#     if (os.path.exists(tmpnvsssub)): rmtree(tmpnvsssub)
#
#     return

# def quotientImage(dividend, divisor, result, allowshift='y', dvdhdrNo=0, dvsrhdrNo=0, do_align=False):
#
#     if (os.path.exists(result)): os.remove(result)
#
#     rnd = id_generator()
#
#     temp_dividend = "/tmp/temp_dividend" + rnd + ".fits"
#     temp_divisor = "/tmp/temp_divisor" + rnd + ".fits"
#
#     if do_align:
#         align_images(dividend, divisor, temp_dividend, temp_divisor)
#     else:
#         shiftto0header(dividend, temp_dividend, dvdhdrNo)
#         shiftto0header(divisor, temp_divisor, dvsrhdrNo)
#
#     hdr_dvd = getheader(temp_dividend, 0)
#     maxX_dvd = int(hdr_dvd['NAXIS1'])
#     maxY_dvd = int(hdr_dvd['NAXIS2'])
#
#     hdr_dvs = getheader(temp_divisor, 0)
#     maxX_dvs = int(hdr_dvs['NAXIS1'])
#     maxY_dvs = int(hdr_dvs['NAXIS2'])
#
#     if maxX_dvd == maxX_dvs and maxY_dvd == maxY_dvs:
#         iraf.imarith(operand1=temp_dividend, operand2=temp_divisor, op='/', result=result)
#     elif allowshift == 'y':
#         maxX = min(maxX_dvd, maxX_dvs)
#         maxY = min(maxY_dvd, maxY_dvs)
#         cutout = '[1:%s,1:%s]' % (maxX, maxY)
#         iraf.imarith(operand1=temp_dividend + cutout, operand2=temp_divisor + cutout, op='/', result=result)
#
#     return

