import math
from astropy import units as u
import numpy as np
import ibastro.constants as myco
from astropy import constants as const
from astropy import units as u
# from astropy.modeling.blackbody import blackbody_lambda
from ibastro.convertunits import JyToSI


def Tb(flux, theta, freq, fluxErr=0.0, thetaErr=0.0):
    '''Calculates brightness temperature (K) for constant Tb approximation 
     
    from: 
    flux (Jy), 
    angular diameter (arcsec) and 
    frequency (GHz).
    additionally calculates the error in Tb
    from fluxErr (%) and thetaErr (%) (0% and 0% by default)    
    i.e. Tb(flux,theta,freq,fluxErr,thetaErr)
    output is a list:
    [0=>Tb,1=>dTb] in K
    '''

    fluxSI = JyToSI(flux)
    thetaRad = math.math.radians(theta / 3600.0)
    freqHz = freq * 1.0E9
    brTemp = 2.0 * pow(myco.C_c, 2) * fluxSI / (pow(freqHz, 2) * math.pi * pow(thetaRad, 2) * myco.C_k)
    brTempErr = brTemp * math.sqrt(pow(fluxErr, 2) + 2.0 * pow(thetaErr, 2))
    result = [brTemp, brTempErr]

    return result


def Tb_tau(Te, tau_nu, nu):
    '''Calculates brightness temperature (K) for constant Tb approximation

    from:
    Te - electron temperature (K),
    tau_nu - optical thickness at freq nu
    nu - frequency (GHz).
    '''
    result = Te * (1 - np.exp(-tau_nu))
    return result


def TbPeak(peakFlux, beamMaj, beamMin, freq, peakFluxErr=0.0, beamMajErr=0.0, beamMinErr=0.0):
    '''Calculates Peak Brightness Temperature (Tb) for constant Tb approximation
    
    Sinth. beam solid angle:http://www.cv.nrao.edu/course/astr534/Interferometers.html
    Main equation: Rohlfs & Wilson page 190
    from: 
    peakFlux (Jy/Beam),
    beamMaj and beamMin sinthisized beam maj and min FWHM (arcsec),
    freq (GHz).
    additionally calculates the error in TbPeak
    from fluxErr (%) and beamMajErr and beamMinErr (%) (0% and 0% by default)    
    i.e. Tb(flux,beamMaj, beamMin,freq,fluxErr,beamMajErr,beamMinErr)
    output is a list:
    [0=>TbPeak,1=>dTbPeak] in K
'''
    peakFluxSI = JyToSI(peakFlux)
    beamMajRad = math.radians(beamMaj / 3600.0)
    beamMinRad = math.radians(beamMin / 3600.0)
    freqHz = freq * 1.0E9
    solidAngleBeam = math.math.pi * beamMajRad * beamMinRad / (4.0 * math.log10(2.0))
    brTemp = pow(myco.C_c, 2) * peakFluxSI / (2.0 * pow(freqHz, 2) * solidAngleBeam * myco.C_k)
    brTempErr = brTemp * math.sqrt(pow(peakFluxErr, 2) + pow(beamMajErr, 2) + pow(beamMinErr, 2))
    result = [brTemp, brTempErr]
    return result


def NeGath(S5GHz, D, theta, epsilon=0.35, Te=1.0E4, Y=0.1, X=0.33, dS5GHz=0.0, dD=0.0, dTe=0.0, dtheta=0.0,
           depsilon=0.0):
    '''Calculate electron density (cm^-3) from Gathier 1987
    
    from:
    S5GHz 5GHz integrated flux density (mJy)
    D distance (Kpc)
    theta angular diameter (arcsec)
    epsilon volume filling factor (default=0.0.35)
    Te electron temperature (default=10E4K)
    Y He abundance (default= 0.1 )
    X fraction of the doubly ionized He (default=1/3  )
    additionally calculates the error in Ne
    from dS5GHz (%), dD, dTe (%), dtheta (%) and depsilon (%) (0% by default)
    i.e. Ne ( S5GHz, dist, theta, epsilon, Te, Y, X, dS5GHz, dTe, dtheta , depsilon )
    
    output is a list:
    [ Ne , dNe ] in (cm^-3)
    '''
    A = pow(((1.0 + Y + 3.0 * X * Y) / (1.0 + Y + X * Y)), -0.5)
    Ne = 4.96E2 * pow(S5GHz, 0.5) * pow(Te, 0.25) * pow(D, -0.5) * pow(theta / 2.0, -1.5) * pow(epsilon, -0.5) * A
    dNe = Ne * math.sqrt(
        0.5 * pow(dS5GHz, 2) + 0.25 * pow(dTe, 2) + 0.5 * pow(dD, 2) + 1.5 * pow(dtheta, 2) + 0.5 * pow(depsilon, 2))
    result = [Ne, dNe]
    return result


def radius(D, theta, dD=0.0, dtheta=0.0):
    '''Calculates the true radius (pc)
    
    from:
    D distance (Kpc)
    theta angular diameter (arcsec)
    additionally calculates the error in radii 
    from dD (%) and dtheta (%) (0% by default)
    i.e. radius(dist,theta,ddist,dtheta)     
    output is a list:
    [ R, dR ] in (pc)
    '''

    R = D * theta / (2 * 206.265)
    dR = R * math.sqrt(pow(dD, 2) + pow(dtheta, 2))
    result = [R, dR]
    return result


def distance(R, theta, dR=0.0, dtheta=0.0):
    '''Calculates the distance (kpc)
    
    from:
    R radius (pc)
    theta angular diameter (arcsec)
    additionally calculates the error in distance 
    from dR (%) and dtheta (%) (0% by default)
    i.e. distance(R,theta,dR,dtheta)     
    output is a list:
    [ D, dD ] in (kpc)
    '''

    D = R * (2 * 206.265) / theta
    dD = D * np.sqrt(pow(dR, 2) + np.power(dtheta, 2))
    return D,dD


def angdiam(R, D):
    '''Calculates the angular diameter (arcsec)
    
    from:
    R radius (pc)
    D distance (kpc)
    output is a diam in (arcsec)
    '''
    result = 2 * (R * u.pc  / (D * u.kpc)  * u.radian).to(u.arcsec)
    return result


def MionGath(Ne, D, theta, epsilon=0.35, Y=0.1, X=0.33, dNe=0.0, dD=0.0, dtheta=0.0, depsilon=0.0):
    '''Calculate ionised mass (Msun) from Gathier 1987
    
    from:
    Ne electron density (cm^-3)
    D distance ( Kpc )
    theta angular diameter ( arcsec )
    epsilon volume filling factor ( default = 0.35 )
    Y He abundance (default= 0.1 )
    X fraction of the doubly ionized He (default=1/3  )
    additionally calculates the error in Mion 
    from dNe (%), dD (%), dtheta (%) and depsilon (0% by default)
    i.e. MionGath(Ne,D,theta,epsilon,dNe,dD,dtheta,depsilon)     
    output is a list:
    [ Mion, dMion ] in (Msun)
    '''
    A = (1.0 + 4.0 * Y) / (1.0 + Y + X * Y)
    Mi = 1.18E-8 * Ne * pow(D, 3) * pow(theta / 2.0, 3) * epsilon * A
    dMi = Mi * math.sqrt(pow(dNe, 2) + 3.0 * pow(dD, 2) + 3.0 * pow(dtheta, 2) + pow(depsilon, 2))
    result = [Mi, dMi]
    return result


def MionBoff(Ne, D, Hbeta, alphabeta, dNe=0.0, dD=0.0, dHbeta=0.0):
    '''Calculate ionised mass (Msun) from Boffi & Stanghellini 1993
    
    from:
    Ne electron density (cm^-3)
    D distance ( Kpc )
    Hbeta flux (erg cm^-2 s^-1)
    alphabeta H recombination coefficient (cm^3 s^-1)
    additionally calculates the error in Mion 
    from dNe (%), dD (%), dtheta (%) and dHbeta (%) (0% by default)
    i.e. MionBoff(Ne,D,Hbeta,alphabeta,dNe,dD,dHbeta)     
    output is a list:
    [ Mion, dMion ] in (Msun)
    '''
    Mi = 3.45E-2 * Hbeta * pow(D, 2) / (alphabeta * Ne)
    dMi = Mi * math.sqrt(pow(dNe, 2) + 2.0 * pow(dD, 2) + pow(dHbeta, 2))
    result = [Mi, dMi]
    return result


def alphabeta(Te):
    '''Calculates H recombination coefficient alphabeta from Boffi & Stanghellini 1993
    ; see also Brocklehurst 1971 table V
    
    from: 
    Te electron temperature (K)
    i.e. alphabeta(Te) in (cm^3 s^-1)
    '''
    result = 9.69E-11 * pow(Te, -0.88)
    return result


def radioTohbeta(S, freq, Y=1.1, Te=1.0E4):
    ''' calculates optically thin radio to Hbeta ( Pottasch 1984 )
    
    S int flux density ( mJy )
    freq frequency ( GHz )
    Y abundance (see Yeff function: default 1.01 i.e. 10% of He1 0% of He2)
    Te electron temperature ( K ) (default 1e4 K)
    i.e. radioTohbeta(S,freq,Y,Te)
    output is Hbeta flux in ( erg cm ^ -2 s ^ -1 )
    '''
    result = S / (2.91E10 * pow(Te, 0.53) * pow(freq, -0.1) * Y)
    return result


def radioTohalpha(S, freq, Y=1.1, Te=1.0E4, haTohb=2.85):
    ''' calculates optically thin radio to Halpha ( Pottasch 1984 )

    S int flux density ( mJy )
    freq frequency ( GHz )
    Y abundance (see Yeff function: default 1.01 i.e. 10% of He1 0% of He2)
    Te electron temperature ( K ) (default 1e4 K)
    i.e. radioTohalpha(S,freq,Y,Te,haTohb)
    haTohb Halpha to Hbeta ratio
    output is Halpha flux in ( erg cm ^ -2 s ^ -1 )
    '''

    result = haTohb * radioTohbeta(S, freq, Y, Te)
    return result


def hbetaToradio(Hbeta, freq, Y=1.1, Te=1.0E4):
    ''' calculates Hbeta flux from optically thin radio ( Pottasch 1984 )
    
    Hbeta flux  ( erg cm ^ -2 s ^ -1 )
    freq frequency ( GHz )
    Y abundance (see Yeff function: default 1.01 i.e. 10% of He1 0% of He2)
    Te electron temperature ( K ) (default 1e4 K)
    i.e. hbetaToradio(Hbeta,freq,Y, Te)
    output is radio flux in (mJy) 
    '''

    result = Hbeta * (2.91E10 * pow(Te, 0.53) * pow(freq, -0.1) * Y)
    return result


def haplhaToradio(Halpha, freq, Y=1.1, Te=1.0E4, haTohb=2.85):
    ''' calculates Halpha flux from optically thin radio ( Pottasch 1984 )

    Halpha flux  ( erg cm ^ -2 s ^ -1 )
    freq frequency ( GHz )
    Y abundance (see Yeff function: default 1.01 i.e. 10% of He1 0% of He2)
    Te electron temperature ( K ) (default 1e4 K)
    haTohb Halpha to Hbeta ratio
    i.e. halphaToradio(Hbeta,freq,Y, Te, haTohb)
    output is radio flux in (mJy)
    '''

    result = hbetaToradio(Halpha / haTohb, freq, Y, Te)
    return result


def Yeff(nH1, nHe1, nHe2, Te=1.0E4, freq=5.0E9):
    '''Calculates Y from Pottasch 1984 (see IV-24)

    from:
    nH1 ionised H
    nHe1 and nHe2 ionised He
    Te electron temperature (K)
    freq frequency (Hz)
    i.e. Yeff (nH1, nHe1, nHe2, Te, freq)
    '''

    result1 = math.log(4.95E-2 * pow(Te, 1.5) * pow(freq, -1.0)) / math.log(9.9E-2 * pow(Te, 1.5) * pow(freq, -1.0))
    result = 1.0 + nHe1 / nH1 + 4.0 * nHe2 / nH1 * result1
    return result


def fluxconvert(flux, freq1, freq2, spindex):
    '''Converts flux density from freq1 to freq2 using the spectral index
    
    from:
    flux known flux density 
    fromfreq from frequency1
    tofreq to frequency2 
    spindex spectralindex
    i.e. fluxconvert(flux,fromfreq,tofreq,spindex)
    result is in the same units as the input flux
    '''
    result = flux * pow(freq2 / freq1, spindex)
    return result


def spindexTwoFreq(flux1, flux2, freq1, freq2):
    '''Calculates spectral index from radio flux at two frequencies
    
    from:
    flux1 flux at freq1
    flux2 flux at freq2
    freq1
    freq2
    '''
    result = math.log10(flux1 / flux2) / math.log10(freq1 / freq2)
    return result


def fluxconvtb(S2, freq1, freq2, Tb2, Te=1.0E4):
    '''Calculates flux density S1 at frequency freq1 if its known
    flux density and brightness temperature at freq2

    from:
    S2 known flux density
    freq1 freq of the out flux (unit must be the same as freq2)
    freq2 freq of the in flux (unit must be the same as freq1)
    Tb2 brightness temp (K)
    Te electron temp (K)
    result is flux density in the same unit as the S2
    '''

    gamma = pow(freq1 / freq2, -2.1)
    psi = 1. - Tb2 / Te
    alpha = 2. - (2.1 / math.log(gamma)) * math.log((1. - pow(psi, gamma)) / (1. - psi))
    result = S2 * pow(freq1 / freq2, alpha)
    return result


def colradtohalpha(S1, Halpha):
    result = 2.5 / 2.51 * math.log10(S1 / Halpha * 1 / 1.09E12)
    return result


def Chbeta_fromRadio(S1, freq1, Hbeta, Te=1.E4, Y=1.1):
    '''Calculates c(extinction) in Hbeta from observed radio flux
    (S1) at frequency freq1 and observed Hbeta flux

    from:
    S1 radio continuum flux density [mJy]
    freq1 frequency of the in radio flux [GHz]
    Hbeta Hbeta flux [erg cm^-2 s^-1]
    Te electron temperature [K]
    Y He abundance
    result is Chbeta'''

    Knst = 1. / (2.51 * 1.E10 * pow(Te, 0.53) * pow(freq1, -0.1) * Y)
    # Knst = 1/1.09E12
    result = math.log10(Knst * S1 / Hbeta)
    return result


def Chalpha_fromRadio(S1, freq1, Halpha, Te=1.E4, Y=1.1):
    '''Calculates c(extinction) in Halpha from observed radio flux
    (S1) at frequency freq1 and observed Halpha flux

    from:
    S1 radio continuum flux density [mJy]
    freq1 frequency of the in radio flux [GHz]
    Hbeta Hbeta flux [erg cm^-2 s^-1]
    Te electron temperature [K]
    Y He abundance
    result is Chalpha'''

    result = Chbeta_fromRadio(S1, freq1, Halpha / 2.85, Te, Y)
    return result


def solidAngle(theta):
    '''solid angle from angular diameter
        from:
        theta - angular diameter [arcsec]'''
    torads = np.radians(theta / (2. * 3600.0))
    return 2. * np.pi * (1. - math.cos(torads))

def solidAngle_ap(theta):
    '''solid angle from angular diameter
        from:
        theta - angular diameter [arcsec]'''
    return np.pi / 4 * theta ** 2. * 3600. ** -2. * (np.pi / 180.) ** 2.


def tauFromTau_c(nu, nu_c, tau_c=1.):
    '''calculates optical thickness at freq nu
        from known opt. thick.
        Pottasch 1984
        from:
        nu - frequency
        nu_c - frequency at known opt. thick.
        tau_c - known opt. thickness'''

    tau = tau_c * np.power(nu / nu_c, -2.1)

    return tau




def diffLogSourceCount(S_list, Nbins, startS='auto'):
    '''Calculates the differential source count dn/ds
        from list of fluxes and ds

        from:
        S_list list of flux densities (in same units)
        ds flux density interval
        logscale flag for scale (True log, False decimal)
        startS initial S (bin start)
        if startS == auto => startS = min(S_list)'''

    # convert list of fluxes to array of numpy floats#
    list_S = np.array(S_list).astype(float)

    if startS == 'auto':
        startS = min(list_S)

    ds = (math.log10(max(list_S)) - math.log10(startS)) / Nbins

    bins = np.arange(math.log10(startS), Nbins * ds, ds)

    histo, bins = np.histogram(np.log10(list_S), bins)

    outfile = open('outcondon.txt', 'w')
    k = 0
    for i in histo:
        diffNo = float(i) / ds
        bincenter = (pow(10, bins[k + 1]) + pow(10, bins[k])) / 2.
        outfile.write(str(diffNo) + ',' + str(bincenter) + "\n")
        k = k + 1
    outfile.close()
    return


def rcSpectralDist_ST01(nu, nu_c, theta, Te):  # ,alphathick = 2.):
    ''' Calculates expected radio flux from
        two component (thin & thick) model: Siodmiak & Tylenda 2001 eq.6

        from:
        nu - frequency (GHz)
        Te - electron temperature (K)
        tau_nu - optical thickness at freq. nu
        ksi - thick part of the nebula
        epsilon - (epslon*tau_nu) optical thickness in the thin part
        omega - solid angle of the nebula

        returns:
        flux - [mJy]'''

    ksi = 0.27
    epsilon = 0.19
    # Te = 1.E4
    omega = solidAngle(theta)
    alphaf = -2.1  # -1. * alphathick - 0.1
    Fnu = 30723615. * Te * np.power(nu, 2.) * ((1 - np.exp(- np.power(nu / nu_c, alphaf))) * ksi * omega + (
        1. - np.exp(epsilon * - np.power(nu / nu_c, alphaf))) * (1. - ksi) * omega)
    return Fnu


def spindex(nu, A, alpha):
    ''' spindex TODO'''
    S = A * np.power(nu, alpha)
    return S


def twomassToJy(band, mag):
    '''Converts mag to flux in Jy
        using zero mag fluxes:
        http://www.ipac.caltech.edu/2mass/releases/allsky/faq.html#jansky
        Band	Lambda (micm)	Bandwidth (micm)    Fnu - 0 mag (Jy)	Flambda - 0 mag (W cm-2 micm-1)
        J       1.235 \pm 0.006	0.162 \pm 0.001       1594 \pm 27.8             3.129E-13 \pm 5.464E-15
        H	1.662 \pm 0.009	0.251 \pm 0.002       1024 \pm 20.0             1.133E-13 \pm 2.212E-15
        Ks	2.159 \pm 0.011	0.262 \pm 0.002       666.7 \pm 12.6            4.283E-14 \pm 8.053E-16
        from:
        band - band (J,H,Ks)
        mag - magnitude

        returns:
        Fnu - [Jy]'''

    zerofluxes = {'J': 1594.0, 'H': 1024.0, 'Ks': 666.7}
    Fnu = zerofluxes[band] * pow(10., -0.4 * mag)
    return Fnu


def uncmagToJy(dmag, Fnu):
    '''Converts uncertanty in mag to uncertanty in Jy
        from:
        dmag - uncertanty in mag
        Fnu - Flux in Jy

        returns:
        dFnu - [Jy]'''

    dFnu = max(abs(Fnu * (pow(10., -dmag / 2.5) - 1.)), abs(Fnu * (pow(10., dmag / 2.5) - 1.)))
    return dFnu


def twomassUncmagToJy(dmag, Fnu=False, band=False, mag=False):
    '''Converts 2mass uncertanty in mag to uncertanty in Jy
        from:
        dmag - uncertanty in mag
        Fnu - Flux in Jy
        band - band (J,H,Ks)
        mag - magnitude

        returns:
        dFnu - [Jy]'''

    if not Fnu and band and mag:
        Fnu = twomassToJy(band, mag)
    dFnu = uncmagToJy(dmag, Fnu)
    return dFnu


def freqtolambda(freq):
    '''Converts frequency to wavelength
        from:
        freq - frequency in GHz
        
        returns:
        lambda - [m]'''

    return myco.C_c / freq * 1E-9


def lambdatofreq(lmbd):
    '''Converts wavelength to frequency
        from:
        lmbd in meters
        
        returns:
        freq - [GHz]'''

    return myco.C_c / lmbd * 1E-9


def sigmaVukotic(S, theta):
    ''' Sigma from Vukotic et al. 2009
        from:
        S in Jy
        theta in arcmin
    
        returns:
        sigma in Wm-2Hz-1sr-1'''

    Sigma = 1.505E-19 * S / pow(theta, 2)
    return Sigma


def fluxFromSpindex(nu1, spindex, nu2, S2):
    '''Calculates flux at nu1
        from:
        nu1: frequency 1 
        spindex: spectral index
        nu2: frequency 2
        S2: flux at feq. 2
        
        nu1 and nu2 must be in same units
        return flux at nu1 in the same units as S2
        '''
    S1 = S2 * pow((nu1 / nu2), spindex)
    return S1


def extinction_correction_EBV(mag, E_B_V, factor=3.2):
    """
    Corrects magnitude for extinction using E(B-V) correction
    :param mag: input magnitude
    :param E_B_V: E(B-V) correction 
    :param factor: factor
    :return: corrected magnitude
    """
    return mag - factor * E_B_V


def obs_flux(Flux_lambda, Radius, Distance, A_lambda=0.):
    """
    Calculates observed flux at lambda from a star with:
    r=Radius at d=Distance and extinct by A_lambda. 
    :param Flux_lambda: float/numpy array surface flux at lambda (quantity)
    :param Radius: float radius of the star (quantity)
    :param Distance: float distance to the star (pc)
    :param A_lambda: float extinction at lambda (no units)
    :return: float (quantity)
    """
    return pow(10., -0.4 * A_lambda) * pow((Radius.to(u.pc) / (Distance * u.pc)), 2.) * Flux_lambda


def radius_from_luminosity(Teff, L):
    """
    Calculates Radius of a star from its L and Teff
    :param Teff: Teff in T sollar (quantity)
    :param L: L in L sollar (no units)
    :return: float Radius (meters)
    """
    return const.R_sun * np.sqrt(L) * pow(Teff, -2)

def expansion_rate_angular(D,V):
    # D kpc
    # V km/s

    # in a year
    Vkms = V * u.km / u.s
    Year = (1 * u.year).to(u.s)
    Vkm = Vkms * Year
    Vpc = Vkm.to(u.pc)

    dtheta = (angdiam(Vpc.value, D) / 2.).to(u.marcsec)


    return dtheta

# def bb_monochr_flux(Teff, L_solar, lambdas, Distance=10., A_lambda=0.):
#     radius = radius_from_luminosity(Teff / (myco.C_Tsun * u.K), L_solar)
#     monochromatic_fluxes = blackbody_lambda(lambdas, Teff)
#     return obs_flux(monochromatic_fluxes, radius, Distance, A_lambda)


def black_body(Teff, L_sun, lambda_min, lambda_max, Distance, A_V=0., points=10000):
    step = (lambda_max - lambda_min) / float(points)

    lambdas = np.arange(lambda_min, lambda_max, step) * u.AA

    with np.errstate(all='ignore'):
        radius = radius_from_luminosity(Teff / (myco.C_Tsun * u.K), L_sun)

        monochromatic_fluxes = blackbody_lambda(lambdas, Teff)

        flux_lam = obs_flux(monochromatic_fluxes, radius, Distance, A_V)

    return lambdas, flux_lam, radius


# Rayleigh-Jeans functions at freq
def RJ_function(freq):
    return 2. * freq ** 2. * myco.C_k * myco.C_c ** -2.


## calculates emission measure [pc cm^-6] from
## critical frquency freq_0 [GHz]
## electron temperature Te [K]
def EM(freq_0, Te):
  return 3048780. * (Te/1.E4)**1.35 * freq_0 ** 2.1


def log_uncertainty(obs, obs_err):
    """
    Uncertainty of the observation in the log scate
    see: http: // faculty.washington.edu / stuve / uwess / log_error.pdf
    @param obs: observation value (lin scale)
    @param obs_err: observation uncertainty (lin scale)
    @return: obs_err in log scale
    """
    if obs_err is None:
        return None
    return 0.434 * obs_err / obs


def powr_law(x, intersect, power, base = 10.):
    """
    Power law function i.e. base ^ intercept * x ^ power
    @param base: base of the log
    @param x: value
    @param intersect:
    @param power:
    @return:
    """
    return base ** intersect * x ** power

def log_powr_law(x, intersect, power):
    """
    Power law function for loged value
    @param x:
    @param intersect:
    @param power:
    @return:
    """
    return intersect + x * power
