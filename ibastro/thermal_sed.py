from ibastro import physics as ibph
import numpy as np
import math
import scipy.integrate as integrate


def flux_nu(freq, theta, freq_0, mu, Te, model):
    """
    calculate flux [mJy] at freq [GHz] given:
    Te: electron temperature (K)
    theta: angular size [arcsec]
    freq_0: critical freq [GHz]
    model: model for density distribution, choose from (Olnon 1975):
    http://adsabs.harvard.edu/abs/1975A%26A....39..217O
           cylindrical: model I
           spherical: model II
           trpowerlaw: truncated power law model V
           sphshell: spherical shell (new model)

    @param model: model used for fitting, use one of:
            cylindrical, spherical, trpowerlawmod, sphshell, longcylindrical
    @param mu: ratio between inner and outer radius of the shell i.e. mu=Rin/Rout
    @param Te: electron temperature in K
    @param freq: frequency in GHz
    @param theta: angular diameter in arcsec
    @param freq_0: turnover frequency in GHz
    @return: flux in mJy
    """
    # correction for geometry
    corrections = {
        "cylindrical"    : 1,
        "spherical"      : 1,
        # "trpowerlawmod"  : 1 - 3. * mu / 8.,
        "trpowerlawmod"  : 8. / 3. - 2 * mu,
        "sphshell"       : 1 - mu,
        # "sphshell"       : mu,
        "longcylindrical": 2
    }

    if model not in corrections.keys():
        print("Model must be one of:{}.".format(list(corrections.keys())))
        raise ValueError

    opt_thick_corr = corrections[model]

    # tau at nu assuming critical freq = freq_0
    tau_nu = ibph.tauFromTau_c(nu=freq, nu_c=freq_0)

    tau_integral = opt_thick_model(tau_nu / opt_thick_corr, mu, model)

    sld_angl = ibph.solidAngle_ap(theta)

    res_flux = ibph.RJ_function(freq * 1E9) * Te * sld_angl * tau_integral * 1E26
    # in mJy
    return res_flux * 1E3


# models for optical thickness
def opt_thick_model(tau, mu, model):
    res = False
    if model == "cylindrical":
        res = tau_cylindrical(tau)
    elif model == "spherical":
        res = tau_spherical(tau)
    elif model == "trpowerlawmod":
        res = tau_trpowerlawmod(tau, mu)
    elif model == 'sphshell':
        res = tau_sphshell(tau, mu)
    elif model == 'longcylindrical':
        res = tau_cylindrical(tau)
    return res


# optical thickness for cylindrical density distribution model I
def tau_cylindrical(tau):
    return 1 - np.exp(-1 * tau)


# optical thickness for spherical density distribution model II
def tau_spherical(tau):
    return 1 - 2 / tau ** 2 * (1 - (tau + 1) * np.exp(-1 * tau))


def tau_sphshell(tau, mu):
    res = np.array([])
    for t in tau:
        int_res_1, err = integrate.quad(optdepth_int_3, 0, mu, args=(t, mu,))
        int_res_2, err = integrate.quad(optdepth_int_4, mu, 1, args=(t,))
        res = np.append(res, int_res_1 + int_res_2)
    # extra 2* in eq 23 (4pi instead of 2pi)
    return 2. * res


# optical thickness modified truncated power-law
def tau_trpowerlawmod(tau, mu):
    res = np.array([])
    for t in tau:
        int_res_1, err = integrate.quad(optdepth_int_6, 0, 1, args=(t, mu,))
        int_res_2, err = integrate.quad(optdepth_int_2, 1, np.inf, args=(t,))
        res = np.append(res, int_res_1 + int_res_2)
    # extra 2* in eq 23 (4pi instead of 2pi)
    return 2 * res


# opt depth integral 2 (Olnon 1975 second part Eq.23)
def optdepth_int_2(x, p):
    return x * (1. - np.exp(-3. / 16. * math.pi * p / (x ** 3.)))


def optdepth_int_3(x, p, mu):
    return x * (1. - np.exp(-p * gx_sphshell(x, mu)))


def optdepth_int_4(x, p):
    return x * (1. - np.exp(-p * gx_sphere(x)))


# opt depth integral 6 (Olnon 1975 first part Eq.23)
def optdepth_int_6(x, p, mu):
    return x * (1. - np.exp(-3. / 8. * p * gx_pwlawmod(x, mu)))


# spherical shell
# g(x) function (new)
def gx_sphshell(x, mu):
    return np.sqrt(1. - x ** 2.) - np.sqrt(mu ** 2. - x ** 2.)


def gx_sphere(x):
    return np.sqrt(1. - x ** 2.)


# g(x) function, defined in Olnon 1975, eq. 21 with correction for spherical shell
def gx_pwlawmod(x, mu):
    if x == 0:
        res = 8. / 3. - 2 * mu
    elif 0 < x <= mu:
        res = x ** -3. * (math.pi / 2 - math.atan(1. / x * np.sqrt(1. - x ** 2.)) -
                          x * np.sqrt(1. - x ** 2.)) + 2. * np.sqrt(1. - x ** 2.) - \
              2. * np.sqrt(mu ** 2. - x ** 2.)
    elif mu < x < 1.:
        res = x ** -3. * (math.pi / 2. - math.atan(1. / x * np.sqrt(1. - x ** 2.)) -
                          x * np.sqrt(1. - x ** 2.)) + 2. * np.sqrt(1. - x ** 2.)
    else:
        res = x ** -3 * math.pi / 2.
    return res
