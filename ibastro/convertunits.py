'''
Created on Aug 12, 2010

@author: ivan
'''
from math import log10
def JyToSI ( flux ):
    '''Convert flux in Jy to SI (W m^-2 Hz^-1)'''
    result = flux * 1E-26
    return result

def SIToJy ( flux ):
    '''Convert flux in SI units (W m^-2 Hz^-1) to Jy'''
    result = flux * 1E26
    return result

def RylToErg (inR,pixSize):
    '''Convert flux in Ryleys to [erg cm^-2 s^-1]'''
    result = 5.66E-18 * pow(pixSize,2.) * inR
    return result

def MagFromFlux(flux,F0):
    '''Convert flux to magnitudes'''
    result = -2.5 * log10(flux/F0)
    return result

def uncFluxToMag (dF,flux):
    '''Convert uncertanty in flux to magnitudes'''
    result = max(-2.5 * log10(1. - dF/flux) , -2.5 * log10(1. + dF/flux))
    return result

def uncMagToFlux (dm,flux):
    '''Convert uncertanty in magnitudes to flux'''
    result = flux * max(1. - pow(10.,-1. * 0.4 * dm), 1. - pow(10.,0.4 * dm))
    return result

