# import os
# from shutil import rmtree

from astropy import units as u
from astropy import wcs
from astropy.coordinates import SkyCoord
from astropy.io import fits
# from qb_common.strings import id_generator
import ibastro.montages as montages
from astropy.io.fits import getheader
import math
import numpy as np
# import ibastro.fitslibs as fitslibs


def gal2radec(crd1, crd2, frame_in='galactic', frame_out='fk5'):
    """
    Convert galactic to decimal RA,DEC
    :param crd1: float glon
    :param crd2: float glat
    :param frame_in: string
    :param frame_out: string
    :return: strings
    """
    crd1 = float(crd1)
    crd2 = float(crd2)
    c = SkyCoord(l=crd1 * u.degree, b=crd2 * u.degree, frame=frame_in)
    radec = getattr(c, frame_out)
    return radec.ra.deg, radec.dec.deg


def gal2hmsdms(crd1, crd2, frame_in='galactic', frame_out='fk5'):
    """
    Convert galactic to hmsdms RA,DEC
    :param crd1: float glon
    :param crd2: float glat
    :param frame_in: string
    :param frame_out: string
    :return: strings
    """
    crd1 = float(crd1)
    crd2 = float(crd2)
    dra, ddec = gal2radec(crd1, crd2, frame_in, frame_out)
    return radec2hmsdms(dra, ddec, frame_out)


def radec2hmsdms(crd1, crd2, frame_in='fk5', frame_out='fk5'):
    """
    Convert decimal RA,DEC to hmsdms RA,DEC
    :param crd1: float RA 
    :param crd2: float DEC
    :param frame_in: string
    :param frame_out: string
    :return: strings
    """
    crd1 = float(crd1)
    crd2 = float(crd2)
    c = SkyCoord(ra=crd1 * u.degree, dec=crd2 * u.degree, frame=frame_in)
    c = change_frame(c, frame_in, frame_out)
    RA = c.ra.to_string('h', sep=':', alwayssign=False, precision=1, pad=True)
    DEC = c.dec.to_string(sep=':', alwayssign=False, precision=2, pad=True)
    return RA, DEC


def hmsdms2gal(crd1, crd2, frame_in='fk5', frame_out='fk5'):
    """
    Convert hmsdms coordinates to galactic
    :param crd1: RA HMS
    :param crd2: DEC DMS
    :param frame_in: in coordinate frame
    :param frame_out: out coordinate frame
    :return: Glon, Glat
    """
    ra, dec = hmsdms2radec(crd1, crd2, frame_in, frame_out)
    glon, glat = radec2gal(ra, dec, frame_out)
    return glon, glat


def coord2pix(inImage, Xcoord, Ycoord):
    """
    Convert astronomical coordinates to pixel coordinates in an fits image
    :param inImage: string, full path to the image
    :param Xcoord: astro X coordinate
    :param Ycoord: astro Y coordinate
    :return: X,Y in pixels
    """
    hdulist = fits.open(inImage)
    w = wcs.WCS(hdulist[0].header)
    Xpix, Ypix = w.wcs_world2pix([[Xcoord, Ycoord]], 1)[0].astype(int)
    Xpix = Xpix + 1
    Ypix = Ypix + 1
    return Xpix, Ypix


def pix2coord(inImage, Xcoord, Ycoord, hduid=0):
    """
    Convert pixel coordinates in an fits image to RA, DEC (J2000)
    :param inImage: string, full path to the image
    :param Xcoord: X coordinate in pixels
    :param Ycoord: Y coordinate in pixels
    :param hduid: hdu id (0 by default)
    :return: RA, DEC (J2000 decimal)
    NOTE: on error this funcion will resort to "oldpix2coord" function
    """
    try:
        ra, dec = montages.pix2coord(inImage, Xcoord, Ycoord)
        if math.isnan(float(ra)) or math.isnan(float(dec)) or not isinstance(ra, float) or not isinstance(dec, float):
            ra, dec = oldpix2coord(inImage, Xcoord, Ycoord, hduid)
    except:
        ra, dec = oldpix2coord(inImage, Xcoord, Ycoord, hduid)
    return ra, dec


def natural_coords(inImage, RA, DEC, hdrNo=0):
    """
    Convert RA, DEC (J2000 decimal) to natural coordinate of the input fits image
    :param inImage: full path to the in fits image
    :param RA: RA (J2000, decimal)
    :param DEC: DEC (J2000, decimal)
    :param hdrNo: hdr no
    :return: natural coordinates (X,Y) or False on error
    """
    hdr = getheader(inImage, hdrNo)
    tmp_coordinates_1 = str(hdr['CTYPE1']).split("-")
    tmp_coordinates_2 = str(hdr['CTYPE2']).split("-")
    coordinates_1 = tmp_coordinates_1[0]
    coordinates_2 = tmp_coordinates_2[0]
    if coordinates_1 == 'RA' and coordinates_2 == 'DEC':
        return float(RA), float(DEC)
    elif coordinates_1 == 'GLON' and coordinates_2 == 'GLAT':
        return radec2gal(float(RA), float(DEC))
    return False


def change_frame(c, frame_in, frame_out):
    """
    Change frame
    :param c: SkyCoords object
    :param frame_in: string
    :param frame_out: string
    :return: SkyCoords object in frame_out
    """
    if frame_in != frame_out:
        c = getattr(c, frame_out)
    return c


def radec2gal(RA, DEC, frame_in='fk5'):
    """
    Convert RA/DEC (deg) to Galactic
    :param RA: RA
    :param DEC: DEC
    :param frame_in: frame of RA/DEC
    :return: Glat, Glon
    """
    RA = float(RA)
    DEC = float(DEC)
    c = SkyCoord(ra=RA * u.degree, dec=DEC * u.degree, frame=frame_in)
    galactic = c.galactic
    return galactic.l.deg, galactic.b.deg


def oldpix2coord(inImage, Xcoord, Ycoord, hduid=0):
    """
    OLD Convert pixel coordinates in an fits image to RA, DEC (J2000)
    :param inImage: string, full path to the image
    :param Xcoord: X coordinate in pixels
    :param Ycoord: Y coordinate in pixels
    :param hduid: hdu id (0 by default)
    :return: RA, DEC (J2000 deg)
    """
    hdu = fits.open(inImage)
    w = wcs.WCS(hdu[hduid].header)
    ra, dec = w.all_pix2world(int(Xcoord) - 1, int(Ycoord) - 1, ra_dec_order=True)
    return float(ra), float(dec)


def hmsdms2radec(RAhms, DEChms, frame_in='fk5', frame_out='fk5'):
    """
    Convert RA/DEC (hmsdms) to RA/DEC (deg)
    :param RAhms: string
    :param DEChms: string
    :param frame_in: frame of hmsdms coordinates
    :param frame_out: frame of out coordinates
    :return: RA,DEC (deg)
    """
    c = SkyCoord("%s %s" % (RAhms, DEChms), unit=(u.hourangle, u.deg), frame=frame_in)
    c = change_frame(c, frame_in, frame_out)
    ra = c.ra.deg
    dec = c.dec.deg
    return ra, dec


def radec_separation(x1, y1, x2, y2, unit='arcsec'):
    '''
    FInd angular separation between two radec coordinates
    :param x1: float/string first coordinate DRAJ2000 deg
    :param y1: float/string first coordinate DDECJ2000 deg
    :param x2: float/string second coordinate DRAJ2000 deg
    :param y2: float/string second coordinate DDECJ2000 deg
    :return: float separation in arcsec
    '''
    c1 = SkyCoord(ra=float(x1) * u.degree, dec=float(y1) * u.degree, frame='fk5')
    c2 = SkyCoord(ra=float(x2) * u.degree, dec=float(y2) * u.degree, frame='fk5')
    sep = c1.separation(c2)
    if unit == 'arcsec':
        res = sep.arcsecond
    elif unit == 'arcdeg':
        res = sep.degree
    return res


def radstoarcsecs(value):
    """
    convert radians to arcseconds
    @param value: float radians
    @return: float arcseconds
    """
    x = value * u.rad
    arcs = x.to(u.arcsec)
    return arcs.value


#
#
#
#
#
#
#
#
#
#
#
#
#
#
# def angular_distance(Xcoord1, Ycoord1, frame1, Xcoord2, Ycoord2, frame2):
#     c1 = SkyCoord(Xcoord1, Ycoord1, unit=(u.deg, u.deg), frame=frame1)
#     c2 = SkyCoord(Xcoord2, Ycoord2, unit=(u.deg, u.deg), frame=frame2)
#     return c1.separation(c2).arcsecond
#
# # ========== UNUSED ?? ====================
#
# def coord2pixMIR(inImage, Xcoord, Ycoord):
#     from mirpy import miriad
#
#     tmp_reg = "/tmp/" + id_generator() + 'tmpreg.mir'
#     miriad.fits(IN=inImage, out=tmp_reg, op='xyin')
#     coordstr = '%s,%s' % (Xcoord, Ycoord)
#     result = miriad.impos(IN=tmp_reg, coord=coordstr, type='absdeg,absdeg')
#     if os.path.exists(tmp_reg): rmtree(tmp_reg)
#     return result
#
