import shutil
import ibcommon.parse as ibparse
import ibcommon.strings as ibstrings
import casatasks as ct
from astropy import units as u
from astropy.coordinates import Longitude, Latitude, SkyCoord
from math import isnan

## run CASA imstat
## https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_imstat/about
def run_imstat(in_image, in_region, mask=''):
    tmp_stats = ct.imstat(imagename=in_image, region=in_region, mask=mask)
    res = {}
    ## convert single vectors (i.e. [4]) into values
    if not tmp_stats:
        return False
    for i in tmp_stats:
        res[i] = ibparse.singlevec(tmp_stats[i])
    if res['npts'] == 0:
        res = False
    return res


## create residual image
def create_residual(tmpmodel, total, ann_region):
    tmpfile = ibstrings.generic_file(".","im")

    bckg = run_imstat(in_image=tmpmodel, in_region=ann_region)
    # TODO
    # check what is the level for background subtr
    expression = 'IM0-{}'.format(bckg['mean'])
    ct.immath(imagename=tmpmodel, mode='evalexpr', outfile=tmpfile,
           expr=expression)
    shutil.rmtree(tmpmodel)
    shutil.move(tmpfile, tmpmodel)
    expression = 'IM1-IM0'
    ct.immath(imagename=[tmpmodel, total], mode='evalexpr', outfile=tmpfile,
           expr=expression)
    shutil.rmtree(total)
    shutil.move(tmpfile, total)


# run CASA imfit
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_imfit/about
def run_imfit(fitinput):
    # run imfit
    tmpfitresults = ct.imfit(**fitinput)
    # check original imfit False return
    if not tmpfitresults:
        return False

    # attempt to create one dimensional dictionary from
    # the imfit output if fails return False
    try:
        results = {}
        for i in tmpfitresults:
            results[i] = ibparse.singlevec(tmpfitresults[i])
    except:
        print('NaNs in fit results')
        return False

    # if fit didn't converge return False
    if not results['converged']:
        return False

    return results


# prepare CASA imfit estimates file
def write_estimates(**inp):

    estimate_line = "{},{},{},{}arcsec,{}arcsec,{}deg,{}\n". \
        format(inp['peak'], inp['peak_x'], inp['peak_y'], inp['maj_axis'],
               inp['min_axis'], inp['bpa'], inp['fixed'])

    with open(inp['estimates_file'], "w") as open_file:
        open_file.write(estimate_line)


def subtract_bckgs(bckg_list, imfile, aperture):
    # bckg_list = self.obs.bckg
    # imfile = self.imfile
    # aperture = self.obs.pars['use_bmaj']
    # no background sources for subtraction
    if len(bckg_list) == 0:
        return

    for bckg in bckg_list:
        tmpimfile = ibstrings.generic_file(".","im")
        tmpmodel = ibstrings.generic_file(".","im")

        shutil.move(imfile, tmpimfile)
        bckg_reg = 'circle[[{}deg,{}deg],{}arcsec]'.format(bckg[0], bckg[1], aperture)

        fit_input = {'imagename': tmpimfile,
                     'region': bckg_reg,
                     'dooff': True,
                     'model': tmpmodel
                     }
        fit_res = run_imfit(fit_input)
        try:
            if fit_res['converged']:
                parsed = parse_fit(fit_res)
                c1 = SkyCoord(ra=bckg[0] * u.degree, dec=bckg[1] * u.degree, frame='fk5')
                c2 = SkyCoord(ra=parsed['fit_posra'] * u.degree, dec=parsed['fit_posdec'] * u.degree, frame='fk5')
                sep = c1.separation(c2)
                if sep.arcsecond < 0.5 * aperture and \
                        parsed['peak'] > 0 and parsed['maj_ax'] / aperture < 2.:
                    res_ann = 'annulus[[{}deg,{}deg],[{}arcsec,{}arcsec]]'.format(parsed['fit_posra'],
                                                                                  parsed['fit_posdec'],
                                                                                  5. * parsed['maj_ax'],
                                                                                  6. * parsed['maj_ax'])
                create_residual(tmpmodel, tmpimfile, res_ann)
        except:
            print('not converged')

        shutil.move(tmpimfile, imfile)
        shutil.rmtree(tmpmodel)
    # do background statistics again

# parse fit results from imfit
def parse_fit(fit_res):

    imfit_out_component = fit_res['results']['component0']

    flux = imfit_out_component['flux']
    peak = imfit_out_component['peak']
    shape = imfit_out_component['shape']
    parsed = {
        'flux_err': flux['error'][0],
        'flux_unit': flux['unit'],
        'flux': flux['value'][0],
        'peak_err': peak['error'],
        'peak_unit': peak['unit'],
        'peak': peak['value'],
        'm0': shape['direction']['m0']['value'],
        'm1': shape['direction']['m1']['value'],
        'fit_posra': Longitude(shape['direction']['m0']['value'], u.rad).deg,
        'fit_posdec': Latitude(shape['direction']['m1']['value'], u.rad).deg,
        'off_lat': shape['direction']['error']['latitude']['value'],
        'off_lat_unit': shape['direction']['error']['latitude']['unit'],
        'off_long': shape['direction']['error']['longitude']['value'],
        'off_long_unit': shape['direction']['error']['longitude']['unit'],
        'maj_ax': shape['majoraxis']['value'],
        'maj_ax_unit': shape['majoraxis']['unit'],
        'maj_ax_err': shape['majoraxiserror']['value'],
        'maj_ax_err_unit': shape['majoraxiserror']['unit'],
        'min_ax': shape['minoraxis']['value'],
        'min_ax_unit': shape['minoraxis']['unit'],
        'min_ax_err': shape['minoraxiserror']['value'],
        'min_ax_err_unit': shape['minoraxiserror']['unit'],
        'pa': shape['positionangle']['value'],
        'pa_unit': shape['positionangle']['unit'],
        'pa_err': shape['positionangleerror']['value'],
        'pa_err_unit': shape['positionangleerror']['unit'],
        'type': shape['type'],
        'conv': ibparse.booltostring(fit_res['converged']),
        'ispoint': ibparse.booltostring(imfit_out_component['ispoint']),
        'peak_x': int(round(imfit_out_component['pixelcoords'][0])),
        'peak_y': int(round(imfit_out_component['pixelcoords'][0]))

    }
    check_nans = ['flux_err', 'flux', 'peak_err', 'peak', 'maj_ax', 'maj_ax_err', 'min_ax', 'min_ax_err', 'pa',
                  'pa_err', 'off_lat', 'off_long']
    for check_nan in check_nans:
        if isnan(parsed[check_nan]):
            del parsed[check_nan]
    return parsed


def make_annulus(dra,ddec,r_in,r_out):
    return 'annulus[[{}deg,{}deg],[{}arcsec,{}arcsec]]'.format(dra,ddec,r_in,r_out)

def make_aperture(dra,ddec,r):
    return 'circle[[{}deg,{}deg],{}arcsec]'.format(dra,ddec,r)