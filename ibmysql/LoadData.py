import ibmysql.models.MainGPN as MainGPN
import ibmysql.models.MCs as MCs
import ibmysql.models.Gpnfullview as View


class LoadData:

    def __init__(self, db="view", table='view'):
        self.db = db
        self.table = table

        self.idpnmain = None

    @property
    def db(self):
        return self._db

    @db.setter
    def db(self, db):
        if db == "MainGPN":
            self._db = MainGPN
        elif db == "MCs":
            self._db = MCs
        elif db == "view":
            self._db = View

    @property
    def table(self):
        return self._table

    @table.setter
    def table(self, table):
        if table == "view":
            self._table = self.db.Gpnfullview
        else:
            self._table = getattr(self.db, table)

    @property
    def idpnmain(self):
        return self._idpnmain

    @idpnmain.setter
    def idpnmain(self, idpnmain):
        self._idpnmain = idpnmain

    def do_select(self,cols):
        lst = []
        for col in cols:
            lst.append(getattr(self.table, col))
        return self.table.select(*lst)

    @staticmethod
    def get_object_full(idpnmain):
        table = View.Gpnfullview
        return table.select().where(table.idpnmain == idpnmain)[0]
