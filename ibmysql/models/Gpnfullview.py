import ibcommon.parse as ibparse
from peewee import *

mysql_ini = ibparse.parse_ini_arguments('/etc/.pymysql-ini', 'mysqldb')
db_MainGPN_view = MySQLDatabase('MainGPN', **mysql_ini)
db_MainGPN_view.connect()


class UnknownField(object):
    def __init__(self, *_, **__): pass


class BaseModel(Model):
    class Meta:
        database = db_MainGPN_view


class Gpnfullview(BaseModel):
    cs_ddecj2000 = DecimalField(column_name='CS_DDECJ2000', null=True)
    cs_decj2000 = CharField(column_name='CS_DECJ2000', null=True)
    cs_draj2000 = DecimalField(column_name='CS_DRAJ2000', null=True)
    cs_glat = DecimalField(column_name='CS_Glat', null=True)
    cs_glon = DecimalField(column_name='CS_Glon', null=True)
    cs_raj2000 = CharField(column_name='CS_RAJ2000', null=True)
    csstat = CharField(column_name='CSstat', constraints=[SQL("DEFAULT 'p'")], null=True)
    catalogue = CharField(column_name='Catalogue', null=True)
    ddecj2000 = DecimalField(column_name='DDECJ2000', null=True)
    decj2000 = CharField(column_name='DECJ2000', null=True)
    draj2000 = DecimalField(column_name='DRAJ2000', null=True)
    epa = DecimalField(column_name='EPA', null=True)
    gpa = DecimalField(column_name='GPA', null=True)
    glat = DecimalField(column_name='Glat', null=True)
    glon = DecimalField(column_name='Glon', null=True)
    h_rad_vel = FloatField(column_name='H_rad_vel', null=True)
    majdiam = DecimalField(column_name='MajDiam', null=True)
    majext = DecimalField(column_name='MajExt', null=True)
    mindiam = DecimalField(column_name='MinDiam', null=True)
    name = CharField(column_name='Name', null=True)
    padiam = FloatField(column_name='PAdiam', null=True)
    png = CharField(column_name='PNG', null=True)
    pnstat = CharField(column_name='PNstat', null=True)
    pnstatus = CharField(column_name='PNstatus', null=True)
    raj2000 = CharField(column_name='RAJ2000', null=True)
    simbadid = CharField(column_name='SimbadID', null=True)
    domain = CharField(constraints=[SQL("DEFAULT 'Galaxy'")])
    errmajdiam = DecimalField(column_name='errMajDiam', null=True)
    errmindiam = DecimalField(column_name='errMinDiam', null=True)
    errpa = DecimalField(column_name='errPA', null=True)
    errpadiam = FloatField(column_name='errPAdiam', null=True)
    errlogflux = FloatField(column_name='errlogFlux', null=True)
    flaglogflux = CharField(column_name='flaglogFlux', null=True)
    idpnmain = IntegerField(column_name='idPNMain', constraints=[SQL("DEFAULT 0")])
    instrument = CharField(null=True)
    logflux = FloatField(column_name='logFlux', null=True)
    mainclass = CharField(column_name='mainClass', null=True)
    refcoord = CharField(column_name='refCoord', constraints=[SQL("DEFAULT '-'")], null=True)
    ref_tbirflux_100um = CharField(column_name='ref_tbIRFlux_100um', null=True)
    ref_tbirflux_12um = CharField(column_name='ref_tbIRFlux_12um', null=True)
    ref_tbirflux_160um = CharField(column_name='ref_tbIRFlux_160um', null=True)
    ref_tbirflux_22um = CharField(column_name='ref_tbIRFlux_22um', null=True)
    ref_tbirflux_24um = CharField(column_name='ref_tbIRFlux_24um', null=True)
    ref_tbirflux_250um = CharField(column_name='ref_tbIRFlux_250um', null=True)
    ref_tbirflux_25um = CharField(column_name='ref_tbIRFlux_25um', null=True)
    ref_tbirflux_350um = CharField(column_name='ref_tbIRFlux_350um', null=True)
    ref_tbirflux_500um = CharField(column_name='ref_tbIRFlux_500um', null=True)
    ref_tbirflux_60um = CharField(column_name='ref_tbIRFlux_60um', null=True)
    ref_tbirflux_70um = CharField(column_name='ref_tbIRFlux_70um', null=True)
    ref_tbirflux_8um = CharField(column_name='ref_tbIRFlux_8um', null=True)
    ref_tbirflux_h = CharField(column_name='ref_tbIRFlux_H', null=True)
    ref_tbirflux_j = CharField(column_name='ref_tbIRFlux_J', null=True)
    ref_tbirflux_ks = CharField(column_name='ref_tbIRFlux_Ks', null=True)
    ref_tbirmag_h = CharField(column_name='ref_tbIRMag_H', null=True)
    ref_tbirmag_j = CharField(column_name='ref_tbIRMag_J', null=True)
    ref_tbirmag_ks = CharField(column_name='ref_tbIRMag_Ks', null=True)
    ref_tbirmag_y = CharField(column_name='ref_tbIRMag_Y', null=True)
    ref_tbirmag_z = CharField(column_name='ref_tbIRMag_Z', null=True)
    ref_tbradiocont_11cm = CharField(column_name='ref_tbRadioCont_11cm', null=True)
    ref_tbradiocont_13cm = CharField(column_name='ref_tbRadioCont_13cm', null=True)
    ref_tbradiocont_20cm = CharField(column_name='ref_tbRadioCont_20cm', null=True)
    ref_tbradiocont_2cm = CharField(column_name='ref_tbRadioCont_2cm', null=True)
    ref_tbradiocont_36cm = CharField(column_name='ref_tbRadioCont_36cm', null=True)
    ref_tbradiocont_3cm = CharField(column_name='ref_tbRadioCont_3cm', null=True)
    ref_tbradiocont_6cm = CharField(column_name='ref_tbRadioCont_6cm', null=True)
    reftbangdiam = CharField(column_name='reftbAngDiam', null=True)
    reftbangext = CharField(column_name='reftbAngExt', null=True)
    reftbcnames = CharField(column_name='reftbCNames', null=True)
    reftbcscoords = CharField(column_name='reftbCSCoords', null=True)
    reftbhrvel = CharField(column_name='reftbHrvel', null=True)
    reftbpa = CharField(column_name='reftbPA', null=True)
    reftbpnmorph = CharField(column_name='reftbPNMorph', null=True)
    reftblogfhalpha = CharField(column_name='reftblogFHalpha', null=True)
    show = CharField(constraints=[SQL("DEFAULT 'y'")], null=True)
    subclass = CharField(column_name='subClass', null=True)
    tbirflux_flux_100um = FloatField(column_name='tbIRFlux_Flux_100um', null=True)
    tbirflux_flux_12um = FloatField(column_name='tbIRFlux_Flux_12um', null=True)
    tbirflux_flux_160um = FloatField(column_name='tbIRFlux_Flux_160um', null=True)
    tbirflux_flux_22um = FloatField(column_name='tbIRFlux_Flux_22um', null=True)
    tbirflux_flux_24um = FloatField(column_name='tbIRFlux_Flux_24um', null=True)
    tbirflux_flux_250um = FloatField(column_name='tbIRFlux_Flux_250um', null=True)
    tbirflux_flux_25um = FloatField(column_name='tbIRFlux_Flux_25um', null=True)
    tbirflux_flux_350um = FloatField(column_name='tbIRFlux_Flux_350um', null=True)
    tbirflux_flux_500um = FloatField(column_name='tbIRFlux_Flux_500um', null=True)
    tbirflux_flux_60um = FloatField(column_name='tbIRFlux_Flux_60um', null=True)
    tbirflux_flux_70um = FloatField(column_name='tbIRFlux_Flux_70um', null=True)
    tbirflux_flux_8um = FloatField(column_name='tbIRFlux_Flux_8um', null=True)
    tbirflux_flux_h = FloatField(column_name='tbIRFlux_Flux_H', null=True)
    tbirflux_flux_j = FloatField(column_name='tbIRFlux_Flux_J', null=True)
    tbirflux_flux_ks = FloatField(column_name='tbIRFlux_Flux_Ks', null=True)
    tbirflux_errflux_100um = FloatField(column_name='tbIRFlux_errFlux_100um', null=True)
    tbirflux_errflux_12um = FloatField(column_name='tbIRFlux_errFlux_12um', null=True)
    tbirflux_errflux_160um = FloatField(column_name='tbIRFlux_errFlux_160um', null=True)
    tbirflux_errflux_22um = FloatField(column_name='tbIRFlux_errFlux_22um', null=True)
    tbirflux_errflux_24um = FloatField(column_name='tbIRFlux_errFlux_24um', null=True)
    tbirflux_errflux_250um = FloatField(column_name='tbIRFlux_errFlux_250um', null=True)
    tbirflux_errflux_25um = FloatField(column_name='tbIRFlux_errFlux_25um', null=True)
    tbirflux_errflux_350um = FloatField(column_name='tbIRFlux_errFlux_350um', null=True)
    tbirflux_errflux_500um = FloatField(column_name='tbIRFlux_errFlux_500um', null=True)
    tbirflux_errflux_60um = FloatField(column_name='tbIRFlux_errFlux_60um', null=True)
    tbirflux_errflux_70um = FloatField(column_name='tbIRFlux_errFlux_70um', null=True)
    tbirflux_errflux_8um = FloatField(column_name='tbIRFlux_errFlux_8um', null=True)
    tbirflux_errflux_h = FloatField(column_name='tbIRFlux_errFlux_H', null=True)
    tbirflux_errflux_j = FloatField(column_name='tbIRFlux_errFlux_J', null=True)
    tbirflux_errflux_ks = FloatField(column_name='tbIRFlux_errFlux_Ks', null=True)
    tbirflux_flagflux_100um = CharField(column_name='tbIRFlux_flagFlux_100um', null=True)
    tbirflux_flagflux_12um = CharField(column_name='tbIRFlux_flagFlux_12um', null=True)
    tbirflux_flagflux_160um = CharField(column_name='tbIRFlux_flagFlux_160um', null=True)
    tbirflux_flagflux_22um = CharField(column_name='tbIRFlux_flagFlux_22um', null=True)
    tbirflux_flagflux_24um = CharField(column_name='tbIRFlux_flagFlux_24um', null=True)
    tbirflux_flagflux_250um = CharField(column_name='tbIRFlux_flagFlux_250um', null=True)
    tbirflux_flagflux_25um = CharField(column_name='tbIRFlux_flagFlux_25um', null=True)
    tbirflux_flagflux_350um = CharField(column_name='tbIRFlux_flagFlux_350um', null=True)
    tbirflux_flagflux_500um = CharField(column_name='tbIRFlux_flagFlux_500um', null=True)
    tbirflux_flagflux_60um = CharField(column_name='tbIRFlux_flagFlux_60um', null=True)
    tbirflux_flagflux_70um = CharField(column_name='tbIRFlux_flagFlux_70um', null=True)
    tbirflux_flagflux_8um = CharField(column_name='tbIRFlux_flagFlux_8um', null=True)
    tbirflux_flagflux_h = CharField(column_name='tbIRFlux_flagFlux_H', null=True)
    tbirflux_flagflux_j = CharField(column_name='tbIRFlux_flagFlux_J', null=True)
    tbirflux_flagflux_ks = CharField(column_name='tbIRFlux_flagFlux_Ks', null=True)
    tbirflux_instrument_100um = CharField(column_name='tbIRFlux_instrument_100um', null=True)
    tbirflux_instrument_12um = CharField(column_name='tbIRFlux_instrument_12um', null=True)
    tbirflux_instrument_160um = CharField(column_name='tbIRFlux_instrument_160um', null=True)
    tbirflux_instrument_22um = CharField(column_name='tbIRFlux_instrument_22um', null=True)
    tbirflux_instrument_24um = CharField(column_name='tbIRFlux_instrument_24um', null=True)
    tbirflux_instrument_250um = CharField(column_name='tbIRFlux_instrument_250um', null=True)
    tbirflux_instrument_25um = CharField(column_name='tbIRFlux_instrument_25um', null=True)
    tbirflux_instrument_350um = CharField(column_name='tbIRFlux_instrument_350um', null=True)
    tbirflux_instrument_500um = CharField(column_name='tbIRFlux_instrument_500um', null=True)
    tbirflux_instrument_60um = CharField(column_name='tbIRFlux_instrument_60um', null=True)
    tbirflux_instrument_70um = CharField(column_name='tbIRFlux_instrument_70um', null=True)
    tbirflux_instrument_8um = CharField(column_name='tbIRFlux_instrument_8um', null=True)
    tbirflux_instrument_h = CharField(column_name='tbIRFlux_instrument_H', null=True)
    tbirflux_instrument_j = CharField(column_name='tbIRFlux_instrument_J', null=True)
    tbirflux_instrument_ks = CharField(column_name='tbIRFlux_instrument_Ks', null=True)
    tbirmag_mag_h = FloatField(column_name='tbIRMag_Mag_H', null=True)
    tbirmag_mag_j = FloatField(column_name='tbIRMag_Mag_J', null=True)
    tbirmag_mag_ks = FloatField(column_name='tbIRMag_Mag_Ks', null=True)
    tbirmag_mag_y = FloatField(column_name='tbIRMag_Mag_Y', null=True)
    tbirmag_mag_z = FloatField(column_name='tbIRMag_Mag_Z', null=True)
    tbirmag_errmag_h = FloatField(column_name='tbIRMag_errMag_H', null=True)
    tbirmag_errmag_j = FloatField(column_name='tbIRMag_errMag_J', null=True)
    tbirmag_errmag_ks = FloatField(column_name='tbIRMag_errMag_Ks', null=True)
    tbirmag_errmag_y = FloatField(column_name='tbIRMag_errMag_Y', null=True)
    tbirmag_errmag_z = FloatField(column_name='tbIRMag_errMag_Z', null=True)
    tbirmag_flagmag_h = CharField(column_name='tbIRMag_flagMag_H', null=True)
    tbirmag_flagmag_j = CharField(column_name='tbIRMag_flagMag_J', null=True)
    tbirmag_flagmag_ks = CharField(column_name='tbIRMag_flagMag_Ks', null=True)
    tbirmag_flagmag_y = CharField(column_name='tbIRMag_flagMag_Y', null=True)
    tbirmag_flagmag_z = CharField(column_name='tbIRMag_flagMag_Z', null=True)
    tbirmag_instrument_h = CharField(column_name='tbIRMag_instrument_H', null=True)
    tbirmag_instrument_j = CharField(column_name='tbIRMag_instrument_J', null=True)
    tbirmag_instrument_ks = CharField(column_name='tbIRMag_instrument_Ks', null=True)
    tbirmag_instrument_y = CharField(column_name='tbIRMag_instrument_Y', null=True)
    tbirmag_instrument_z = CharField(column_name='tbIRMag_instrument_Z', null=True)
    tbradiocont_flux_11cm = FloatField(column_name='tbRadioCont_Flux_11cm', null=True)
    tbradiocont_flux_13cm = FloatField(column_name='tbRadioCont_Flux_13cm', null=True)
    tbradiocont_flux_20cm = FloatField(column_name='tbRadioCont_Flux_20cm', null=True)
    tbradiocont_flux_2cm = FloatField(column_name='tbRadioCont_Flux_2cm', null=True)
    tbradiocont_flux_36cm = FloatField(column_name='tbRadioCont_Flux_36cm', null=True)
    tbradiocont_flux_3cm = FloatField(column_name='tbRadioCont_Flux_3cm', null=True)
    tbradiocont_flux_6cm = FloatField(column_name='tbRadioCont_Flux_6cm', null=True)
    tbradiocont_errflux_11cm = FloatField(column_name='tbRadioCont_errFlux_11cm', null=True)
    tbradiocont_errflux_13cm = FloatField(column_name='tbRadioCont_errFlux_13cm', null=True)
    tbradiocont_errflux_20cm = FloatField(column_name='tbRadioCont_errFlux_20cm', null=True)
    tbradiocont_errflux_2cm = FloatField(column_name='tbRadioCont_errFlux_2cm', null=True)
    tbradiocont_errflux_36cm = FloatField(column_name='tbRadioCont_errFlux_36cm', null=True)
    tbradiocont_errflux_3cm = FloatField(column_name='tbRadioCont_errFlux_3cm', null=True)
    tbradiocont_errflux_6cm = FloatField(column_name='tbRadioCont_errFlux_6cm', null=True)
    tbradiocont_flagflux_11cm = CharField(column_name='tbRadioCont_flagFlux_11cm', null=True)
    tbradiocont_flagflux_13cm = CharField(column_name='tbRadioCont_flagFlux_13cm', null=True)
    tbradiocont_flagflux_20cm = CharField(column_name='tbRadioCont_flagFlux_20cm', null=True)
    tbradiocont_flagflux_2cm = CharField(column_name='tbRadioCont_flagFlux_2cm', null=True)
    tbradiocont_flagflux_36cm = CharField(column_name='tbRadioCont_flagFlux_36cm', null=True)
    tbradiocont_flagflux_3cm = CharField(column_name='tbRadioCont_flagFlux_3cm', null=True)
    tbradiocont_flagflux_6cm = CharField(column_name='tbRadioCont_flagFlux_6cm', null=True)
    tbradiocont_freq_11cm = FloatField(column_name='tbRadioCont_freq_11cm', null=True)
    tbradiocont_freq_13cm = FloatField(column_name='tbRadioCont_freq_13cm', null=True)
    tbradiocont_freq_20cm = FloatField(column_name='tbRadioCont_freq_20cm', null=True)
    tbradiocont_freq_2cm = FloatField(column_name='tbRadioCont_freq_2cm', null=True)
    tbradiocont_freq_36cm = FloatField(column_name='tbRadioCont_freq_36cm', null=True)
    tbradiocont_freq_3cm = FloatField(column_name='tbRadioCont_freq_3cm', null=True)
    tbradiocont_freq_6cm = FloatField(column_name='tbRadioCont_freq_6cm', null=True)
    tbradiocont_instrument_11cm = CharField(column_name='tbRadioCont_instrument_11cm', null=True)
    tbradiocont_instrument_13cm = CharField(column_name='tbRadioCont_instrument_13cm', null=True)
    tbradiocont_instrument_20cm = CharField(column_name='tbRadioCont_instrument_20cm', null=True)
    tbradiocont_instrument_2cm = CharField(column_name='tbRadioCont_instrument_2cm', null=True)
    tbradiocont_instrument_36cm = CharField(column_name='tbRadioCont_instrument_36cm', null=True)
    tbradiocont_instrument_3cm = CharField(column_name='tbRadioCont_instrument_3cm', null=True)
    tbradiocont_instrument_6cm = CharField(column_name='tbRadioCont_instrument_6cm', null=True)
    vel_err = FloatField(null=True)

    class Meta:
        table_name = 'gpnfullview'
        primary_key = False
