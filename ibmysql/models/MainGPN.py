import ibcommon.parse as ibparse
from peewee import *

mysql_ini = ibparse.parse_ini_arguments('/etc/.pymysql-ini', 'mysqldb')
db_MainGPN = MySQLDatabase('MainGPN', **mysql_ini)
db_MainGPN.connect()


class UnknownField(object):
    def __init__(self, *_, **__): pass


class BaseModel(Model):
    class Meta:
        database = db_MainGPN


class ReferenceIDs(BaseModel):
    abstract = TextField(column_name='Abstract', null=True)
    author = TextField(column_name='Author', null=True)
    identifier = CharField(column_name='Identifier', unique=True)
    items = TextField(column_name='Items', null=True)
    journal = TextField(column_name='Journal', null=True)
    keywords = TextField(column_name='Keywords', null=True)
    origin = CharField(column_name='Origin', null=True)
    parsedate = CharField(column_name='ParseDate', null=True)
    score = FloatField(column_name='Score', null=True)
    title = TextField(column_name='Title', null=True)
    url = TextField(column_name='URL', null=True)
    year = IntegerField(column_name='Year', null=True)
    comments = TextField(null=True)
    elcatcode = CharField(column_name='elcatCode', null=True, unique=True)
    idreferenceids = IntegerField(column_name='idReferenceIDs', unique=True)
    parsedin = CharField(column_name='parsedIn', constraints=[SQL("DEFAULT 'n'")], null=True)
    user = CharField(column_name='user', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'ReferenceIDs'
        indexes = (
            (('idreferenceids', 'identifier'), True),
        )
        primary_key = CompositeKey('idreferenceids', 'identifier')


class ObjStatus(BaseModel):
    idobjstatus = AutoField(column_name='idobjStatus')
    order = IntegerField(null=True)
    statusdef = TextField(column_name='statusDef', null=True)
    statusid = CharField(column_name='statusId', unique=True)
    statusname = CharField(column_name='statusName', null=True, unique=True)
    statustitle = CharField(column_name='statusTitle', null=True, unique=True)

    class Meta:
        table_name = 'objStatus'


class PNMain(BaseModel):
    catalogue = ForeignKeyField(column_name='Catalogue', field='identifier', model=ReferenceIDs, null=True)
    ddecj2000 = DecimalField(column_name='DDECJ2000', null=True)
    decj2000 = CharField(column_name='DECJ2000', null=True)
    draj2000 = DecimalField(column_name='DRAJ2000', null=True)
    glat = DecimalField(column_name='Glat', null=True)
    glon = DecimalField(column_name='Glon', null=True)
    png = CharField(column_name='PNG', null=True, unique=True)
    pnmaincol = CharField(column_name='PNMaincol', null=True)
    pnstat = ForeignKeyField(column_name='PNstat', field='statusid', model=ObjStatus, null=True)
    raj2000 = CharField(column_name='RAJ2000', null=True)
    simbadid = CharField(column_name='SimbadID', null=True, unique=True)
    created_at = DateTimeField(null=True)
    domain = CharField(constraints=[SQL("DEFAULT 'Galaxy'")])
    idpnmain = AutoField(column_name='idPNMain')
    refcatalogue = CharField(column_name='refCatalogue', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refcoord = ForeignKeyField(backref='ReferenceIDs_ref_coord_set', column_name='refCoord',
                                constraints=[SQL("DEFAULT '-'")], field='identifier', model=ReferenceIDs, null=True)
    refdomain = CharField(column_name='refDomain', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refpng = CharField(column_name='refPNG', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refpnstat = CharField(column_name='refPNstat', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refsimbad = CharField(column_name='refSimbadID', constraints=[SQL("DEFAULT 'sys'")], null=True)
    show = CharField(constraints=[SQL("DEFAULT 'y'")], null=True)
    updated_at = DateTimeField(null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'PNMain'
        indexes = (
            (('refpng', 'refcoord', 'refcatalogue', 'refdomain', 'refpnstat', 'refsimbad'), False),
        )


class PNMainLog(BaseModel):
    catalogue = CharField(column_name='Catalogue')
    ddecj2000 = DecimalField(column_name='DDECJ2000', null=True)
    decj2000 = CharField(column_name='DECJ2000', null=True)
    draj2000 = DecimalField(column_name='DRAJ2000', null=True)
    glat = DecimalField(column_name='Glat', null=True)
    glon = DecimalField(column_name='Glon', null=True)
    png = CharField(column_name='PNG')
    pnstat = CharField(column_name='PNstat', constraints=[SQL("DEFAULT 'n'")])
    raj2000 = CharField(column_name='RAJ2000', null=True)
    action = CharField(null=True)
    date = DateTimeField(null=True)
    domain = CharField(constraints=[SQL("DEFAULT 'Galaxy'")])
    idpnmain = IntegerField(column_name='idPNMain')
    idlog = IntegerField(unique=True)
    refcatalogue = CharField(column_name='refCatalogue', constraints=[SQL("DEFAULT 'sys'")])
    refcoord = CharField(column_name='refCoord', constraints=[SQL("DEFAULT '-'")])
    refdomain = CharField(column_name='refDomain', constraints=[SQL("DEFAULT 'sys'")])
    refpng = CharField(column_name='refPNG', constraints=[SQL("DEFAULT 'sys'")])
    refpnstat = CharField(column_name='refPNstat', constraints=[SQL("DEFAULT 'sys'")])
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")])

    class Meta:
        table_name = 'PNMain_log'
        indexes = (
            (('idlog', 'idpnmain'), True),
        )
        primary_key = CompositeKey('idpnmain', 'idlog')


class FlagMap(BaseModel):
    flag = CharField(unique=True)
    flagexpl = TextField(column_name='flagExpl', null=True)
    idflagmap = AutoField(column_name='idflagMap')

    class Meta:
        table_name = 'flagMap'


class TbAngDiam(BaseModel):
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    majdiam = DecimalField(column_name='MajDiam')
    mindiam = DecimalField(column_name='MinDiam', null=True)
    padiam = FloatField(column_name='PAdiam', null=True)
    comments = TextField(null=True)
    errmajdiam = DecimalField(column_name='errMajDiam', null=True)
    errmindiam = DecimalField(column_name='errMinDiam', null=True)
    errpadiam = FloatField(column_name='errPAdiam', null=True)
    flagmajdiam = ForeignKeyField(column_name='flagMajDiam', field='flag', model=FlagMap, null=True)
    flagmindiam = ForeignKeyField(backref='flagMap_flag_min_diam_set', column_name='flagMinDiam', field='flag',
                                    model=FlagMap, null=True)
    flagpadiam = ForeignKeyField(backref='flagMap_flag_p_adiam_set', column_name='flagPAdiam', field='flag',
                                   model=FlagMap, null=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain)
    idtbangdiam = IntegerField(column_name='idtbAngDiam', unique=True)
    refinuse = CharField(column_name='refInUse', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refrecord = IntegerField(column_name='refRecord', null=True)
    reftable = CharField(column_name='refTable', null=True)
    reference = ForeignKeyField(column_name='reference', field='identifier', model=ReferenceIDs, null=True)
    tempflag = CharField(constraints=[SQL("DEFAULT 'n'")], null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'tbAngDiam'
        indexes = (
            (('idtbangdiam', 'idpnmain'), True),
        )
        primary_key = CompositeKey('idpnmain', 'idtbangdiam')


class PnMainTbAngDiam(BaseModel):
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, primary_key=True)
    idtbangdiam = ForeignKeyField(column_name='idtbAngDiam', field='idtbangdiam', model=TbAngDiam)

    class Meta:
        table_name = 'PNMain_tbAngDiam'


class TbAngExt(BaseModel):
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    majext = DecimalField(column_name='MajExt')
    band = CharField(null=True)
    comments = CharField(null=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain)
    idtbangext = IntegerField(column_name='idtbAngExt', unique=True)
    refinuse = CharField(column_name='refInUse', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refrecord = IntegerField(column_name='refRecord', index=True, null=True)
    reftable = CharField(column_name='refTable', null=True)
    reference = ForeignKeyField(column_name='reference', field='identifier', model=ReferenceIDs, null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'tbAngExt'
        indexes = (
            (('idtbangext', 'idpnmain'), True),
        )
        primary_key = CompositeKey('idpnmain', 'idtbangext')


class PnMainTbAngExt(BaseModel):
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, primary_key=True)
    idtbangext = ForeignKeyField(column_name='idtbAngExt', field='idtbangext', model=TbAngExt)

    class Meta:
        table_name = 'PNMain_tbAngExt'


class TbCNames(BaseModel):
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    name = CharField(column_name='Name', unique=True)
    comments = TextField(null=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain)
    idtbcnames = IntegerField(column_name='idtbCNames', unique=True)
    refinuse = CharField(column_name='refInUse', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refrecord = IntegerField(column_name='refRecord', null=True)
    reftable = CharField(column_name='refTable', null=True)
    reference = ForeignKeyField(column_name='reference', field='identifier', model=ReferenceIDs, null=True)
    simbadid = CharField(column_name='simbadID', constraints=[SQL("DEFAULT 'n'")], null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'tbCNames'
        indexes = (
            (('idtbcnames', 'name', 'idpnmain'), True),
        )
        primary_key = CompositeKey('idpnmain', 'idtbcnames', 'name')


class PnMainTbCNames(BaseModel):
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, primary_key=True)
    idtbcnames = ForeignKeyField(column_name='idtbCNames', field='idtbcnames', model=TbCNames)

    class Meta:
        table_name = 'PNMain_tbCNames'


class TbCsCoords(BaseModel):
    cs_ddecj2000 = DecimalField(column_name='CS_DDECJ2000', null=True)
    cs_decj2000 = CharField(column_name='CS_DECJ2000', null=True)
    cs_draj2000 = DecimalField(column_name='CS_DRAJ2000', null=True)
    cs_glat = DecimalField(column_name='CS_Glat', null=True)
    cs_glon = DecimalField(column_name='CS_Glon', null=True)
    cs_raj2000 = CharField(column_name='CS_RAJ2000', null=True)
    csstat = CharField(column_name='CSstat', constraints=[SQL("DEFAULT 'p'")], null=True)
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    comments = TextField(null=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, null=True)
    idtbcscoords = AutoField(column_name='idtbCSCoords')
    refcsstat = CharField(column_name='refCSstat', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refinuse = CharField(column_name='refInUse', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refrecord = CharField(column_name='refRecord', null=True)
    reftable = CharField(column_name='refTable', null=True)
    reference = ForeignKeyField(column_name='reference', field='identifier', model=ReferenceIDs, null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'tbCSCoords'


class PnMainTbCsCoords(BaseModel):
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, primary_key=True)
    idtbcscoords = ForeignKeyField(column_name='idtbCSCoords', field='idtbcscoords', model=TbCsCoords)

    class Meta:
        table_name = 'PNMain_tbCSCoords'


class TbHrvel(BaseModel):
    h_rad_vel = FloatField(column_name='H_rad_vel', null=True)
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    comments = TextField(null=True)
    flagh_rad_vel = ForeignKeyField(column_name='flagH_rad_vel', field='flag', model=FlagMap, null=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', constraints=[SQL("DEFAULT 0")], field='idpnmain',
                                 model=PNMain)
    idtbhrvel = IntegerField(column_name='idtbHrvel', unique=True)
    refinuse = CharField(column_name='refInUse', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refrecord = IntegerField(column_name='refRecord', null=True)
    reftable = CharField(column_name='refTable', constraints=[SQL("DEFAULT '-'")], null=True)
    reference = ForeignKeyField(column_name='reference', field='identifier', model=ReferenceIDs, null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)
    vel_err = FloatField(null=True)

    class Meta:
        table_name = 'tbHrvel'
        indexes = (
            (('idtbhrvel', 'idpnmain'), True),
        )
        primary_key = CompositeKey('idpnmain', 'idtbhrvel')


class PnMainTbHrvel(BaseModel):
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, primary_key=True)
    idtbhrvel = ForeignKeyField(column_name='idtbHrvel', field='idtbhrvel', model=TbHrvel)

    class Meta:
        table_name = 'PNMain_tbHrvel'


class TbIrFlux(BaseModel):
    flux = FloatField(column_name='Flux', null=True)
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    band = CharField(null=True)
    comments = TextField(null=True)
    errflux = FloatField(column_name='errFlux', null=True)
    flagflux = ForeignKeyField(column_name='flagFlux', field='flag', model=FlagMap, null=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, null=True)
    idtbirflux = AutoField(column_name='idtbIRFlux')
    instrument = CharField(null=True)
    refinuse = CharField(column_name='refInUse', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refrecord = IntegerField(column_name='refRecord', null=True)
    reftable = CharField(column_name='refTable', constraints=[SQL("DEFAULT '-'")], null=True)
    reference = ForeignKeyField(column_name='reference', field='identifier', model=ReferenceIDs, null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'tbIRFlux'


class PnMainTbIrFlux(BaseModel):
    band = CharField()
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain)
    idtbirflux = ForeignKeyField(column_name='idtbIRFlux', field='idtbirflux', model=TbIrFlux, primary_key=True)

    class Meta:
        table_name = 'PNMain_tbIRFlux'


class TbIrMag(BaseModel):
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    mag = FloatField(column_name='Mag', null=True)
    band = CharField(null=True)
    comments = TextField(null=True)
    errmag = FloatField(column_name='errMag', null=True)
    flagmag = ForeignKeyField(column_name='flagMag', field='flag', model=FlagMap, null=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, null=True)
    idtbirmag = AutoField(column_name='idtbIRMag')
    instrument = CharField(null=True)
    refinuse = CharField(column_name='refInUse', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refrecord = IntegerField(column_name='refRecord', null=True)
    reftable = CharField(column_name='refTable', constraints=[SQL("DEFAULT '-'")], null=True)
    reference = ForeignKeyField(column_name='reference', field='identifier', model=ReferenceIDs, null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'tbIRMag'


class PnMainTbIrMag(BaseModel):
    band = CharField()
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain)
    idtbirmag = ForeignKeyField(column_name='idtbIRMag', field='idtbirmag', model=TbIrMag, primary_key=True)

    class Meta:
        table_name = 'PNMain_tbIRMag'


class PnMainTbPa(BaseModel):
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, primary_key=True)
    idtbpa = IntegerField(column_name='idtbPA', index=True)

    class Meta:
        table_name = 'PNMain_tbPA'


class TbPnMorph(BaseModel):
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    comments = TextField(null=True)
    flagmorph = ForeignKeyField(column_name='flagMorph', field='flag', model=FlagMap, null=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, null=True)
    idtbpnmorph = AutoField(column_name='idtbPNMorph')
    mainclass = CharField(column_name='mainClass')
    refinuse = CharField(column_name='refInUse', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refrecord = IntegerField(column_name='refRecord', null=True)
    reftable = CharField(column_name='refTable', null=True)
    reference = ForeignKeyField(column_name='reference', field='identifier', model=ReferenceIDs, null=True)
    subclass = CharField(column_name='subClass', null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'tbPNMorph'


class PnMainTbPnMorph(BaseModel):
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, primary_key=True)
    idtbpnmorph = ForeignKeyField(column_name='idtbPNMorph', field='idtbpnmorph', model=TbPnMorph)

    class Meta:
        table_name = 'PNMain_tbPNMorph'


class PnMainTbRadioCont(BaseModel):
    band = CharField(index=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain)
    idtbradiocont = AutoField(column_name='idtbRadioCont')

    class Meta:
        table_name = 'PNMain_tbRadioCont'


class TblogFHalpha(BaseModel):
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    comments = TextField(null=True)
    errlogflux = FloatField(column_name='errlogFlux', null=True)
    flaglogflux = ForeignKeyField(column_name='flaglogFlux', field='flag', model=FlagMap, null=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, null=True)
    idtblogfhalpha = AutoField(column_name='idtblogFHalpha')
    instrument = CharField(null=True)
    logflux = FloatField(column_name='logFlux', null=True)
    refinuse = CharField(column_name='refInUse', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refrecord = IntegerField(column_name='refRecord', null=True)
    reftable = CharField(column_name='refTable', constraints=[SQL("DEFAULT '-'")], null=True)
    reference = ForeignKeyField(column_name='reference', field='identifier', model=ReferenceIDs, null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'tblogFHalpha'


class PnMainTblogFHalpha(BaseModel):
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, primary_key=True)
    idtblogfhalpha = ForeignKeyField(column_name='idtblogFHalpha', field='idtblogfhalpha', model=TblogFHalpha,
                                       unique=True)

    class Meta:
        table_name = 'PNMain_tblogFHalpha'


class TblogFoiii(BaseModel):
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    comments = TextField(null=True)
    errlogflux = FloatField(column_name='errlogFlux', null=True)
    flaglogflux = ForeignKeyField(column_name='flaglogFlux', field='flag', model=FlagMap, null=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, null=True)
    idtblogfoiii = AutoField(column_name='idtblogFOIII')
    instrument = CharField(null=True)
    logflux = FloatField(column_name='logFlux', null=True)
    refinuse = CharField(column_name='refInUse', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refrecord = IntegerField(column_name='refRecord', null=True)
    reftable = CharField(column_name='refTable', constraints=[SQL("DEFAULT '-'")], null=True)
    reference = ForeignKeyField(column_name='reference', field='identifier', model=ReferenceIDs, null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'tblogFOIII'


class PnMainTblogFoiii(BaseModel):
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, primary_key=True)
    idtblogfoiii = ForeignKeyField(column_name='idtblogFOIII', field='idtblogfoiii', model=TblogFoiii, unique=True)

    class Meta:
        table_name = 'PNMain_tblogFOIII'


class SimbadIDs(BaseModel):
    simbad = ForeignKeyField(column_name='SimbadID', field='name', model=TbCNames)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")])

    class Meta:
        table_name = 'SimbadIDs'
        indexes = (
            (('idpnmain', 'simbad'), True),
        )
        primary_key = CompositeKey('idpnmain', 'simbad')


class BandMap(BaseModel):
    band_id = CharField(null=True)
    band_name = CharField()
    comments = CharField(null=True)
    freq = FloatField(null=True)
    freq_unit = FloatField(null=True)
    freq_unit_name = CharField(null=True)
    instr = CharField(null=True)
    sets = CharField(null=True)
    type = CharField(null=True)
    wav_unit = FloatField(null=True)
    wav_unit_name = CharField(null=True)
    wavelength = FloatField(null=True)

    class Meta:
        table_name = 'bandMap'


class DataDictionary(BaseModel):
    columnname = CharField(column_name='columnName')
    columntype = CharField(column_name='columnType')
    comment = TextField(null=True)
    extracolumn = CharField(column_name='extraColumn', constraints=[SQL("DEFAULT 'n'")], null=True)
    fill_value = CharField()
    iddatadictionary = AutoField(column_name='iddataDictionary')
    maptoband = CharField(column_name='maptoBand', null=True)
    maptocolumn = CharField(column_name='maptoColumn', null=True)
    maptoconvert = CharField(column_name='maptoConvert', null=True)
    maptoconverttype = CharField(column_name='maptoConvertType', null=True)
    maptodatabase = CharField(column_name='maptoDatabase', null=True)
    maptotable = CharField(column_name='maptoTable', null=True)
    maptotype = CharField(column_name='maptoType', null=True)
    recordgroup = IntegerField(column_name='recordGroup', null=True)
    sourcepaper = CharField(column_name='sourcePaper')
    viziercatalogue = CharField(column_name='vizierCatalogue')
    viziertable = CharField(column_name='vizierTable')

    class Meta:
        table_name = 'dataDictionary'


class Helppages(BaseModel):
    date = DateTimeField(null=True)
    idhelppages = AutoField()
    order = IntegerField(null=True)
    text = TextField(null=True)
    title = CharField(null=True)
    topic = CharField(null=True, unique=True)
    user = CharField(null=True)

    class Meta:
        table_name = 'helppages'


class ObjDomain(BaseModel):
    domaindef = TextField(column_name='domainDef', null=True)
    domainid = CharField(column_name='domainId')
    domainname = CharField(column_name='domainName', null=True)
    domaintitle = CharField(column_name='domainTitle', null=True)
    idobjdomain = AutoField(column_name='idobjDomain')

    class Meta:
        table_name = 'objDomain'


class SamplesInfo(BaseModel):
    name = CharField(column_name='Name', index=True)
    user = CharField(column_name='User', constraints=[SQL("DEFAULT 'sys'")], null=True)
    canadd = CharField(column_name='canAdd', constraints=[SQL("DEFAULT 'n'")])
    class_ = CharField(column_name='class')
    color = CharField(constraints=[SQL("DEFAULT 'red'")])
    column = CharField()
    database = CharField()
    default = IntegerField(constraints=[SQL("DEFAULT 0")])
    idsamples = AutoField(column_name='idSamples')
    sampleid = CharField()
    table = CharField()
    title = CharField()
    type = CharField()

    class Meta:
        table_name = 'samplesInfo'


class TablesInfo(BaseModel):
    band = CharField(null=True)
    bandmapped = CharField(column_name='bandMapped', constraints=[SQL("DEFAULT 'n'")], null=True)
    bandtype = CharField(null=True)
    comment = CharField(null=True)
    editable = CharField(constraints=[SQL("DEFAULT 'y'")])
    idinfo = AutoField(column_name='idInfo')
    public = CharField(constraints=[SQL("DEFAULT 'n'")], null=True)
    relatedto = CharField(column_name='relatedTo', constraints=[SQL("DEFAULT 'PNMain'")], null=True)
    relation = CharField(constraints=[SQL("DEFAULT '1-1'")], null=True)
    showintable = CharField(column_name='showInTable', constraints=[SQL("DEFAULT 'y'")], null=True)
    showinfo = IntegerField(column_name='showInfo', constraints=[SQL("DEFAULT 0")])
    tablelongname = CharField(column_name='tableLongName', null=True)
    varcolumn = CharField(column_name='varColumn')
    vargroup = IntegerField(column_name='varGroup', constraints=[SQL("DEFAULT 0")])
    varmain = IntegerField(column_name='varMain', constraints=[SQL("DEFAULT 0")], null=True)
    varname = CharField(column_name='varName')
    varorder = IntegerField(column_name='varOrder', unique=True)
    varsearch = IntegerField(column_name='varSearch', constraints=[SQL("DEFAULT 1")])
    varsee = IntegerField(column_name='varSee', constraints=[SQL("DEFAULT 0")])
    vartable = CharField(column_name='varTable')
    vartype = CharField(column_name='varType')
    varunits = CharField(column_name='varUnits')
    varvar = CharField(column_name='varVar', unique=True)

    class Meta:
        table_name = 'tablesInfo'


class TbPa(BaseModel):
    epa = DecimalField(column_name='EPA', null=True)
    gpa = DecimalField(column_name='GPA', null=True)
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    comments = TextField(null=True)
    errpa = DecimalField(column_name='errPA', null=True)
    flagepa = ForeignKeyField(column_name='flagEPA', field='flag', model=FlagMap, null=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, null=True)
    idtbpa = AutoField(column_name='idtbPA')
    refinuse = CharField(column_name='refInUse', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refrecord = IntegerField(column_name='refRecord', null=True)
    reftable = CharField(column_name='refTable', null=True)
    reference = ForeignKeyField(column_name='reference', field='identifier', model=ReferenceIDs, null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'tbPA'


class TbRadioCont(BaseModel):
    flux = FloatField(column_name='Flux', null=True)
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    band = CharField(null=True)
    comments = TextField(null=True)
    errflux = FloatField(column_name='errFlux', null=True)
    flag = CharField(constraints=[SQL("DEFAULT 'n'")], null=True)
    flagflux = ForeignKeyField(column_name='flagFlux', field='flag', model=FlagMap, null=True)
    freq = FloatField(null=True)
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain, null=True)
    idtbradiocont = AutoField(column_name='idtbRadioCont')
    instrument = CharField(null=True)
    refinuse = CharField(column_name='refInUse', constraints=[SQL("DEFAULT 'sys'")], null=True)
    refrecord = IntegerField(column_name='refRecord', null=True)
    reftable = CharField(column_name='refTable', constraints=[SQL("DEFAULT '-'")], null=True)
    reference = ForeignKeyField(column_name='reference', field='identifier', model=ReferenceIDs, null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'tbRadioCont'


class TbTemp(BaseModel):
    inuse = IntegerField(column_name='InUse', constraints=[SQL("DEFAULT 0")])
    comments = TextField(null=True)
    data = FloatField(null=True)
    flagdata = CharField(column_name='flagData', null=True)
    idpnmain = IntegerField(column_name='idPNMain', constraints=[SQL("DEFAULT 0")], index=True)
    idtbtemp = IntegerField(column_name='idtbTemp', unique=True)
    refrecord = IntegerField(column_name='refRecord', null=True)
    reftable = CharField(column_name='refTable', constraints=[SQL("DEFAULT '-'")], null=True)
    userrecord = CharField(column_name='userRecord', constraints=[SQL("DEFAULT 'sys'")])

    class Meta:
        table_name = 'tbTemp'
        indexes = (
            (('idtbtemp', 'idpnmain'), True),
        )
        primary_key = CompositeKey('idpnmain', 'idtbtemp')


class TbUsrComm(BaseModel):
    comment = TextField(null=True)
    date = DateTimeField()
    idpnmain = ForeignKeyField(column_name='idPNMain', field='idpnmain', model=PNMain)
    idtbusrcomm = IntegerField(column_name='idtbUsrComm', unique=True)
    public = CharField(constraints=[SQL("DEFAULT 'y'")])
    user = CharField(constraints=[SQL("DEFAULT 'sys'")], null=True)

    class Meta:
        table_name = 'tbUsrComm'
        indexes = (
            (('idtbusrcomm', 'idpnmain'), True),
        )
        primary_key = CompositeKey('idpnmain', 'idtbusrcomm')
