import ibcommon.parse as ibparse
from peewee import *

mysql_ini = ibparse.parse_ini_arguments('/etc/.pymysql-ini', 'mysqldb')
db_MCs = MySQLDatabase('MCs', **mysql_ini)
db_MCs.connect()


class UnknownField(object):
    def __init__(self, *_, **__): pass


class BaseModel(Model):
    class Meta:
        database = db_MCs


class ASKAP_LMC_PNe_results(BaseModel):
    em = FloatField(column_name='EM', null=True)
    flux_5ghz = FloatField(column_name='Flux_5GHz', null=True)
    majdiam = FloatField(column_name='MajDiam', null=True)
    mindiam = FloatField(column_name='MinDiam', null=True)
    mion = FloatField(column_name='Mion', null=True)
    name = CharField(column_name='Name', null=True)
    pnstat = CharField(column_name='PNstat', constraints=[SQL("DEFAULT 'P'")], null=True)
    domain = CharField(null=True)
    err_radiodiam = FloatField(column_name='err_radioDiam', null=True)
    flag = CharField(null=True)
    flag_diam = CharField(null=True)
    flag_radiodiam = CharField(column_name='flag_radioDiam', null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True, unique=True)
    radiodiam = FloatField(column_name='radioDiam', null=True)
    radiodiam_npoints = IntegerField(column_name='radioDiam_npoints', null=True)
    ref_diam = CharField(null=True)
    useflux = CharField(column_name='useFlux', constraints=[SQL("DEFAULT 'y'")], null=True)

    class Meta:
        table_name = 'ASKAP_LMC_PNe_results'


class ASKAP_PNe_results(BaseModel):
    em = FloatField(column_name='EM', null=True)
    flux_5ghz = FloatField(column_name='Flux_5GHz', null=True)
    majdiam = FloatField(column_name='MajDiam', null=True)
    mindiam = FloatField(column_name='MinDiam', null=True)
    mion = FloatField(column_name='Mion', null=True)
    name = CharField(column_name='Name', null=True)
    domain = CharField(null=True)
    err_radiodiam = FloatField(column_name='err_radioDiam', null=True)
    flag = CharField(null=True)
    flag_diam = CharField(null=True)
    flag_radiodiam = CharField(column_name='flag_radioDiam', null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True, unique=True)
    radiodiam = FloatField(column_name='radioDiam', null=True)
    radiodiam_npoints = IntegerField(column_name='radioDiam_npoints', null=True)
    ref_diam = CharField(null=True)
    useflux = CharField(column_name='useFlux', constraints=[SQL("DEFAULT 'y'")], null=True)

    class Meta:
        table_name = 'ASKAP_PNe_results'


class ASKAP_SMC_PNe_results(BaseModel):
    em = FloatField(column_name='EM', null=True)
    flux_5ghz = FloatField(column_name='Flux_5GHz', null=True)
    majdiam = FloatField(column_name='MajDiam', null=True)
    mindiam = FloatField(column_name='MinDiam', null=True)
    mion = FloatField(column_name='Mion', null=True)
    name = CharField(column_name='Name', null=True)
    domain = CharField(null=True)
    err_radiodiam = FloatField(column_name='err_radioDiam', null=True)
    flag = CharField(null=True)
    flag_diam = CharField(null=True)
    flag_radiodiam = CharField(column_name='flag_radioDiam', null=True)
    id = IntegerField(constraints=[SQL("DEFAULT 0")])
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    radiodiam = FloatField(column_name='radioDiam', null=True)
    radiodiam_npoints = IntegerField(column_name='radioDiam_npoints', null=True)
    ref_diam = CharField(null=True)
    useflux = CharField(column_name='useFlux', constraints=[SQL("DEFAULT 'y'")], null=True)

    class Meta:
        table_name = 'ASKAP_SMC_PNe_results'
        primary_key = False


class Jacoby_2002_tbl3(BaseModel):
    comments = CharField(column_name='Comments', null=True)
    dej2000 = CharField(column_name='DEJ2000', null=True)
    nii_ha = FloatField(column_name='NII_Ha', null=True)
    raj2000 = CharField(column_name='RAJ2000', null=True)
    field = IntegerField(null=True)
    flag_nii_ha = CharField(column_name='flag_NII_Ha', null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True, unique=True)
    image_diam = FloatField(null=True)
    m_5007 = FloatField(null=True)
    spect_diam = IntegerField(null=True)

    class Meta:
        table_name = 'Jacoby_2002_tbl3'


class Jacoby_2002_tbl4(BaseModel):
    comments = CharField(column_name='Comments', null=True)
    dej2000 = CharField(column_name='DEJ2000', null=True)
    field = IntegerField(column_name='Field', null=True)
    name = CharField(column_name='Name', null=True)
    raj2000 = CharField(column_name='RAJ2000', null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True, unique=True)
    m_5007 = FloatField(null=True)

    class Meta:
        table_name = 'Jacoby_2002_tbl4'


class LMC_aegean_catalogue(BaseModel):
    a = FloatField(null=True)
    b = FloatField(null=True)
    background = FloatField(null=True)
    band = CharField(null=True)
    dec = FloatField(null=True)
    dec_str = CharField(null=True)
    err_a = FloatField(null=True)
    err_b = FloatField(null=True)
    err_dec = FloatField(null=True)
    err_int_flux = FloatField(null=True)
    err_pa = FloatField(null=True)
    err_peak_flux = FloatField(null=True)
    err_ra = FloatField(null=True)
    flags = IntegerField(null=True)
    floodclip = IntegerField(null=True)
    freqhz = IntegerField(column_name='freqHz', null=True)
    idaegean = AutoField()
    int_flux = FloatField(null=True)
    island = IntegerField(null=True)
    local_rms = FloatField(null=True)
    name = CharField(null=True)
    offset_fit = FloatField(null=True)
    pa = FloatField(null=True)
    peak_flux = FloatField(null=True)
    psf_a = FloatField(null=True)
    psf_b = FloatField(null=True)
    psf_pa = FloatField(null=True)
    ra = FloatField(null=True)
    ra_str = CharField(null=True)
    residual_mean = FloatField(null=True)
    residual_std = FloatField(null=True)
    seedclip = IntegerField(null=True)
    source = IntegerField(null=True)
    uuid = CharField(null=True)

    class Meta:
        table_name = 'LMC_aegean_catalogue'


class Leverenz_Table3(BaseModel):
    _13cm_new = FloatField(column_name='13cm_new', null=True)
    _13cm_old = FloatField(column_name='13cm_old', null=True)
    _20cm_new = FloatField(column_name='20cm_new', null=True)
    _20cm_old = FloatField(column_name='20cm_old', null=True)
    _3cm_new = FloatField(column_name='3cm_new', null=True)
    _3cm_old = FloatField(column_name='3cm_old', null=True)
    _6cm_new = FloatField(column_name='6cm_new', null=True)
    _6cm_old = FloatField(column_name='6cm_old', null=True)
    dec = CharField(column_name='DEC', null=True)
    halpha = FloatField(column_name='Halpha', null=True)
    mir = FloatField(column_name='MIR', null=True)
    no_ = IntegerField(column_name='No.', null=True)
    pn = CharField(column_name='PN', null=True)
    ra = CharField(column_name='RA', null=True)
    source = CharField(column_name='Source', null=True)
    err13cm_new = FloatField(null=True)
    err13cm_old = FloatField(null=True)
    err20cm_new = FloatField(null=True)
    err20cm_old = FloatField(null=True)
    err3cm_new = FloatField(null=True)
    err3cm_old = FloatField(null=True)
    err6cm_new = FloatField(null=True)
    err6cm_old = FloatField(null=True)
    flag13cm_new = CharField(null=True)
    flag13cm_old = CharField(null=True)
    flag20cm_new = CharField(null=True)
    flag20cm_old = CharField(null=True)
    flag3cm_new = CharField(null=True)
    flag3cm_old = CharField(null=True)
    flag6cm_new = CharField(null=True)
    flag6cm_old = CharField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True, unique=True)
    theta = FloatField(null=True)

    class Meta:
        table_name = 'Leverenz_Table3'


class SMCPhotometry(BaseModel):
    name = CharField(column_name='Name', null=True)
    band = CharField(null=True)
    fit_posdec = FloatField(null=True)
    fit_posra = FloatField(null=True)
    flag = CharField(null=True)
    flux = FloatField(null=True)
    flux_err = FloatField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    ispoint = CharField(null=True)
    maj_ax = FloatField(null=True)
    min_ax = FloatField(null=True)
    offset = FloatField(null=True)
    pa = FloatField(null=True)
    peak = FloatField(null=True)
    peak_err = FloatField(null=True)

    class Meta:
        table_name = 'SMCPhotometry'


class SMC_aegean_catalogue(BaseModel):
    a = FloatField(null=True)
    b = FloatField(null=True)
    background = FloatField(null=True)
    band = CharField(null=True)
    dec = FloatField(null=True)
    dec_str = CharField(null=True)
    err_a = FloatField(null=True)
    err_b = FloatField(null=True)
    err_dec = FloatField(null=True)
    err_int_flux = FloatField(null=True)
    err_pa = FloatField(null=True)
    err_peak_flux = FloatField(null=True)
    err_ra = FloatField(null=True)
    flags = IntegerField(null=True)
    floodclip = IntegerField(null=True)
    freqhz = IntegerField(column_name='freqHz', null=True)
    idaegean = AutoField()
    int_flux = FloatField(null=True)
    island = IntegerField(null=True)
    local_rms = FloatField(null=True)
    name = CharField(null=True)
    offset_fit = FloatField(null=True)
    pa = FloatField(null=True)
    peak_flux = FloatField(null=True)
    psf_a = FloatField(null=True)
    psf_b = FloatField(null=True)
    psf_pa = FloatField(null=True)
    ra = FloatField(null=True)
    ra_str = CharField(null=True)
    residual_mean = FloatField(null=True)
    residual_std = FloatField(null=True)
    seedclip = IntegerField(null=True)
    source = IntegerField(null=True)
    uuid = CharField(null=True)

    class Meta:
        table_name = 'SMC_aegean_catalogue'


class SMC_aegean_catalogue_v1(BaseModel):
    a = FloatField(null=True)
    b = FloatField(null=True)
    background = FloatField(null=True)
    band = CharField(null=True)
    beammap_flag = CharField(constraints=[SQL("DEFAULT 'y'")], null=True)
    dec = FloatField(null=True)
    dec_str = CharField(null=True)
    err_a = FloatField(null=True)
    err_b = FloatField(null=True)
    err_dec = FloatField(null=True)
    err_int_flux = FloatField(null=True)
    err_pa = FloatField(null=True)
    err_peak_flux = FloatField(null=True)
    err_ra = FloatField(null=True)
    flags = IntegerField(null=True)
    floodclip = IntegerField(null=True)
    freqhz = IntegerField(column_name='freqHz', null=True)
    idaegean = AutoField()
    int_flux = FloatField(null=True)
    island = IntegerField(null=True)
    local_rms = FloatField(null=True)
    name = CharField(null=True)
    offset_fit = FloatField(null=True)
    pa = FloatField(null=True)
    peak_flux = FloatField(null=True)
    psf_a = FloatField(null=True)
    psf_b = FloatField(null=True)
    psf_pa = FloatField(null=True)
    ra = FloatField(null=True)
    ra_str = CharField(null=True)
    residual_mean = FloatField(null=True)
    residual_std = FloatField(null=True)
    seedclip = IntegerField(null=True)
    source = IntegerField(null=True)
    uuid = CharField(null=True)
    xpix = IntegerField(null=True)
    ypix = IntegerField(null=True)

    class Meta:
        table_name = 'SMC_aegean_catalogue_v1'


class SMC_catalogue(BaseModel):
    name = CharField(column_name='Name', null=True)
    band = CharField(null=True)
    beammap_flag = CharField(constraints=[SQL("DEFAULT 'y'")], null=True)
    converged = CharField(null=True)
    del_ = CharField(column_name='del', constraints=[SQL("DEFAULT 'n'")], null=True)
    fit_posdec = FloatField(null=True)
    fit_posra = FloatField(null=True)
    flag = CharField(null=True)
    flux = FloatField(null=True)
    flux_err = FloatField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    ispoint = CharField(null=True)
    maj_ax = FloatField(null=True)
    maj_ax_err = FloatField(null=True)
    min_ax = FloatField(null=True)
    min_ax_err = FloatField(null=True)
    off_lat = FloatField(null=True)
    off_long = FloatField(null=True)
    offset = FloatField(null=True)
    pa = FloatField(null=True)
    pa_err = FloatField(null=True)
    peak = FloatField(null=True)
    peak_err = FloatField(null=True)
    sum = FloatField(null=True)
    xpix = FloatField(null=True)
    ypix = FloatField(null=True)
    zerooff = FloatField(null=True)
    zerooff_err = FloatField(null=True)

    class Meta:
        table_name = 'SMC_catalogue'


class SMC_catalogue_V5(BaseModel):
    name = CharField(column_name='Name', null=True)
    band = CharField(null=True)
    beammap_flag = CharField(constraints=[SQL("DEFAULT 'y'")], null=True)
    del_ = CharField(column_name='del', constraints=[SQL("DEFAULT 'n'")], null=True)
    fit_posdec = FloatField(null=True)
    fit_posra = FloatField(null=True)
    flag = CharField(null=True)
    flux = FloatField(null=True)
    flux_err = FloatField(null=True)
    id = IntegerField(constraints=[SQL("DEFAULT 0")])
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    ispoint = CharField(null=True)
    maj_ax = FloatField(null=True)
    maj_ax_err = FloatField(null=True)
    min_ax = FloatField(null=True)
    min_ax_err = FloatField(null=True)
    off_lat = FloatField(null=True)
    off_long = FloatField(null=True)
    offset = FloatField(null=True)
    pa = FloatField(null=True)
    pa_err = FloatField(null=True)
    peak = FloatField(null=True)
    peak_err = FloatField(null=True)
    sum = FloatField(null=True)
    xpix = FloatField(null=True)
    ypix = FloatField()
    zerooff = FloatField(null=True)
    zerooff_err = FloatField(null=True)

    class Meta:
        table_name = 'SMC_catalogue_V5'
        primary_key = False


class SMC_catalogue_v1(BaseModel):
    name = CharField(column_name='Name', null=True)
    band = CharField(null=True)
    del_ = CharField(column_name='del', constraints=[SQL("DEFAULT 'n'")], null=True)
    fit_posdec = FloatField(null=True)
    fit_posra = FloatField(null=True)
    flag = CharField(null=True)
    flux = FloatField(null=True)
    flux_err = FloatField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    ispoint = CharField(null=True)
    maj_ax = FloatField(null=True)
    min_ax = FloatField(null=True)
    offset = FloatField(null=True)
    pa = FloatField(null=True)
    peak = FloatField(null=True)
    peak_err = FloatField(null=True)
    useflux = FloatField(column_name='useFlux', null=True)

    class Meta:
        table_name = 'SMC_catalogue_v1'


class SMC_catalogue_v2(BaseModel):
    name = CharField(column_name='Name', null=True)
    band = CharField(null=True)
    del_ = CharField(column_name='del', constraints=[SQL("DEFAULT 'n'")], null=True)
    fit_posdec = FloatField(null=True)
    fit_posra = FloatField(null=True)
    flag = CharField(null=True)
    flux = FloatField(null=True)
    flux_err = FloatField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    ispoint = CharField(null=True)
    maj_ax = FloatField(null=True)
    min_ax = FloatField(null=True)
    offset = FloatField(null=True)
    pa = FloatField(null=True)
    peak = FloatField(null=True)
    peak_err = FloatField(null=True)

    class Meta:
        table_name = 'SMC_catalogue_v2'


class SMC_catalogue_v3(BaseModel):
    name = CharField(column_name='Name', null=True)
    band = CharField(null=True)
    del_ = CharField(column_name='del', constraints=[SQL("DEFAULT 'n'")], null=True)
    fit_posdec = FloatField(null=True)
    fit_posra = FloatField(null=True)
    flag = CharField(null=True)
    flux = FloatField(null=True)
    flux_err = FloatField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    ispoint = CharField(null=True)
    maj_ax = FloatField(null=True)
    min_ax = FloatField(null=True)
    offset = FloatField(null=True)
    pa = FloatField(null=True)
    peak = FloatField(null=True)
    peak_err = FloatField(null=True)

    class Meta:
        table_name = 'SMC_catalogue_v3'


class SMC_catalogue_v4(BaseModel):
    name = CharField(column_name='Name', null=True)
    band = CharField(null=True)
    beammap_flag = CharField(constraints=[SQL("DEFAULT 'y'")], null=True)
    del_ = CharField(column_name='del', constraints=[SQL("DEFAULT 'n'")], null=True)
    fit_posdec = FloatField(null=True)
    fit_posra = FloatField(null=True)
    flag = CharField(null=True)
    flux = FloatField(null=True)
    flux_err = FloatField(null=True)
    id = IntegerField(constraints=[SQL("DEFAULT 0")])
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    ispoint = CharField(null=True)
    maj_ax = FloatField(null=True)
    maj_ax_err = FloatField(null=True)
    min_ax = FloatField(null=True)
    min_ax_err = FloatField(null=True)
    off_lat = FloatField(null=True)
    off_long = FloatField(null=True)
    offset = FloatField(null=True)
    pa = FloatField(null=True)
    pa_err = FloatField(null=True)
    peak = FloatField(null=True)
    peak_err = FloatField(null=True)
    sum = FloatField(null=True)
    xpix = FloatField(null=True)
    ypix = FloatField()
    zerooff = FloatField(null=True)
    zerooff_err = FloatField(null=True)

    class Meta:
        table_name = 'SMC_catalogue_v4'
        primary_key = False


class SMC_detected(BaseModel):
    ddecj2000 = FloatField(column_name='DDECJ2000', null=True)
    draj2000 = FloatField(column_name='DRAJ2000', null=True)
    name = CharField(column_name='Name', null=True)
    diam = FloatField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)

    class Meta:
        table_name = 'SMC_detected'


class SMC_radio(BaseModel):
    dej2000 = CharField(column_name='DEJ2000', null=True)
    majdiam = FloatField(column_name='MajDiam', null=True)
    mindiam = FloatField(column_name='MinDiam', null=True)
    name = CharField(column_name='Name', null=True)
    raj2000 = CharField(column_name='RAJ2000', null=True)
    rphot = FloatField(column_name='Rphot', null=True)
    alpha = FloatField(null=True)
    askap_20cm = FloatField(null=True)
    askap_30cm = FloatField(null=True)
    atca_13cm = FloatField(null=True)
    atca_20cm = FloatField(null=True)
    atca_3cm = CharField(null=True)
    atca_6cm = FloatField(null=True)
    cabb_13cm = FloatField(null=True)
    err_askap_20cm = FloatField(null=True)
    err_askap_30cm = FloatField(null=True)
    err_atca_13cm = FloatField(null=True)
    err_atca_20cm = FloatField(null=True)
    err_atca_3cm = FloatField(null=True)
    err_atca_6cm = FloatField(null=True)
    err_cabb_13cm = FloatField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    ne = FloatField(null=True)

    class Meta:
        table_name = 'SMC_radio'


class SMC_sources(BaseModel):
    askap_1320 = FloatField(column_name='ASKAP_1320', null=True)
    askap_960 = FloatField(column_name='ASKAP_960', null=True)
    atca_1400 = FloatField(column_name='ATCA_1400', null=True)
    atca_2300 = FloatField(column_name='ATCA_2300', null=True)
    atca_4800 = FloatField(column_name='ATCA_4800', null=True)
    atca_8600 = FloatField(column_name='ATCA_8600', null=True)
    ddecj2000 = FloatField(column_name='DDECJ2000', null=True)
    decj2000 = CharField(column_name='DECJ2000', null=True)
    draj2000 = FloatField(column_name='DRAJ2000', null=True)
    most_843 = FloatField(column_name='MOST_843', null=True)
    name = CharField(column_name='NAME', null=True)
    name_type = CharField(column_name='NAME_TYPE', null=True)
    raj2000 = CharField(column_name='RAJ2000', null=True)
    s1ghz = FloatField(column_name='S1Ghz', null=True)
    alpha = FloatField(null=True)

    class Meta:
        table_name = 'SMC_sources'


class Stanghellini_2003_tbl3(BaseModel):
    dej2000 = CharField(column_name='DEJ2000', null=True)
    majdiam = FloatField(column_name='MajDiam', null=True)
    mindiam = FloatField(column_name='MinDiam', null=True)
    morphology = CharField(column_name='Morphology', null=True)
    name = CharField(column_name='Name', null=True)
    notes = CharField(column_name='Notes', null=True)
    raj2000 = CharField(column_name='RAJ2000', null=True)
    rphot = FloatField(column_name='Rphot', null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True, unique=True)

    class Meta:
        table_name = 'Stanghellini_2003_tbl3'


class extraRadio(BaseModel):
    bmaj = FloatField(column_name='Bmaj', null=True)
    bmin = FloatField(column_name='Bmin', null=True)
    flux = FloatField(column_name='Flux', null=True)
    flux_err = FloatField(column_name='Flux_err', null=True)
    inuse = CharField(column_name='InUse', constraints=[SQL("DEFAULT 'y'")], null=True)
    band = CharField(null=True)
    bibtex = CharField(null=True)
    comments = CharField(null=True)
    flag = CharField(constraints=[SQL("DEFAULT 'n'")], null=True)
    flagflux = CharField(column_name='flagFlux', null=True)
    freq = FloatField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    instrument = CharField(null=True)
    refkey = IntegerField(column_name='refKey', null=True)
    reference = CharField(null=True)

    class Meta:
        table_name = 'extraRadio'


class pne_fit_params(BaseModel):
    d = FloatField(column_name='D', null=True)
    diam = FloatField(column_name='Diam', null=True)
    em = FloatField(column_name='EM', null=True)
    te = FloatField(column_name='Te', null=True)
    crfreq = FloatField(null=True)
    errd_down = FloatField(column_name='errD_down', null=True)
    errd_up = FloatField(column_name='errD_up', null=True)
    flux_6cm_est = FloatField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    init_diam = FloatField(null=True)
    isconverged = CharField(column_name='isConverged', null=True)
    method = CharField(null=True)
    model = CharField(null=True)
    morph = CharField(null=True)
    n_points = IntegerField(null=True)
    ni = DecimalField(null=True)
    opt_diam = FloatField(null=True)
    tbl = IntegerField(null=True)
    theta = FloatField(null=True)
    time_in = DateTimeField(null=True)

    class Meta:
        table_name = 'pne_fit_params'


class pne_fit_params_em(BaseModel):
    d = FloatField(column_name='D', null=True)
    diam = FloatField(column_name='Diam', null=True)
    em = FloatField(column_name='EM', null=True)
    te = FloatField(column_name='Te', null=True)
    crfreq = FloatField(null=True)
    errd_down = FloatField(column_name='errD_down', null=True)
    errd_up = FloatField(column_name='errD_up', null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    init_diam = FloatField(null=True)
    isconverged = CharField(column_name='isConverged', null=True)
    method = CharField(null=True)
    model = CharField(null=True)
    morph = CharField(null=True)
    n_points = IntegerField(null=True)
    ni = DecimalField(null=True)
    opt_diam = FloatField(null=True)
    tbl = IntegerField(null=True)
    theta = FloatField(null=True)
    time_in = DateTimeField(null=True)

    class Meta:
        table_name = 'pne_fit_params_em'


class pne_fit_results(BaseModel):
    te = FloatField(column_name='Te', null=True)
    cfreq = FloatField(null=True)
    cfreq_low_lim = FloatField(null=True)
    cfreq_p_val = FloatField(null=True)
    cfreq_start = FloatField(null=True)
    cfreq_std_err = FloatField(null=True)
    cfreq_t_val = FloatField(null=True)
    cfreq_up_lim = FloatField(null=True)
    finiter = IntegerField(column_name='finIter', null=True)
    fintol = FloatField(column_name='finTol', null=True)
    fit_quality = IntegerField(null=True)
    fittinglib = CharField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    isconv = CharField(column_name='isConv', null=True)
    lin_inters = FloatField(null=True)
    lin_slope = FloatField(null=True)
    model = CharField(null=True)
    mse = FloatField(null=True)
    n_points = IntegerField(null=True)
    ni = DecimalField(null=True)
    nrmse = FloatField(null=True)
    stopcode = IntegerField(column_name='stopCode', null=True)
    stopmessage = CharField(column_name='stopMessage', null=True)
    theta = FloatField(null=True)
    theta_low_lim = FloatField(null=True)
    theta_p_val = FloatField(null=True)
    theta_start = FloatField(null=True)
    theta_std_err = FloatField(null=True)
    theta_t_val = FloatField(null=True)
    theta_up_lim = FloatField(null=True)
    time_in = DateTimeField(null=True)

    class Meta:
        table_name = 'pne_fit_results'


class pne_fit_results_em(BaseModel):
    te = FloatField(column_name='Te', null=True)
    cfreq = FloatField(null=True)
    cfreq_low_lim = FloatField(null=True)
    cfreq_p_val = FloatField(null=True)
    cfreq_start = FloatField(null=True)
    cfreq_std_err = FloatField(null=True)
    cfreq_t_val = FloatField(null=True)
    cfreq_up_lim = FloatField(null=True)
    finiter = IntegerField(column_name='finIter', null=True)
    fintol = FloatField(column_name='finTol', null=True)
    fit_quality = IntegerField(null=True)
    fittinglib = CharField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    isconv = CharField(column_name='isConv', null=True)
    lin_inters = FloatField(null=True)
    lin_slope = FloatField(null=True)
    model = CharField(null=True)
    mse = FloatField(null=True)
    n_points = IntegerField(null=True)
    ni = DecimalField(null=True)
    nrmse = FloatField(null=True)
    stopcode = IntegerField(column_name='stopCode', null=True)
    stopmessage = CharField(column_name='stopMessage', null=True)
    theta = FloatField(null=True)
    theta_low_lim = FloatField(null=True)
    theta_p_val = FloatField(null=True)
    theta_start = FloatField(null=True)
    theta_std_err = FloatField(null=True)
    theta_t_val = FloatField(null=True)
    theta_up_lim = FloatField(null=True)
    time_in = DateTimeField(null=True)

    class Meta:
        table_name = 'pne_fit_results_em'


class radiotom5007(BaseModel):
    dej2000 = CharField(column_name='DEJ2000', null=True)
    flux = CharField(column_name='Flux', null=True)
    flux_err = CharField(column_name='Flux_err', null=True)
    name = CharField(column_name='Name', null=True)
    raj2000 = CharField(column_name='RAJ2000', null=True)
    diam_opt = FloatField(null=True)
    flag = CharField(null=True)
    idpnmain = IntegerField(column_name='idPNMain', null=True)
    m_5007 = FloatField(null=True)

    class Meta:
        table_name = 'radiotom5007'
