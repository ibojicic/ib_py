import argparse
from peeweedbmodels import dbMainGPN, dbMainMisc, dbMainSNR, dbPhotometry

from datetime import datetime


class PhotInOut:

    def __init__(self, method):
        self.method = method

        self._table = None
        self.phototable = dbPhotometry.Photometry
        self._argstable = dbPhotometry.Arguments
        self._obstable = dbPhotometry.Observations

        self._args = None
        self._targets = None
        self._targets_dict = None

        self.set_args()
        self.set_table()
        self.set_objects()

    @property
    def method(self):
        return self._method

    @method.setter
    def method(self, method):
        self._method = method

    @property
    def input_table(self):
        return self._table

    def set_table(self):
        tablemap = {'pne': dbMainGPN.PnMain, 'snrs': dbMainSNR.GalacticSnRs, 'misc': dbMainMisc.MiscObjects}
        self._table = tablemap[self.args.catalogue]
        return self

    @property
    def phototable(self):
        return self._phototable

    @phototable.setter
    def phototable(self, table):
        self._phototable = table

    @property
    def argstable(self):
        return self._argstable

    @property
    def obstable(self):
        return self._obstable

    @property
    def args(self):
        return self._args

    @property
    def objects(self):
        return self._targets

    def set_args(self):
        # parse inputs
        parser = argparse.ArgumentParser(description='Parse input for the {} photometry.'.format(self.method))

        # SHARED ARGUMENTS
        # object selection
        parser.add_argument('--catalogue', '-c', action='store', choices=['pne', 'snrs', 'misc'], required=True,
                            type=str, help="Choose between prepared catalogues.")
        parser.add_argument('--target', '-o', action='store', required=False, default='all', type=str,
                            help="Choose object by identification (all for all objects from the chosen cat).")
        # path selection
        parser.add_argument('--resultspath', '-i', action='store', required=True, type=str,
                            help="Full path to the folder where results will be stored.")
        parser.add_argument('--infile', '-f', action='store', required=True, type=str,
                            help="Full path to the input image.")
        parser.add_argument('--nowrite_db', '-d', action='store_true',
                            help="Do not write results to the database. (Default write enabled)")

        if self.method == 'casa':

            parser.add_argument('--bckg_regions', '-e', action='store', required=False, type=str, default=None,
                                help="Regions file of point source objects to be subtracted. "
                                     "Format is: dra,ddec; one per line")
            # photometry selection
            parser.add_argument('--annulus_in', '-q', action='store', required=False, type=float, default=1.5,
                                help="Annulus in factor. Default = 1.5.")
            parser.add_argument('--annulus_out', '-n', action='store', required=False, type=float, default=4.,
                                help="Annulus out factor. Default = 4.")
            parser.add_argument('--fix_offset', '-x', action='store_true',
                                help="Fix background offset for imfit. "
                                     "The fixed offset is estimated from the annulus. (Default no)")
            parser.add_argument('--aperture', '-a', action='store', required=False, type=float, default=None,
                                help="Aperture radius in arcsec, if it's not provided maj diameter is used.")

        elif self.method == 'blob':

            parser.add_argument('--bckg_regions', '-e', action='store', required=False, type=str, default=None,
                                help="Regions file of point source objects to be subtracted. "
                                     "Format is: dra,ddec; one per line")
            # photometry selection
            parser.add_argument('--blobpix_minmax', '-u', action='store', required=False, type=tuple,
                                default=(None, None),
                                help="Blobcat minpix and maxpix factors (as tuple e.g. (100,200). Default no constraints. ")
            parser.add_argument('--annulus_in', '-q', action='store', required=False, type=float, default=1.5,
                                help="Annulus in factor. Default = 1.5.")
            parser.add_argument('--annulus_out', '-n', action='store', required=False, type=float, default=4.,
                                help="Annulus out factor. Default = 4.")
            parser.add_argument('--dSNR', '-j', action='store', required=False, type=float, default=4.,
                                help="dSNR parameter for blobcat. Default = 4")
            parser.add_argument('--fSNR', '-g', action='store', required=False, type=float, default=4.,
                                help="fSNR parameter for blobcat. Default = 3")
            parser.add_argument('--aperture', '-a', action='store', required=False, type=float, default=None,
                                help="Aperture radius in arcsec, if it's not provided maj diameter is used.")

        elif self.method == 'aegean':
            parser.add_argument('--seedclip', '-s', action='store', type=float, default=5.,
                                help="Seed clip for aegean. Default = 5.")
            parser.add_argument('--floodclip', '-l', action='store', required=False, type=float, default=3.,
                                help="Flood clip for aegean. Default = 3.")
            parser.add_argument('--max_offset', '-j', action='store', required=False, type=float, default=None,
                                help="Maximium offset from catalogued position. Default = major beam / 2.")
            parser.add_argument('--clip_limits', '-e', action='store', required=False, type=tuple,
                                default=(None, None, -0.5),
                                help="Clip limits as tuple (min seedclip, min floodclip, step down). "
                                     "Default = (max seedlip, max floodclip, -0.5)")

        self._args = parser.parse_args()

    @property
    def targets(self):
        return self._targets

    @property
    def targets_dict(self):
        return self._targets_dict

    def set_objects(self):
        targets = self.input_table.select(self.input_table.name,
                                          self.input_table.draj2000,
                                          self.input_table.ddecj2000,
                                          self.input_table.majdiam) \
            .where(self.input_table.flag == 'y')

        if self.args.target != 'all':
            targets = targets.where(self.input_table.name == self.args.target)

        if targets.count() > 0:
            self._targets = targets
            self._targets_dict = [row for row in targets.dicts()]

            return True

        return False

    def write_to_photometry(self, results):
        if not self.args.nowrite_db:
            self.phototable.create(date=datetime.now(), **results)
            self.argstable.create(**results)
            self.obstable.create(**results)
        else:
            print("Writing to the db disabled.")

    def args_dict(self):
        return vars(self.args)
