import shutil
import math
import casatasks as ct
from ibcasa.casafncs import run_imstat, subtract_bckgs, make_annulus
import ibaegean.aegeanfncs as ibaegean

class CasaBlobPhotometry:
    _fitresults = {}
    _bckgstats = {}
    # current im file for proccessing
    _imfile = None
    # current fits file for proccessing
    _fitsfile = None

    def __init__(self, observation):
        # initiate prepared for photometry flag
        self.prepared = False

        # load observation setup
        self.obs = observation

        # set init imfile
        self.imfile = self.obs.files['imfile']
        # # export original image
        ct.exportfits(self.imfile, self.obs.files['fitsfile'])

    @property
    def prepared(self):
        return self._prepared

    @prepared.setter
    def prepared(self, prepared):
        self._prepared = prepared

    @property
    def imfile(self):
        return self._imfile

    @imfile.setter
    def imfile(self, imfile):
        self._imfile = imfile

    @property
    def fitsfile(self):
        return self._fitsfile

    @fitsfile.setter
    def fitsfile(self, fitsfile):
        self._fitsfile = fitsfile

    @property
    def bckgstats(self):
        return self._bckgstats

    @property
    def dbresults(self):
        return self._dbresults

    @dbresults.setter
    def dbresults(self, dbresults):
        self._dbresults = dbresults

    def set_bckgstats(self, region, stats=None):
        # stats in the 'region_background'
        if stats is None:
            stats = run_imstat(in_image=self.imfile, in_region=region)
        self._bckgstats = stats
        return self

    def aperture_phot(self, image, aperture):
        mask = "'{}'>{}".format(image, self.zerolevel)
        return run_imstat(image, aperture, mask)

    # gauss fitting results getter and setter
    @property
    def fitresults(self):
        return self._fitresults

    @fitresults.setter
    def fitresults(self, results):
        self._fitresults = results

    def prepare_photometry(self):
        # copy original image to 'imcleaned' image and work on that one
        shutil.copytree(self.imfile, self.obs.files['imcleaned'])
        self.imfile = self.obs.files['imcleaned']

        # subtract background point sources if any
        subtract_bckgs(self.obs.pars['bckg_sources'], self.imfile, self.obs.pars['BMAJ'])
        # region for the backgorund statistics in CASA format
        region_background = make_annulus(self.obs.target['draj2000'], self.obs.target['ddecj2000'],
                                         self.obs.pars['annulus_in'], self.obs.pars['annulus_out'])

        self.set_bckgstats(region_background)

        # make fits image from im
        ct.exportfits(self.imfile, self.obs.files['fitscleaned'])
        self.fitsfile = self.obs.files['fitscleaned']

        # TODO check if everything is ready
        self.prepared = True
        return self

    def background_level(self, method='mean'):
        if method == 'mean':
            return self.bckgstats['mean']
        elif method == 'rms':
            return math.sqrt(self.bckgstats['rms'] ** 2 - self.bckgstats['sigma'] ** 2)
        elif method == 'median':
            return self.bckgstats['median']
        return None

    def construct_bane(self):
        parameters = {
            "--out"    : self.obs.files['banebase']
            }

        self.banecommand = ibaegean.bane_command(self.obs.args['infile'], parameters)


