import os
import shutil
import math
import ibcommon.strings as ibstrings
import casatasks as ct
import ibcommon.dicts as ibdicts
from ibcasa.casafncs import run_imstat, make_aperture
from ibphotometry_old.CasaBlobPhotClass import CasaBlobPhotometry
import ibastro.fitslibs as ibfits
import ibaegean.aegeanfncs as ibaegean

class BlobPhotometry(CasaBlobPhotometry):
    _blobpars = {}
    # zero level for blob masking
    # flux is ALWAYS assumed to be positive
    zerolevel = 0
    # exclude level for blob maskig
    # flux bellow this level is taken into consideration
    masklevel = 999

    def __init__(self, observation):
        super().__init__(observation)

    # getter and setter for blobcat parameters
    @property
    def blobpars(self):
        return self._blobpars

    def aperture_phot(self, image, aperture):
        mask = "'{}'>{}".format(image, self.zerolevel)
        return run_imstat(image, aperture, mask)

    def set_db_results(self):

        set_hash = ibstrings.id_generator(32)

        self.dbresults = ibdicts.merge_dicts([
            {
                'hash'  : set_hash,
                'method': 'blob'
                },
            self.obs.target,
            self.obs.files,
            self.obs.pars,
            self.fitresults,
            self.blobpars
            ])

        return self

    def background_level(self, method='mean'):
        if method == 'mean':
            return self.bckgstats['mean']
        elif method == 'rms':
            return math.sqrt(self.bckgstats['rms'] ** 2 - self.bckgstats['sigma'] ** 2)
        elif method == 'median':
            return self.bckgstats['median']
        return None

    # apply blobcat algorithm to find extended emission
    # see http://adsabs.harvard.edu/abs/2012MNRAS.425..979H
    # script source: http://blobcat.sourceforge.net/
    def blobit(self):

        if not self.prepared:
            self.prepare_photometry()

        # self.construct_bane()
        # ibaegean.run_command(self.banecommand)


        self._blobpars = {
            'rmsval': self.bckgstats['rms'],
            # 'rmsmap': self.obs.files['banerms'],
            'bmaj'  : self.obs.pars['BMAJ'],
            'bmin'  : self.obs.pars['BMIN'],
            'bpa'   : self.obs.pars['BPA'],
            'dSNR'  : self.obs.pars['dSNR'],  # self.aprtrstats['aprtr_median'] / self.bckgstats['rms'],
            'fSNR'  : self.obs.pars['fSNR'],  # self.aprtrstats['aprtr_q1'] / self.bckgstats['rms'],
            # 'dSNR': self.aprtrstats['aprtr_median'] / self.bckgstats['rms'] + 1,
            # 'fSNR': self.aprtrstats['aprtr_q1'] / self.bckgstats['rms'] + 1,
            'hfill' : self.masklevel
            }
        if isinstance(self.obs.pars['blobpix_minmax'][0], int):
            self._blobpars['maxpix'] = self.obs.pars['blobpix_minmax'][0],

        if isinstance(self.obs.pars['blobpix_minmax'][1], int):
            self._blobpars['minpix'] = self.obs.pars['blobpix_minmax'][1],

        pars = ''
        for par in self._blobpars:
            pars = "{} --{}={}".format(pars, par, self._blobpars[par])

        tmpimage = "tmpblobs.fits"
        tmpblobs = "tmpblobs_blobs.fits"

        shutil.copy(self.fitsfile, tmpimage)

        command = "blobcat {} --write {}".format(tmpimage, pars)
        os.system(command)

        if not os.path.exists(tmpblobs):
            return False

        shutil.move(tmpblobs, self.obs.files['fitsfile_blobs'])
        ct.importfits(fitsimage=self.obs.files['fitsfile_blobs'], imagename=self.obs.files['imfile_blobs'])
        expression = 'iif(IM0<{},{},IM1)'.format(self.masklevel, self.zerolevel)
        ct.immath(imagename=[self.obs.files['imfile_blobs'], self.imfile], mode='evalexpr',
                  outfile=self.obs.files['maskedblobs'], expr=expression)
        ct.exportfits(fitsimage=self.obs.files['maskedblobsfits'], imagename=self.obs.files['maskedblobs'])



        # region for apperture fitting in CASA format
        region_aperture = make_aperture(self.obs.target['draj2000'], self.obs.target['ddecj2000'],
                                        self.obs.pars['aperture'])

        tmp_blobresults = self.aperture_phot(self.obs.files['maskedblobs'], region_aperture)

        blobresults = {'type': 'blob'}
        # add blob prefix to all keys
        for blobkey in tmp_blobresults:
            blobresults["blob_{}".format(blobkey)] = tmp_blobresults[blobkey]

        # number of beams
        blobresults['blob_no_beams'] = self.obs.pars['beams_per_pixel'] * blobresults['blob_npts']

        # correct for background flux
        blobresults['background'] = self.background_level()
        blobresults['flux'] = blobresults['blob_flux'] - blobresults['blob_no_beams'] * blobresults['background']
        blobresults['flux_err'] = blobresults['blob_no_beams'] * self.bckgstats['rms']
        blobresults['peak'] = tmp_blobresults['max']

        blobresults['rms'] = self.bckgstats['rms']
        blobresults['std'] = self.bckgstats['sigma']

        self.fitresults = blobresults

        return self

