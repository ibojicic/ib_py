import math
from ibphotometry_old.ObservationClass import Observation


class CasaObservation(Observation):
    # out files
    outfiles = {
        'imfile'         : ['input', 'im'],
        'fitsfile'       : ['input', 'fits'],
        'imcleaned'      : ['cleaned', 'im'],
        'fitscleaned'    : ['cleaned', 'fits'],
        'pngfile'        : ['phot', 'png'],
        'modelfile'      : ['model', 'im'],
        'modelfits'      : ['model', 'fits'],
        'logtable'       : ['table', 'log'],
        'residualimage'  : ['residual', 'im'],
        'residualfits'   : ['residual', 'fits'],
        'residualpngfile': ['residual', 'png'],
        'summaryfile'    : ['summary', 'txt'],
        'imfitestims'    : ['estimates', 'txt']
        }
    # header items
    hlist = ['CDELT1', 'CDELT2', 'BMAJ', 'BMIN', 'BPA']

    def __init__(self, method, target, inout):
        super().__init__(method, target, inout)

        self.create_object_folder()
        self.set_out_files()
        self.create_imfile()
        self.set_headeritems()
        self.set_obs_pars()

    def set_obs_pars(self):
        pars = self.hdr

        pars['BMAJ'] = pars['BMAJ'] * 3600.
        pars['BMIN'] = pars['BMIN'] * 3600.

        pars['CDELT1'] = abs(pars['CDELT1']) * 3600
        pars['CDELT2'] = abs(pars['CDELT2']) * 3600

        # beam area
        pars['beam_area'] = pars['BMAJ'] * pars['BMAJ'] * math.pi / (4. * math.log(2.))
        # no of beams per pixel
        pars['beams_per_pixel'] = pars['CDELT1'] * pars['CDELT2'] / pars['beam_area']

        # if aperture size is not provided use maj diameter
        # if no maj diameter use maj beam size
        pars['aperture'] = self.args['aperture']
        if pars['aperture'] is None:
            if self.target['majdiam'] is None:
                pars['aperture'] = pars['BMAJ']
                pars['resolved'] = False
            else:
                pars['aperture'] = float(self.target['majdiam']) / 2.
                pars['resolved'] = pars['BMAJ'] / self.target['majdiam'] < 2.

        # TODO check if annulus is within image
        pars['annulus_in'] = pars['aperture'] * self.args['annulus_in']
        pars['annulus_out'] = pars['aperture'] * self.args['annulus_out']
        pars['bckg_sources'] = self.parse_bckg_file()
        # from input arguments
        pars['fix_offset'] = self.args['fix_offset']
        pars['infile'] = self.args['infile']

        self.pars = pars
        return self
