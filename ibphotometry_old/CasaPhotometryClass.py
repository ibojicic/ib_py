import ibcommon.strings as ibstrings
import casatasks as ct
import ibcommon.dicts as ibdicts
from ibcasa.casafncs import write_estimates, run_imfit, parse_fit
from ibastro.coordtrans import radec_separation
from ibphotometry_old.CasaBlobPhotClass import CasaBlobPhotometry
from pprint import pprint

class CasaPhotometry(CasaBlobPhotometry):
    _fit_estimates_file = ''

    def __init__(self, observation):
        super().__init__(observation)

    def aperture_phot(self, image, aperture):
        mask = "'{}'>{}".format(image, self.zerolevel)
        return run_imstat(image, aperture, mask)

    # gauss fitting estimates file getter and setter
    # see ESTIMATES in  https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_imfit/about
    @property
    def fit_estimates_file(self):
        return self._fit_estimates_file

    @fit_estimates_file.setter
    def fit_estimates_file(self, estimates_file):
        self._fit_estimates_file = estimates_file

    def set_db_results(self):

        set_hash = ibstrings.id_generator(32)

        self.dbresults = ibdicts.merge_dicts([
            {
                'hash'  : set_hash,
                'method': 'casa'
                },
            self.obs.target,
            self.obs.files,
            self.obs.pars,
            self.fitresults,
            ])

        return self

    def gaussfit(self):

        if not self.prepared:
            self.prepare_photometry()

        # region for gaussian fitting in CASA format
        region_fit = 'circle[[{}deg,{}deg],{}arcsec]'.format(self.obs.target['draj2000'],
                                                             self.obs.target['ddecj2000'],
                                                             self.obs.pars['annulus_in'])

        # run source fitting
        fit_input = {
            'imagename': self.imfile,
            'region'   : region_fit,
            'logfile'  : self.obs.files['logtable'],
            'dooff'    : True,
            'offset'   : self.bckgstats['mean'],
            'fixoffset': False,  # self.obs.pars['fix_offset'],
            'summary'  : self.obs.files['summaryfile'],
            'model'    : self.obs.files['modelfile'],
            'estimates': self.fit_estimates_file
            }

        fitresults = run_imfit(fit_input)

        if not fitresults:
            return False

        parsedresults = parse_fit(fitresults)

        # calculate offset from original position
        parsedresults['offset_fit'] = radec_separation(parsedresults['fit_posra'],
                                                       parsedresults['fit_posdec'],
                                                       self.obs.target['draj2000'],
                                                       self.obs.target['ddecj2000'])
        parsedresults['type'] = 'imfit'
        parsedresults['rms'] = self.bckgstats['rms']
        parsedresults['std'] = self.bckgstats['sigma']

        self.fitresults = parsedresults
        return self

    def gaussfit_fixed(self):
        # run source fitting with fixed estimates
        # estimates are made from temp gauss fitting run

        self.gaussfit()


        estimates = {
            'estimates_file': self.obs.files['imfitestims'],
            'peak'          : self.fitresults['peak'],
            'peak_x'        : self.fitresults['peak_x'],
            'peak_y'        : self.fitresults['peak_y'],
            'maj_axis'      : self.obs.pars['BMAJ'],
            'min_axis'      : self.obs.pars['BMIN'],
            'bpa'           : self.obs.pars['BPA'],
            'fixed'         : 'xyabp'
            }
        write_estimates(**estimates)
        self.fit_estimates_file = self.obs.files['imfitestims']

        self.fitresults = None

        self.gaussfit()

        return self

    def make_residual(self):
        ct.immath([self.obs.files['imfile'], self.obs.files['modelfile']], 'evalexpr', self.obs.files['residualimage'],
                  expr='IM0-IM1')
        ct.exportfits(self.obs.files['residualimage'], self.obs.files['residualfits'])
        return self
