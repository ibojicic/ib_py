import os, sys
import ibcommon.parse as ibparse
import ibastro.fitslibs as ibfits
import casatasks as ct
import csv


class Observation:
    outfiles = {
        'name': ['filename', 'fileextension']
        }

    hlist = []

    def __init__(self, method, target, inout):

        self.method = method
        self.target = target
        self.args = inout

        self.objectfolder = None
        self.files = {}
        self.pars = None
        self.hdr = {}

    @property
    def method(self):
        return self._method

    @method.setter
    def method(self, method):
        self._method = method

    @property
    def pars(self):
        return self._pars

    @pars.setter
    def pars(self, pars):
        self._pars = pars

    @property
    def target(self):
        return self._target

    @target.setter
    def target(self, target):
        self._target = target

    @property
    def args(self):
        return self._args

    @args.setter
    def args(self, args):
        self._args = args

    @property
    def hdr(self):
        return self._hdr

    @hdr.setter
    def hdr(self, items):
        self._hdr = items

    @property
    def objectfolder(self):
        return self._objectfolder

    @objectfolder.setter
    def objectfolder(self, folder):
        self._objectfolder = folder

    def construct_file_name(self, outfile):
        if self.outfiles[outfile][1] is not None:
            extension = '.{}'.format(self.outfiles[outfile][1])
        else:
            extension = ''
        return '{}{}_{}{}'. \
            format(self.objectfolder, self.target['name'], self.outfiles[outfile][0], extension)

    def create_object_folder(self):
        folder = "{}{}/{}".format(ibparse.corrpath(self.args['resultspath']), self.target['name'], self.method)
        if os.path.exists(folder):
            sys.exit("Output folder for object: '{}' already exists, exiting...".format(folder))
        os.makedirs(folder)
        if not os.path.exists(folder):
            sys.exit("Can't create output folder for object: '{}' already exists, exiting...".format(folder))
        self.objectfolder = ibparse.corrpath(folder)
        return self

    # TODO
    def set_out_files(self):
        for outf in self.outfiles:
            self.files[outf] = self.construct_file_name(outf)
        return self

    def set_obs_pars(self):
        self.pars = None
        return self

    def set_headeritems(self, hdrNo=0):
        self.hdr = ibfits.getHeaderItems(self.args['infile'], self.hlist, hdrNo)
        return self

    def create_imfile(self):
        if not os.path.exists(self.files['imfile']):
            ct.importfits(fitsimage=self.args['infile'], imagename=self.files['imfile'])
        return self

    # background objects for subtracting
    def parse_bckg_file(self):
        result = []
        if self.args['bckg_regions'] is not None and os.path.exists(self.args['bckg_regions']):
            with open(self.args['bckg_regions'], 'r') as f:
                reader = csv.reader(f, quoting=csv.QUOTE_NONNUMERIC)
                result = list(reader)
        return result
