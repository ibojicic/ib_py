import ibcommon.dicts as ibdicts
import ibcommon.strings as ibstrings

from ibastro import coordtrans
import os
import csv
import pandas as pd
import numpy as np
import ibaegean.aegeanfncs as ibaegean


class AegeanPhot:

    def __init__(self, observation):
        self.obs = observation
        self.dfresults = None
        self.dbresults = None
        self.fitresults = None
        self.aegcommand = None
        self.mimascommand = None
        self.aerescommand = None
        self.banecommand = None

    @property
    def obs(self):
        return self._obs

    @obs.setter
    def obs(self, observation):
        self._obs = observation

    @property
    def dfresults(self):
        return self._dfresults

    @dfresults.setter
    def dfresults(self, dfresults):
        self._dfresults = dfresults

    @property
    def dbresults(self):
        return self._dbresults

    @dbresults.setter
    def dbresults(self, dbresults):
        self._dbresults = dbresults

    @property
    def fitresults(self):
        return self._fitresults

    @fitresults.setter
    def fitresults(self, results):
        self._fitresults = results

    def construct_bane(self):
        parameters = {
            "--out"    : self.obs.files['banebase']
            }

        self.banecommand = ibaegean.bane_command(self.obs.args['infile'], parameters)

    def construct_aegean(self):
        parameters = {
            "--table"    : self.obs.files['aeg_table'],
            "--seedclip" : self.obs.args['seedclip'],
            "--floodclip": self.obs.args['floodclip'],
            "--region"   : self.obs.files['mimas']
            }

        self.aegcommand = ibaegean.aegean_command(self.obs.args['infile'], parameters)


    def construct_mimas(self):
        parameters = {
            '+c'    : [str(self.obs.target['draj2000']),
                       str(self.obs.target['ddecj2000']),
                       str(self.obs.pars['mimas_radius'])],
            '-depth': '20'
            }

        self.mimascommand = ibaegean.mimas_command(self.obs.files['mimas'], parameters)

    def construct_aeres(self):
        self.aerescommand = ibaegean.aeres_command(self.obs.files['comp_table'],
                                                   self.obs.args['infile'],
                                                   self.obs.files['residualfits']
                                                   )

    # TODO check output
    def parse_results(self):
        results = []
        if not os.path.exists(self.obs.files['comp_table']):
            return False
        with open(self.obs.files['comp_table'], 'r') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
            header = True
            for row_line in csvreader:
                if not header:
                    fitresults = dict(zip(inp_header, row_line))
                    separation = coordtrans.radec_separation(fitresults['ra'],
                                                             fitresults['dec'],
                                                             self.obs.target['draj2000'],
                                                             self.obs.target['ddecj2000'])
                    extra = {
                        'offset_fit': separation,
                        'fit_posra' : fitresults['ra'],
                        'fit_posdec': fitresults['dec'],
                        }
                    results.append(ibdicts.merge_two_dicts(fitresults, extra))
                else:
                    inp_header = row_line

                header = False
        self._dfresults = pd.DataFrame(results)
        return True

    def min_offset_result(self):
        return self.dfresults[self.dfresults.offset_fit == self.dfresults.offset_fit.min()]

    def run_until_find(self):

        min_seedclip, min_floodclip, step_down = self.obs.args['clip_limits']

        if min_seedclip is None:
            min_seedclip = self.obs.args['seedclip']
        if min_floodclip is None:
            min_floodclip = self.obs.args['floodclip']
        step_down = -1 * abs(step_down)

        sclips_array = np.arange(self.obs.args['seedclip'], min_seedclip + step_down, step_down)

        self.construct_bane()
        ibaegean.run_command(self.banecommand)

        for sclip in sclips_array:
            self.obs.args['seedclip'] = sclip
            fclips_array = np.arange( self.obs.args['floodclip'], min_floodclip + step_down, step_down)
            for fclip in fclips_array:
                self.obs.args['floodclip'] = fclip
                self.construct_aegean()
                self.construct_mimas()
                ibaegean.run_command(self.mimascommand)
                ibaegean.run_command(self.aegcommand)
                self.parse_results()
                curr_result = self.min_offset_result()
                if curr_result.offset_fit.iloc[0] < self.obs.pars['max_offset']:
                    return curr_result
        return None

    def photometry(self):
        results = {'type': 'aegean'}
        rawresults = self.run_until_find().to_dict()
        for key, res in rawresults.items():
            results[key] = ibstrings.change_to_number(list(res.values())[0])

        convert_keys = {
            'int_flux'     : 'flux',
            'err_int_flux' : 'flux_err',
            'peak_flux'    : 'peak',
            'err_peak_flux': 'peak_err',
            'a'            : 'maj_ax',
            'err_a'        : 'maj_ax_err',
            'b'            : 'min_ax',
            'err_b'        : 'min_ax_err',
            'err_pa'       : 'pa_err',
            'residual_std' : 'std',
            'local_rms'    : 'rms'
            }
        for oldkey, newkey in convert_keys.items():
            results[newkey] = results.pop(oldkey)

        self.fitresults = results

    def set_db_results(self):

        set_hash = ibstrings.id_generator(32)

        self.dbresults = ibdicts.merge_dicts([
            {
                'hash'  : set_hash,
                'method': 'aegean'
                },
            self.obs.target,
            self.obs.files,
            self.obs.pars,
            self.fitresults
            ])
        return self

    def make_residual(self):
        self.construct_aeres()
        ibaegean.run_command(self.aerescommand, self.obs.files['residualfits'])
