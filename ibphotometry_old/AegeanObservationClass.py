import math
from ibphotometry_old.ObservationClass import Observation
from ibastro.coordtrans import radstoarcsecs


class AegeanObservation(Observation):
    # out files
    outfiles = {
        'aeg_table'      : ['aeg_table', 'csv'],
        'comp_table'     : ['aeg_table_comp', 'csv'],
        'mimas'          : ['mimas', 'csv'],
        'residualfits'   : ['residual', 'fits'],
        'residualpngfile': ['residual', 'png'],
        'pngfile'        : ['fitimage', 'png'],
        'banebase'       : ['bane', None],
        'banebkg'        : ['bane_bkg', 'fits'],
        'banerms'        : ['bane_rms', 'fits']
        }
    # header items
    hlist = ['CDELT1', 'CDELT2', 'BMAJ', 'BMIN', 'BPA']

    def __init__(self, method, target, inout):
        super().__init__(method, target, inout)
        self.create_object_folder()
        self.set_out_files()
        self.set_headeritems()
        self.set_obs_pars()

    def set_obs_pars(self):
        pars = self.hdr

        pars['BMAJ'] = pars['BMAJ'] * 3600.
        pars['BMIN'] = pars['BMIN'] * 3600.

        pars['CDELT1'] = radstoarcsecs(abs(pars['CDELT1']))
        pars['CDELT2'] = radstoarcsecs(abs(pars['CDELT2']))

        pars['max_offset'] = self.args['max_offset']
        if pars['max_offset'] is None:
            pars['max_offset'] = pars['BMAJ'] / 2.

        # beam area
        pars['beam_area'] = pars['BMAJ'] * pars['BMAJ'] * math.pi / (4. * math.log(2.))
        # no of beams per pixel
        pars['beams_per_pixel'] = pars['CDELT1'] * pars['CDELT2'] / pars['beam_area']

        pars['mimas_radius'] = 2. * pars['BMAJ'] / 3600.

        self.pars = pars
        return self
