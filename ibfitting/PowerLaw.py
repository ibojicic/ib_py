import numpy as np
from scipy.optimize import curve_fit
from ibfitting import forecasting_metrics as fm
from scipy.odr import ODR, Model, RealData
import ibastro.physics as ibph
from ibfitting import InOutFit
from importlib import reload
import warnings
from scipy.optimize import OptimizeWarning

warnings.simplefilter("error", OptimizeWarning)
reload(fm)
reload(InOutFit)
reload(fm)


class PowerLaw(InOutFit.InOutFit):
    def __init__(self, fit_parameters):
        super().__init__(fit_parameters)
        self._fitter = None

    @property
    def no_points(self):
        if self._xdata is None:
            return None
        return len(self._xdata)

    @property
    def _xdata_log(self):
        if self._xdata is None:
            return None
        return np.log10(self._xdata)

    @property
    def _ydata_log(self):
        if self._ydata is None:
            return None
        return np.log10(self._ydata)

    @property
    def _xdata_err_log(self):
        if self._xdata_err is None:
            return None
        return ibph.log_uncertainty(self._xdata, self._xdata_err)

    @property
    def _ydata_err_log(self):
        if self._ydata_err is None:
            return None
        return ibph.log_uncertainty(self._ydata, self._ydata_err)

    @staticmethod
    def log_powr_law_fixed(x, intersect):
        return intersect + x * -0.1

    @staticmethod
    def log_powr_law(x, intersect, power):
        return intersect + x * power

    @staticmethod
    def log_powr_law_odr(b, x):
        return b[0] + x * b[1]

    def run_fit(self):
        if self._params['fittype'] == 'odr' or self.no_points < 3:
            self._fitter = ibph.powr_law
            self.run_fit_odr()
        elif self._params['fittype'] == 'curvefit' and self.no_points > 2:
            self._fitter = ibph.log_powr_law
            self.run_curve_fit()
        elif self._params['fittype'] == 'curvefitfixed' and self.no_points > 2:
            self._fitter = self.log_powr_law_fixed
            self.run_curve_fit_fixed()

        return self

    def run_curve_fit(self):
        try:
            fit_results, self.fit_pcov = curve_fit(self._fitter,
                                                   self._xdata_log,
                                                   self._ydata_log,)
                                                   # sigma=self._ydata_err_log)

        except (RuntimeError, OptimizeWarning) as err:
            print("Fit didn't converge. Error:{}".format(err))
            return self

        self.fit_results = {
            'intersect': fit_results[0],
            'power'    : fit_results[1]
        }

        fit_errors = self.calc_fit_err()
        self.fit_errors = {
            'intersect_err': fit_errors[0],
            'power_err'    : fit_errors[1]
        }
        self.set_predicted_values(ibph.powr_law, self._xdata, **self.fit_results)

        return self

    def run_curve_fit_fixed(self):
        try:
            fit_results, self.fit_pcov = curve_fit(self._fitter,
                                                   self._xdata_log,
                                                   self._ydata_log,
                                                   sigma=self._ydata_err_log)
        except (RuntimeError, OptimizeWarning) as err:
            print("Fit didn't converge. Error:{}".format(err))
            return self

        self.fit_results = {
            'intersect': fit_results[0],
            'power'    : -0.1
        }

        fit_errors = self.calc_fit_err()
        self.fit_errors = {
            'intersect_err': fit_errors[0],
            'power_err'    : None
        }
        self.set_predicted_values(ibph.powr_law, self._xdata, **self.fit_results)

        return self

    def run_fit_odr(self):
        xerrors = None
        yerrors = None
        if self._xdata_err_log is not None:
            xerrors = self._xdata_err_log  # * self._xdata_log
        if self._ydata_err_log is not None:
            yerrors = self._ydata_err_log  # * self._ydata_log
        data = RealData(self._xdata_log, self._ydata_log, sx=xerrors, sy=yerrors)
        model = Model(self.log_powr_law_odr)

        odr = ODR(data, model, [1, 0])
        odr.set_job(fit_type=2)
        output = odr.run()

        self.fit_results = output.beta
        self.fit_pcov = output.cov_beta
        self.fit_results = {
            'intersect': output.beta[0],
            'power'    : output.beta[1]
        }

        fit_errors = self.calc_fit_err()
        self.fit_errors = {
            'intersect_err': fit_errors[0],
            'power_err'    : fit_errors[1]
        }
        self.set_predicted_values(self._fitter, self._xdata, **self.fit_results)

        return output
