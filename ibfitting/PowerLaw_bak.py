import pandas as pd
import numpy as np
import math
from ibastro import physics as ibph
import scipy.integrate as integrate
from scipy.optimize import curve_fit
from scipy.stats.distributions import t
from ibcommon import dicts as ibdict
from datetime import datetime
from sklearn.metrics import mean_squared_error
from ibfitting import forecasting_metrics as fm
from scipy.odr import ODR, Model, Data, RealData


class PowerLaw:
    def __init__(self, data, xobs, yobs, xobs_err=None, yobs_err=None, scale='linlin', fittype='curvefit'):
        self._data = data.copy()
        self._xscale = scale[0:3]
        self._yscale = scale[3:6]
        self.set_xdata(xobs, xobs_err)
        self.set_ydata(yobs, yobs_err)
        self.fit_results = None
        self.fit_errors = None
        self.fit_pcov = None
        self._fittype = fittype

    @property
    def fittype(self):
        return self._fittype

    @property
    def data(self):
        return self._data

    @property
    def xscale(self):
        return self._xscale

    @property
    def yscale(self):
        return self._yscale

    @property
    def xdata(self):
        return self._xdata

    def set_xdata(self, xobs, xobs_err):
        if self.xscale == 'lin':
            self._xdata = np.log10(self.data[xobs])
        else:
            self._xdata = self.data[xobs]
        self._xdata_err = self.set_uncert(xobs, xobs_err, self.xscale)

    @property
    def ydata(self):
        return self._ydata

    def set_ydata(self, yobs, yobs_err):
        if self.yscale == 'lin':
            self._ydata = np.log10(self.data[yobs])
        else:
            self._ydata = self.data[yobs]
        self._ydata_err = self.set_uncert(yobs, yobs_err, self.yscale)

    def set_uncert(self, obs, obs_err, scale):
        if obs_err is None:
            return None
        elif scale == 'log':
            return self.data[obs_err]
        # http: // faculty.washington.edu / stuve / uwess / log_error.pdf
        elif scale == 'lin':
            ratios = self.data[obs_err] / self.data[obs]
            return 0.434 * ratios
        return None

    @property
    def xdata_err(self):
        return self._xdata_err

    @property
    def ydata_err(self):
        return self._ydata_err

    @property
    def no_points(self):
        return len(self.ydata)

    @property
    def fit_results(self):
        return self._fit_results

    @fit_results.setter
    def fit_results(self, fit_results):
        if fit_results is None:
            self._fit_results = None
        else:
            self._fit_results = {"factor": fit_results[0], "power": fit_results[1]}

    @property
    def fit_errors(self):
        return self._fit_errors

    @fit_errors.setter
    def fit_errors(self, fit_errors):
        if fit_errors is None:
            self._fit_errors = None
        else:
            self._fit_errors = {"factor_err": fit_errors[0], "power_err": fit_errors[1]}

    @property
    def fit_pcov(self):
        return self._fit_pcov

    @fit_pcov.setter
    def fit_pcov(self, pcov):
        self._fit_pcov = pcov

    @property
    def full_results(self):
        res = ibdict.merge_dicts(
                [self.fit_results,
                 self.fit_errors])
        res['n_points'] = self.no_points
        res['nrmse'] = self.calc_nrmse()
        res['mape'] = self.calc_mape()
        res['smape'] = self.calc_smape()
        res['mse'] = self.calc_mse()
        res['rchi'] = self.reduced_chi()
        res['rsqared'] = self.r_squared()
        res['time_in'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        return res

    @property
    def predicted_y(self):
        if self.fit_results is None:
            return None
        return self.powr_law(self.xdata, **self.fit_results)

    def calc_nrmse(self):
        if self.fit_results is None or self.ydata_err is None:
            return None
        yfit = self.powr_law(self.xdata, **self.fit_results)
        sample_weights = 1. / self.ydata_err ** 2
        rmse = mean_squared_error(self.ydata, yfit, sample_weight=sample_weights)
        # normalizer = np.std(self.flux_data)
        normalizer = np.quantile(self.ydata, 0.75) - np.quantile(self.ydata, 0.25)
        if normalizer == 0:
            normalizer = 1.
        nrmse = 100 * np.sqrt(rmse) / normalizer
        return nrmse

    def calc_mape(self):
        try:
            res = fm.mape(self.ydata, self.predicted_y) * 100
        except TypeError:
            res = None
        return res

    def calc_smape(self):
        try:
            res = fm.smape(self.ydata, self.predicted_y) * 100
        except TypeError:
            res = None
        return res

    def calc_mse(self):
        try:
            res = fm.mse(self.ydata, self.predicted_y)
        except TypeError:
            res = None
        return res

    def r_squared(self):
        try:
            res = fm.r_squared(self.ydata, self.predicted_y)
        except TypeError:
            res = None
        return res

    def reduced_chi(self):
        if self.ydata_err is None:
            return None
        try:
            res = fm.redchisqg(self.ydata, self.predicted_y, 2, self.ydata_err)
        except TypeError:
            res = None
        return res

    @staticmethod
    def check_infs(inp_list):
        res = []
        for el in inp_list:
            if np.isinf(el):
                res.append(None)
            else:
                res.append(el)

    def calc_fit_err(self, pcov, fit_results):
        sigma_ab = np.sqrt(np.diag(pcov))
        alpha = 0.2  # 80% confidence interval
        p = len(fit_results)  # number of parameters
        dof = max(0, self.no_points - p)  # number of degrees of freedom
        tval = t.ppf(1.0 - alpha / 2.0, dof)  # student-t value for the dof and confidence level
        return self.check_infs(sigma_ab)  # * tval

    @staticmethod
    def powr_law(x, factor, power):
        return factor + x * power

    @staticmethod
    def powr_law_odr(b, x):
        return b[0] + x * b[1]

    def run_fit(self):
        if self.fittype == 'odr':
            self.run_fit_odr()
        elif self.fittype == 'curvefit':
            self.run_curve_fit()

        return self

    def run_curve_fit(self):
        try:
            fit_results, pcov_errors = curve_fit(self.powr_law,
                                                 self.xdata,
                                                 self.ydata,
                                                 sigma=self.ydata_err)
                # ,
                                                 # absolute_sigma=True)
            # bounds=(1.e-6, [1.e4, 30]))
            fit_errors = self.calc_fit_err(pcov_errors, fit_results)
            self.fit_results = fit_results
            self.fit_errors = fit_errors
            self.fit_pcov = pcov_errors

        except RuntimeError:
            print("Fit didn't converge...")

        return self

    def run_fit_odr(self):
        xerrors = None
        yerrors = None
        if self.xdata_err is not None:
            xerrors = self.xdata_err * self.xdata
        if self.ydata_err is not None:
            yerrors = self.ydata_err * self.ydata
        data = RealData(self.xdata, self.ydata, xerrors, yerrors)
        model = Model(self.powr_law_odr)

        odr = ODR(data, model, [1, 0])
        odr.set_job(fit_type=2)
        output = odr.run()

        fit_errors = self.calc_fit_err(output.cov_beta, output.beta)
        self.fit_results = output.beta
        self.fit_errors = fit_errors
        self.fit_pcov = output.cov_beta

        return output
