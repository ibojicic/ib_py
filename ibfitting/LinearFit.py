import numpy as np
import pandas as pd
from scipy import optimize
# import pymc3 as pm
# import theano
# import theano.tensor as tt
from sklearn import datasets, linear_model


class LinearFit:

    def __init__(self):
        print("test")

    # https://docs.pymc.io/notebooks/GLM-robust-with-outlier-detection.html#
    @staticmethod
    def linear_fit_hogg(data, x_obs, y_obs, x_obs_err=None, y_obs_err=None):
        # TODO
        print('todo')

    def linear_fit(self, data, xobs, yobs, xobs_err=None, yobs_err=None, scale='linlin'):
        # Create linear regression object
        regr = linear_model.LinearRegression()

        x_data = self._transform_scale(data[xobs]).reshape([-1,1])
        y_data = self._transform_scale(data[yobs])
        # Train the model using the training sets

        regr.fit(x_data, y_data)
        return {
            'rsqr':regr.score(x_data, y_data),
            'intercept':regr.intercept_,
            'slope':regr.coef_
            }

    @staticmethod
    def _transform_scale(inp_list, scale):
        data_list = np.array(list(inp_list))
        if scale == 'lin':
            return data_list
        elif scale == 'log':
            return np.log10(data_list)

        return None