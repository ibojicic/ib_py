import pandas as pd
import numpy as np
from scipy.stats.distributions import t
from ibcommon import dicts as ibdict
from datetime import datetime
from ibfitting import forecasting_metrics as fm
from ibastro import thermal_sed

from importlib import reload

reload(fm)


class InOutFit:
    _def_metrics = ('nrmse', 'smape', 'mape', 'mse', 'redchisqg', 'r_squared', 'adj_rsquare')

    def __init__(self, fit_parameters):
        self._params = fit_parameters
        self._xdata = None
        self._ydata = None
        self._xdata_err = None
        self._ydata_err = None
        self._metadata = {}
        self._predicted = None
        self._bounds = None
        self._init_pars = None
        self.fit_results = None
        self.fit_errors = None
        self.fit_pcov = None
        self.metrics = None

    def set_data_pandas(self, df: pd.DataFrame, xcol: str, ycol: str, xcol_err: str = None, ycol_err: str = None,
                        **kwargs):
        """
        Import data neaded for fitting from pandas dataframe
        @return:
        @param df: input dataframe, required
        @param xcol: string, name of the column for the xdata, required
        @param ycol: string, name of the column for the ydata, required
        @param xcol_err: string, name of the column for the ydata uncertanites, optional
        @param ycol_err: string, name of the column for the ydata uncertanites, optional
        @param kwargs: other columns needed for the fit, the values of dataframe[value] are stored in
                self._metadata[parameter]
        @return: self
        """
        self._xdata = df[xcol]
        self._ydata = df[ycol]
        if xcol_err is not None:
            self._xdata_err = df[xcol_err]
        if ycol_err is not None:
            self._ydata_err = df[ycol_err]
        for key, val in kwargs.items():
            self._metadata[key] = df[val]
        return self

    def set_bounds(self, bounds: tuple):
        """
        set bounds fot curve_fit fitter
        @param bounds: tuple, bounds for the fit
        @return: self
        """
        self._bounds = bounds
        return self

    @property
    def fit_results(self):
        """
        @return: dict, fited and fixed parameters {parameter:value}
        """
        return self._fit_results

    @fit_results.setter
    def fit_results(self, fit_results):
        self._fit_results = fit_results

    @property
    def fit_errors(self):
        """
        @return: dict, fit errors {parameter:value}
        """
        return self._fit_errors

    @fit_errors.setter
    def fit_errors(self, fit_errors):
        self._fit_errors = fit_errors

    @property
    def fit_pcov(self):
        return self._fit_pcov

    @fit_pcov.setter
    def fit_pcov(self, pcov):
        self._fit_pcov = pcov

    @property
    def full_results(self, return_metrics=_def_metrics):
        self.metrics = return_metrics
        res = ibdict.merge_dicts(
            [self._params,
             self._init_pars,
             self.fit_results,
             self.fit_errors,
             self.metrics])
        res['n_points'] = len(self._ydata)
        res['time_in'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        return res

    def set_predicted_values(self, fitter, xdata, **fit_results):
        if fit_results is None:
            self._predicted = None
        else:
            self._predicted = fitter(xdata, **fit_results)
        return self

    ###########################################################################
    # metrics and errors
    ###########################################################################

    def calc_fit_err(self, t_stat=False, conf_lev=0.2):
        sigma_ab = np.sqrt(np.diag(self.fit_pcov))
        dof = max(0, len(self._ydata) - len(self.fit_results))  # number of degrees of freedom
        tval = 1.
        if t_stat:
            tval = t.ppf(1.0 - conf_lev / 2.0, dof)  # student-t value for the dof and confidence level
        return sigma_ab * tval

    @property
    def metrics(self):
        return self._metrics

    @metrics.setter
    def metrics(self, metrics):

        parameters = {
            'actual'     : self._ydata,
            'predicted'  : self._predicted,
            'errors'     : self._ydata_err,
            'nrmse_norm ': 'quantile',
            'deg_free'   : 2
        }
        self._metrics = fm.evaluate(metrics=metrics, **parameters)
