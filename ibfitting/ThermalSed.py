import numpy as np
from ibastro import thermal_sed
from scipy.optimize import curve_fit
from ibfitting import forecasting_metrics as fm
from ibfitting import InOutFit
from importlib import reload

reload(fm)
reload(InOutFit)


class ThermalSed(InOutFit.InOutFit):
    _def_init_diam = 1.
    _def_bounds = (1.e-6, [1.e4, 30])

    def __init__(self, fit_parameters):
        super().__init__(fit_parameters)
        self._fitter = thermal_sed.flux_nu
        self._beams = None
        self._bounds = self._def_bounds

    ###########################################################################
    # fitting algorithm
    ###########################################################################

    def run_fit(self, **kwargs):

        self.set_init_pars()
        use_sigma = kwargs.get('use_sigma', True)

        try:
            fit_results, self.fit_pcov = curve_fit(
                lambda x, theta, freq_0: self._fitter(x, theta,
                                                      freq_0,
                                                      self._params['mu'],
                                                      self._params['Te'],
                                                      self._params['model']),
                self._xdata,
                self._ydata,
                sigma=self._ydata_err if use_sigma else 1,
                p0=list(self._init_pars.values()),
                bounds=self._bounds,
                **kwargs
            )

        except (ValueError,RuntimeError) as err:
            print("Fit didn't converge. Error:{}".format(err))
            return self

        self.fit_results = {"theta" : fit_results[0],
                            "freq_0": fit_results[1],
                            "mu"    : self._params['mu'],
                            'Te'    : self._params['Te'],
                            'model' : self._params['model']}

        fit_errors = self.calc_fit_err()
        self.fit_errors = {
            'theta_err' : fit_errors[0],
            'freq_0_err': fit_errors[1]
        }
        self.set_predicted_values(self._fitter, self._xdata, **self.fit_results)
        return self
    ###########################################################################
    # initial parameters
    ###########################################################################

    def set_init_pars(self):
        init_diam = self._def_init_diam
        if 'beams' in self._metadata:
            if np.sum(np.isnan(self._metadata['beams'])) < len(self._metadata['beams']):
                init_diam = np.nanmin(self._metadata['beams'])
        init_freq_0 = np.nanmin(self._xdata)
        res = {
            'theta_start' : init_diam,
            'freq_0_start': init_freq_0
        }
        self._init_pars = res
