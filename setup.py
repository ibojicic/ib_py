from setuptools import setup, find_packages

with open("README.rst") as f:
    readme = f.read()

with open("LICENSE") as f:
    license = f.read()

setup(
    name="ib_py",
    version="1.1.0",
    description="Function Libraries and Tools",
    long_description=readme,
    author="Ivan Bojicic",
    author_email="ibojicic@gmail.com",
    license=license,
    packages=find_packages(),
    install_requires=[
        "Click",
    ],
    entry_points={
        "console_scripts": [
            "reprojectto        = ibtools.reprojectto:cli",
            "karmann            = ibtools.karmann:cli",
            "fitscutout         = ibtools.fitscutout:cli",
            "fitscutout_mosaic  = ibtools.fitscutout_mosaic:cli",
            "make_rgb           = ibtools.make_rgb:cli",
            "fitssubtract       = ibtools.fitssubtract:cli",
            "fitsdivide         = ibtools.fitsdivide:cli",
            "spindeximages      = ibtools.spindeximages:cli",
            "blobcat            = ibtools.ib_blobcat:main",
            "casaphot           = ibtools.casa_photometry:main",
            "blobphot           = ibtools.blob_photometry:main",
            "aegeanphot         = ibtools.aegean_photometry:main",
            "recalccoords       = ibtools.recalc_coords_in_mysql:cli",
            "radiosed           = ibtools.radio_sed:cli"
        ]
    }

)
