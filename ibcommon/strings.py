from ibcommon import parse
import random as rnd
import string
from datetime import datetime


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    """
    Create random string
    :param size: number of characters
    :param chars: characters to pick from
    :return: string
    """
    return ''.join(rnd.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for x in range(size))

def currtime():
    """
    create string with the current time
    :return: string
    """
    now = datetime.now()
    return now.strftime("%m_%d_%Y_%H_%M_%S")


def generic_file(path_to_file, extenstion, no_chars = 6):
    """
    create string for a generic file
    @param path_to_file: full path to the file
    @param extenstion: extension of the file (no dot)
    @param no_chars: no of chars for the file name
    @return: string
    """
    return "{}{}.{}".format(parse.corrpath(path_to_file), id_generator(no_chars), extenstion)


def is_number(s):
    """
    check if string is a number
    from: https://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float
    @param s: string
    @return: boolean
    """
    try:
        float(s)
        return True
    except ValueError:
        return False

def change_to_number(s):
    """
    convert type to number, if not a number leave as it is
    @param s: string
    @return: converted
    """
    if is_number(s):
        return float(s)
    return s
