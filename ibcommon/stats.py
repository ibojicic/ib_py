import numpy as np
from scipy.optimize import curve_fit




######################################
# Setting up test data
def norm(x, mean, sd):
    norm = []
    for i in range(x.size):
        norm += [1.0 / (sd * np.sqrt(2 * np.pi)) * np.exp(-(x[i] - mean) ** 2 / (2 * sd ** 2))]
    return np.array(norm)


######################################
# Solving

def res(p, y, x):
    m, dm, sd1, sd2 = p
    m1 = m
    m2 = m1 + dm
    y_fit = norm(x, m1, sd1) + norm(x, m2, sd2)
    err = y - y_fit
    return err


def fit_combination(x_data, y_data):
    params, cov = curve_fit(bimodal, x_data, y_data)
    return params, cov


def gauss(x, mu, sigma, A):
    return A * np.exp(-(x - mu) ** 2 / 2 / sigma ** 2)


def bimodal(x, mu1, sigma1, A1, mu2, sigma2, A2):
    return gauss(x, mu1, sigma1, A1) + gauss(x, mu2, sigma2, A2)
