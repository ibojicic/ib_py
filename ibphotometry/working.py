from astropy.coordinates import SkyCoord
import astropy.units as u
from photutils import SkyEllipticalAperture, SkyEllipticalAnnulus, aperture_photometry
from ibastro import coordtrans, montages, fitslibs
from ibcommon.strings import id_generator
import os
from astropy.wcs import WCS
from astropy.io import fits
import numpy as np
##################################################################
############### INPUT PARAMETERS        ##########################
##################################################################

# input file
# in_file = '/Users/ibojicic/data/PROJECTS/astro/LMC_ASKAP_SNRs/LMC_observations-8532-image_cubes-1373_corrected.fits'

in_file = '5500.fits'

# object RAJ2000 and DEJ2000 centroid position (in deg)
# objRA = 73.18041666666667
# objDEC = -66.63844444444445
objRA = 96.0722945
objDEC = -69.8102883

# beam size in deg

bmaj = 2.77E-3
bmin = 2.77E-3

# semimajor axis of the aperture in arcsec
aprt_a = 106/2.

# semiminor axis of the aperture in arcsec
aprt_b = 106/2.

# positional angle of the aperture in deg
# For a right-handed world coordinate system,
# the position angle increases counterclockwise from North (PA=0).
aprt_theta = 0

# annulus offset in multiplies of aperture (>1)
annu_off = 1.2

# annulus size in multiplies of aperture (>annu_off)
annu_size = 1.5

# cutout size (in X times major aperture)

cutout_ratio = 6


# full path to ds9.reg file with point sources within object

# ds9_file = "/Users/ibojicic/data/PROJECTS/astro/LMC_ASKAP_SNRs/test.reg"
ds9_file = "points.reg"

##################################################################
############### DEFINED PARAMETERS      ##########################
##################################################################
# define and make working folder
out_folder = "ibphot_{}/".format(id_generator())
os.mkdir(out_folder)


# define out cuotout
out_cutout = "{}cutout.fits".format(out_folder)

# convert to astropy positions
positions = SkyCoord(ra=objRA, dec=objDEC, unit='deg')


##################################################################
############### MAKE CUTOUT             ##########################
##################################################################

# cutout size X * major axis (semiaxis * 2)
cutout_size = cutout_ratio * 2 * aprt_a

montages.FITScutout(
    inImage=in_file,
    outImage=out_cutout,
    Xcoord=objRA,
    Ycoord=objDEC,
    box_x_coord=cutout_size,
    box_y_coord=cutout_size
)

##################################################################
############### MIMAS & BANE & AEGEAN   ##########################
##################################################################

# bane & aegean extract point sources auto

bane_full_out = "{}bane_points_cutout".format(out_folder)

bane_forpoint_command = "BANE --out {} {}".format(bane_full_out, out_cutout)

os.system(bane_forpoint_command)


#
# table_forpoint_out = "{}tbl.fits".format(out_folder)
# aegean_forpoint_command = "aegean --cores 1 --noise {}_rms.fits --background {}_bkg.fits --blankout " \
#                           "--table {} {}".format(bane_forpoint_out, bane_forpoint_out, table_forpoint_out,
#                                                  out_cutout)
# os.system(aegean_forpoint_command)

# mimas mask point sources from ds9.reg file
    # out_masked_nopoints = "{}cutout_masked.fits".format(out_folder)
    #
    # mimas_forpoint_out = "{}mimas_forpoint_region.mim".format(out_folder)
    #
    # mimas_forpoint_region_command = "MIMAS --reg2mim {} {} -depth 20".format(ds9_file, mimas_forpoint_out)
    #
    # os.system(mimas_forpoint_region_command)
    #
    # mimas_file_command = "MIMAS --maskimage {} {} {} --negate".format(mimas_forpoint_out, out_cutout, out_masked_nopoints)
    #
    # os.system(mimas_file_command)
    #
    #
    # # mimas mask extended region
    # out_masked_noextend = "{}cutout_masked.fits".format(out_folder)
    #
    # mimas_out = "{}mimas_region.mim".format(out_folder)
    #
    # mimas_region_command = "MIMAS -o {} +c {} {} {} -depth 16".format(mimas_out, objRA, objDEC, aprt_a / 3600.)
    #
    # os.system(mimas_region_command)
    #
    # mimas_file_command = "MIMAS --maskimage {} {} {} --negate".format(mimas_out, out_cutout, out_masked_noextend)
    #
    # os.system(mimas_file_command)
    #
    # bane_out = "{}bane_cutout".format(out_folder)
    #
    # bane_command = "BANE --out {} {}".format(bane_out, out_masked_noextend)
    #
    # os.system(bane_command)


# measure rms and bkg


# define aperture and annulus for photometry
aper = SkyEllipticalAperture(positions,
                             a=aprt_a*u.arcsec,
                             b=aprt_b*u.arcsec,
                             theta=aprt_theta*u.deg)
annu = SkyEllipticalAnnulus(positions,
                            a_in=annu_off*aprt_a*u.arcsec,
                            a_out=annu_size*aprt_a*u.arcsec,
                            b_out=annu_size*aprt_b*u.arcsec,
                            theta=aprt_theta*u.deg)


hdu_bkg = fits.open("{}_bkg.fits".format(bane_full_out))
data_bkg = hdu_bkg[0].data
header_bkg = hdu_bkg[0].header

wcs = WCS(hdu_bkg[0].header)
bkg_table = aperture_photometry(data_bkg, annu, wcs=wcs)
bkg_pix_annu = bkg_table['aperture_sum']/annu.to_pixel(wcs=wcs).area


subtr_bkg = "{}subtr_bkg.fits".format(out_folder)
hdu_src = fits.open(out_cutout)
data_src = hdu_src[0].data
header_src = hdu_src[0].header
new_data = data_src - bkg_pix_annu
fits.writeto(subtr_bkg, new_data, header_src, overwrite=True)


hdu_rms = fits.open("{}_rms.fits".format(bane_full_out))
# data_rms = u.Quantity(hdu_rms[0].data, unit=hdu_rms[0].header['BUNIT'])

# beam_sigma = 50*u.arcsec
beam_area = 2*np.pi*(bmaj*u.deg*bmin*u.deg)

data_rms = u.Quantity(hdu_rms[0].data, unit=u.Jy)
wcs = WCS(hdu_rms[0].header)

rms_table = aperture_photometry(data_rms, annu, wcs=wcs)

rms_pix_annu = rms_table['aperture_sum']/annu.to_pixel(wcs=wcs).area

print(rms_table)

print(beam_area)