from importlib import reload
import pandas as pd
import plotnine as gg
import numpy as np

from ibplotting import ggPlots, PlotBasics
from ibfitting import ThermalSed


# ThermalSed = reload(ThermalSed)
# ggPlots = reload(ggPlots)


class PlotStats(PlotBasics.PlotBasics):

    def __init__(self, plotype):
        super().__init__(plotype)
        self.data = pd.DataFrame()

    def pwlaw_regression_plot(self, factor, power):

        self.elements = gg.geom_abline()
        return self

    def bland_altman_plot(self, obs1, obs2, diffplot='total', extend_x=1.3, plot_ids=None,
                          show_sdmarker=True, **xyparams):

        self.bland_altman_transf(obs1, obs2, diffplot)

        md = np.mean(self.data['ba_diff'])  # Mean of the difference
        sd = np.std(self.data['ba_diff'], axis=0)  # Standard deviation of the difference

        self.plot_xy('ba_mean', 'ba_diff', symmetric=False, **xyparams)
        if plot_ids is not None:
            self.plot_ids(plot_ids, 'ba_mean', 'ba_diff')
        maxx = np.max(abs(self.data['ba_mean']))
        minx = np.min(abs(self.data['ba_mean']))
        self.xlimits = (minx, maxx * extend_x)
        maxy = np.max(abs(self.data['ba_diff']))
        self.ylimits = (-maxy, maxy)

        self.elements = gg.geom_hline(yintercept=md + 1.96 * sd, linetype='dashed')
        self.elements = gg.geom_hline(yintercept=md - 1.96 * sd, linetype='dashed')
        self.elements = gg.geom_hline(yintercept=md)
        self.elements = gg.geom_hline(yintercept=0, linetype='dotted')

        toplevel = md + 1.96 * sd
        botlevel = md - 1.96 * sd
        ann_offset = toplevel / maxy

        if show_sdmarker:
            self.elements = gg.annotate(geom="text", label='1.96$\\cdot$sd', x=np.inf, y=toplevel,
                                        ha='right', va='bottom')
            self.elements = gg.annotate(geom="text", label='-1.96$\\cdot$sd', x=np.inf, y=botlevel - 8 * ann_offset,
                                        ha='right', va='top')

        self.elements = gg.annotate(geom="text", label="{}%".format(round(toplevel, 1)), x=np.inf,
                                    y=toplevel - 8 * ann_offset,
                                    ha='right', va='top')
        self.elements = gg.annotate(geom="text", label="{}%".format(round(botlevel, 1)), x=np.inf, y=botlevel,
                                    ha='right', va='bottom')
        self.elements = gg.annotate(geom="text", label="{}%".format(round(md, 1)), x=np.inf, y=md,
                                    ha='right', va='bottom')

    def bland_altman_transf(self, obs1, obs2, diffplot='total'):
        data1 = np.asarray(self.data[obs1])
        data2 = np.asarray(self.data[obs2])
        self.data['ba_mean'] = np.mean([data1, data2], axis=0)
        if diffplot == 'total':
            self.data['ba_diff'] = data2 - data1  # Difference between data1 and data2
        elif diffplot == 'percovermean':
            self.data['ba_diff'] = 100 * (data2 - data1) / np.mean([data1, data2],
                                                                   axis=0)  # Difference between data1 and data2 in perc
        elif diffplot == 'percovertotal':
            self.data['ba_diff'] = 100 * (data2 - data1) / data1  # Difference between data1 and data2 in perc

    def bland_altman_medians(self, obs1, obs2, diffplot, no_bins=10, group_on=None, **params):
        ord_pars = {
            'colour'  : "#000080",
            'linetype': 'solid'
        }
        aes_pars, ord_pars = self.parse_params(params, {}, ord_pars)

        self.bland_altman_transf(obs1, obs2, diffplot)

        # self.data['cuts'], bins = pd.qcut(self.data['ba_mean'], no_bins, retbins=True)
        range_for_cut = self.data[self.data[obs2].notna()][obs1]
        self.data['cuts'], bins = pd.qcut(range_for_cut, no_bins, retbins=True)
        if group_on is not None:
            nogroups = self.data[group_on].nunique()
            binscol = np.repeat(np.delete(bins, -1), nogroups)
            grouped = self.data.groupby(['cuts', group_on])
        else:
            binscol = np.delete(bins, -1)
            grouped = self.data.groupby(['cuts'])

        grouped_median = grouped.median()
        grouped_median['bins'] = binscol
        grouped_median.reset_index(inplace=True)

        if group_on is not None:
            plot_data = grouped_median[[group_on, 'bins', 'ba_diff']]
            # TODO
            # plot_data.append(pd.DataFrame())
        else:
            plot_data = grouped_median[['bins', 'ba_diff']]
            plot_data = plot_data.append(pd.DataFrame({'bins': bins[-1], 'ba_diff': plot_data.tail(1).ba_diff}))

        self.elements = gg.geom_step(data=plot_data, mapping=gg.aes(x='bins', y='ba_diff', **aes_pars),
                                     direction='hv', **ord_pars)
