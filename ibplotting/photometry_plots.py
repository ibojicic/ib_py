import numpy as np
import ibplotting.plotlibs as ibplot
import os

def make_fit_images(results):

    if not os.path.exists(results.obs.args['infile']):
        return False


    cont_levels = np.array([2., 3., 5., 8., 12., 17., 23.]) * (float(results.fitresults['std'])) + \
                  results.fitresults['rms']

    rms_levels = np.array([1.]) * float(results.fitresults['rms'])

    fit_crosses = [[results.fitresults['fit_posra'], results.fitresults['fit_posdec'], results.obs.target['name']]]

    diam = None
    if results.obs.target['majdiam'] is not None:
        diam = results.obs.target['majdiam'] / 3600.

    annulus_in = None
    if 'annulus_in' in results.obs.pars:
        annulus_in = results.obs.pars['annulus_in'] / 3600.

    annulus_out = None
    if 'annulus_out' in results.obs.pars:
        annulus_out = results.obs.pars['annulus_out'] / 3600.

    aperture = None
    if 'aperture' in results.obs.pars:
        aperture = results.obs.pars['aperture'] / 3600.

    imsize = set_imsize(diam, results.obs.pars['BMAJ'] / 3600., annulus_out)

    ibplot.photFigure(results.obs.files['residualfits'],
                      results.obs.files['residualpngfile'],
                      results.obs.target['draj2000'],
                      results.obs.target['ddecj2000'],
                      PNDiam=diam,
                      cLevels=cont_levels,
                      # -1,
                      rLevels=rms_levels,
                      crosses=fit_crosses,
                      imsize= imsize,
                      inR_ann = annulus_in,
                      inR_dann = annulus_out,
                      inR_app = aperture

    )

    ibplot.photFigure(results.obs.args['infile'],
                      results.obs.files['pngfile'],
                      results.obs.target['draj2000'],
                      results.obs.target['ddecj2000'],
                      PNDiam=diam,
                      cLevels=cont_levels,
                      # -1,
                      rLevels=rms_levels,
                      crosses=fit_crosses,
                      imsize=imsize,
                      inR_ann=annulus_in,
                      inR_dann=annulus_out,
                      inR_app=aperture
                      )

    return True

def make_blob_images(results):

    if not os.path.exists(results.obs.files['maskedblobsfits']):
        return False

    cont_levels = np.array([1.]) * results.obs.args['dSNR'] * results.fitresults['rms']
    rms_levels = np.array([1.]) * results.obs.args['fSNR'] * results.fitresults['rms']

    diam = None
    if results.obs.target['majdiam'] is not None:
        diam = results.obs.target['majdiam'] / 3600.

    imsize = set_imsize(diam, results.obs.pars['BMAJ'] / 3600., results.obs.pars['annulus_in'] / 3600.)

    ibplot.photFigure(results.obs.files['maskedblobsfits'],
                      results.obs.files['maskedblobspng'],
                      results.obs.target['draj2000'],
                      results.obs.target['ddecj2000'],
                      inR_ann=results.obs.pars['annulus_in'] / 3600.,
                      inR_dann=results.obs.pars['annulus_out'] / 3600.,
                      inR_app=results.obs.pars['aperture'] / 3600.,
                      PNDiam=diam,
                      cLevels=cont_levels,
                      # -1,
                      valmin=1E-6,
                      valmax=results.obs.args['dSNR'] * results.fitresults['rms'] * 2,
                      rLevels=rms_levels,
                      imsize=imsize
                      )


def set_imsize(diam, beam, annulus_out, default = 120. / 3600.):

    if diam is None:
        diam = 0
    if annulus_out is None:
        annulus_out = 0

    return max([10. * beam, 1.2 * annulus_out, 2. * diam, default])
