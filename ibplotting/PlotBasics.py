from importlib import reload

import numpy as np
import pandas as pd
import plotnine as gg
from ibcommon import dicts
from ibplotting import ggPlots

ggPlots = reload(ggPlots)


class PlotBasics(ggPlots.ggPlots):

    def __init__(self, plotype='linlin'):
        super().__init__(plotype)
        self.data = pd.DataFrame()

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, data: pd.DataFrame):
        self._data = data.copy()

    def plot_xy(self, xcol, ycol, symmetric=True, **params):
        # default params
        aes_pars = {}
        ord_pars = {
            'shape' : 'o',
            'size'  : 2,
            'stroke': 0.2}

        aes_pars, ord_pars = self.parse_params(params, aes_pars, ord_pars)

        x = self.data[xcol].copy()
        y = self.data[ycol].copy()
        axlimits = None
        if symmetric:
            axlimits = (np.nanmin(pd.concat([x, y])),
                        np.nanmax(pd.concat([x, y]), ))
        self.xlimits = axlimits
        self.ylimits = axlimits

        self.elements = gg.geom_point(self.data, gg.aes(x=xcol, y=ycol, **aes_pars), na_rm=True, **ord_pars)
        self.elements = gg.scale_fill_brewer(type='qual', palette=self.config['pallete'])
        return self

    def plot_errorbars(self, xcol, ycol, xerr=None, yerr=None, **params):
        # default params
        ord_pars = {
            # 'colour': "#000080",
            'size'  : 0.3,
            'height': 0,
            'width' : 0
        }
        aes_pars, ord_pars = self.parse_params(params, {}, ord_pars)
        if xerr is None and yerr is None:
            return self
        if xerr is not None:
            xpars = dicts.without_keys(ord_pars, ['width'])
            # ord_pars.pop('width', None)
            self.data['xerr_min'] = self.data[xcol] - self.data[xerr]
            self.data['xerr_max'] = self.data[xcol] + self.data[xerr]
            self.elements = gg.geom_errorbarh(self.data, gg.aes(y=ycol, xmin="xerr_min", xmax="xerr_max", **aes_pars),
                                              na_rm=True, inherit_aes=False, **xpars)
        if yerr is not None:
            ypars = dicts.without_keys(ord_pars, 'height')
            # ord_pars.pop('height', None)
            self.data['yerr_min'] = self.data[ycol] - self.data[yerr]
            self.data['yerr_max'] = self.data[ycol] + self.data[yerr]
            self.elements = gg.geom_errorbar(self.data, gg.aes(x=xcol, ymin="yerr_min", ymax="yerr_max", **aes_pars),
                                             na_rm=True, inherit_aes=False, **ypars)
            return self

    def plot_ids(self, idcol, xcol, ycol):
        idsdata = self.data[[xcol, ycol, idcol]].dropna()
        self.elements = gg.geom_text(idsdata, gg.aes(x=xcol, y=ycol, label=idcol), na_rm=True)
        return self


    def plot_oneone(self, **kwargs):
        self.elements = gg.geom_abline(slope=1, **kwargs)
        return self

    def plot_histo(self, xcol, **kwargs):
        self.elements = gg.scale_y_continuous(expand=[0, 0])
        self.elements = gg.geom_histogram(self.data, gg.aes(x=xcol), inherit_aes=False, **kwargs)
        return self

    def plot_stat_bin(self, xcol, **kwargs):
        self.elements = gg.scale_y_continuous(expand=[0, 0])
        self.elements = gg.stat_bin(self.data, gg.aes(x=xcol, y='stat(count)/max(count)'),
                                    geom="step", **kwargs)

    def plot_density(self, xcol, **kwargs):
        self.elements = gg.scale_y_continuous(expand=[0, 0])
        self.elements = gg.stat_density(self.data, gg.aes(x=xcol, y='stat(count)/max(count)'),
                                        **kwargs)

    def plot_pwr_law(self, xcol, factor, power, **kwargs):
        x = self.freq_points(self.data[xcol], no_points=2)
        y = factor * (x ** power)
        data = pd.DataFrame({"x": x, "y": y})
        self.elements = gg.geom_line(data, gg.aes(x=x, y=y),
                                     inherit_aes=False,
                                     **kwargs)

        return self

    def plot_line(self, xcol, ycol, **params):
        self.elements = gg.geom_line(self.data, gg.aes(x=xcol, y=ycol),
                                     inherit_aes=False,
                                     **params)

    def plot_hline(self, y, **params):
        self.elements = gg.geom_hline(yintercept=y, **params)

    def plot_pwr_law_confidence(self, xcol, params, fitted_pars, fitted_err, clr):

        if fitted_err is None:
            return
        plotter = ThermalSed.ThermalSed(params)

        x = self.freq_points(xdata)

        fitted_pars_max = fitted_pars.copy()

        fitted_pars_max['theta'] = fitted_pars_max['theta'] + fitted_err['theta_err'] * 1.645
        fitted_pars_max['freq_0'] = fitted_pars_max['freq_0'] + fitted_err['freq_0_err'] * 1.645
        y_max = plotter.flux_nu(1, 1, **fitted_pars_max)

        fitted_pars_min = fitted_pars.copy()
        fitted_pars_min['theta'] = fitted_pars_min['theta'] - fitted_err['theta_err'] * 1.645
        fitted_pars_min['freq_0'] = fitted_pars_min['freq_0'] - fitted_err['freq_0_err'] * 1.645
        y_min = plotter.flux_nu(1, 1, **fitted_pars_min)

        data = pd.DataFrame({"x": x, "y_min": y_min, "y_max": y_max})
        self.elements = gg.geom_ribbon(data, gg.aes(x=x, ymin=y_min, ymax=y_max), fill=clr, alpha=0.05)

        return self
