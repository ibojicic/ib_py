import numpy as np
from matplotlib.path import Path

def markCross():
    verts = [(-1, 0.), (1, 0.), (0., 1), (0., -1)]
    codes = [Path.MOVETO, Path.LINETO, Path.MOVETO, Path.LINETO]
    return Path(verts, codes)

def markBox():
    verts = [(-1, 1.), (1, 1.), (1., 1), (1., -1), (1, -1), (-1, -1), (-1, -1), (-1, 1), (0,0)]
    codes = [Path.MOVETO, Path.LINETO, Path.MOVETO, Path.LINETO, Path.MOVETO, Path.LINETO, Path.MOVETO,
             Path.LINETO, Path.CLOSEPOLY]
    return Path(verts, codes)

def markCircle():
    draw = Path.arc(0,360, n=100)
    vrts = draw.vertices
    cds = draw.codes
    verts = np.insert(vrts, np.arange(len(vrts)), vrts, axis=0)
    codes = np.insert(np.full(len(cds), 1), np.arange(len(cds)), cds)
    return Path(verts, codes)