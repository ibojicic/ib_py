import os
import shutil
from typing import Iterable
from ibcommon import parse, strings
import ibastro.fitslibs as ibastr
import ibastro.montages as ibmont

os.environ['MPLCONFIGDIR'] = '/tmp'
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

plt.ioff()

import aplpy


class PlotImage:
    default_config = {
        'fig_size'       : (8.67, 7.79),
        'subplots_adjust': {
            'bottom': 0.1,
            'left'  : 0.12,
            'top'   : 0.95,
            'right' : 0.92
        },
        'convention'     : 'calabretta',
        'grey_invert'    : True
    }
    shift_to_0header = ['hst', 'panstarrs']

    def __init__(self, image, working_dir=None, save_working_fits=True):
        self.config = self.default_config
        self.north = True
        self.imsize = None
        self.range_perc = [5, 99.5]
        self.range_val = [None, None]
        self.working_dir = working_dir
        self.image = image

    @property
    def working_dir(self):
        return self._working_dir

    @working_dir.setter
    def working_dir(self, working_dir):
        if working_dir is None:
            working_dir = "plotimage_workingdir_{}_tmp".format(ibastr.id_generator(6))
        if not os.path.exists(working_dir):
            os.makedirs(working_dir)
        self._working_dir = parse.corrpath(working_dir)

    @property
    def fitsfile(self):
        return self._fitsfile

    @property
    def config(self):
        return self._config

    @config.setter
    def config(self, config):
        self._config = config

    def set_config_pars(self, **kwargs):
        for key, val in kwargs.items():
            self._config[key] = val
        return self

    @property
    def north(self):
        return self._north

    @north.setter
    def north(self, north):
        self._north = north

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, fitsfile):
        base_fits_name = os.path.basename(fitsfile)
        inimage = "{}wrk_{}".format(self.working_dir, base_fits_name)
        shutil.copyfile(fitsfile, inimage)
        if not self.check_header(inimage):
            raise Exception("Problem with image header")
        self._image = self.set_image(inimage)
        self._fitsfile = inimage

    @property
    def imsize(self):
        return self._imsize

    @imsize.setter
    def imsize(self, imsize):
        self._imsize = imsize

    def set_image(self, fitsfile):
        fig = plt.figure(figsize=self.config['fig_size'])
        fig.subplots_adjust(**self.config['subplots_adjust'])
        # TODO fix north
        return aplpy.FITSFigure(fitsfile, figure=fig, convention=self.config['convention'],
                                north=False)  # north=self.north)

    @property
    def range_perc(self):
        return self._range_perc

    @range_perc.setter
    def range_perc(self, inrange: Iterable):
        self._range_perc = {
            'pmin': inrange[0],
            'pmax': inrange[1]
        }
        self._range_val = {
            'vmin': None,
            'vmax': None
        }

    @property
    def range_val(self):
        return self._range_val

    @range_val.setter
    def range_val(self, inrange: Iterable):
        self._range_val = {
            'vmin': inrange[0],
            'vmax': inrange[1]
        }
        self._range_perc = {
            'pmin': None,
            'pmax': None
        }

    def center_image(self, ra=None, dec=None):
        if ra is not None and dec is not None:
            self.image.recenter(ra, dec, radius=self.imsize)
        return self

    def bw_image(self, invert=True, stretch_scale='arcsinh'):
        self.image.show_grayscale(invert=invert, **self.range_perc, **self.range_val, stretch=stretch_scale)
        return self

    def hide_labels(self, hide):
        if hide:
            self.image.axis_labels.hide()
            self.image.tick_labels.hide()
        else:
            self.image.axis_labels.show()
            self.image.tick_labels.show()

    def add_scalebar(self, value=None):
        """
        add scalebar to top left corner
        :param value: scallbar size in arcsec, if None it's calculated from image size
        :return: self
        """
        if self.imsize is None and value is None:
            print("Image size and/or bar size is not specified.")
            return self
        if value is None:
            value = parse.round_to_n(self.imsize * 3600. / 2., 2)
        self.image.add_scalebar(value / 3600.)
        self.image.scalebar.set_corner('top left')
        self.image.scalebar.set_font_size(16)
        self.image.scalebar.set_label("{} arcsec".format(value))
        return self

    def add_text_bar(self, add_text, textsize=28):
        self.image.add_label(0.5, 0.08, text=add_text, size=textsize, relative=True, backgroundcolor="white")
        return self

    def add_beam(self):
        bmajval = ibastr.getHeaderItems(self.fitsfile, ['BMAJ'])
        if bmajval['BMAJ'] is None:
            return self
        self.image.add_beam()
        self.image.beam.set_color('black')
        self.image.beam.set_hatch('+')
        return self

    def save_image(self, out_file, use_dpi=150):
        self.image.save(out_file, dpi=use_dpi)
        return self

    # TODO
    #  do stats on data not on cutout
    def image_stats(self, RA=None, DEC=None, xsize=None, ysize=None):
        if RA is None or DEC is None or xsize is None or ysize is None:
            tempimage = self.image
        else:
            tempimage = "/tmp/pltimg_{}.fits".format(strings.id_generator(6))
            res_montage = ibmont.FITScutout(self.fitsfile, tempimage, RA, DEC, xsize, ysize)
            if not res_montage:
                # TODO
                ibastr.FITScutout_astropy(self.fitsfile, tempimage,RA, DEC, xsize,ysize)
        stats = ibastr.imageStats(tempimage)
        os.remove(tempimage)
        return stats

    def check_header(self, fitsfile):
        if ibastr.getHeaderItems(fitsfile, ['NAXIS'], 0)['NAXIS'] >= 2:
            return True
        if ibastr.getHeaderItems(fitsfile, ['NAXIS'], 1)['NAXIS'] >= 2:
            ibastr.shiftto0header(fitsfile, fitsfile, 1)
            return True
        return False

    def add_contours(self, levels, contour_image=None, **kwargs):
        if contour_image is None:
            contour_image = self.fitsfile
        self.image.show_contour(contour_image, levels=levels, **kwargs)

    def add_ellipse(self, RA, DEC, majorax, minorax=None, pa=0, **kwargs):
        if minorax is None:
            minorax = majorax
        self.image.show_ellipses(RA, DEC, majorax, minorax, pa, **kwargs)
