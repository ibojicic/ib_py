from importlib import reload


from ibplotting import PlotImage
reload(PlotImage)


class PlotObject(PlotImage.PlotImage):
    min_cutout = {
        'hst'      : 3,
        'iphas'    : 30,
        'nvss'     : 150,
        'panstarrs': 15,
        'shs'      : 30,
        'sss'      : 30,
        'skymapper': 15
        }

    def __init__(self, image, ra, dec, type='greyscale', working_dir=None):
        super().__init__(image, working_dir)
        self.ra = ra
        self.dec = dec
        self.obj_diam = None
        self.instrument = None
        # self.center_image(self.ra, self.dec)

    @property
    def ra(self):
        return self._ra

    @ra.setter
    def ra(self,ra):
        self._ra = ra

    @property
    def dec(self):
        return self._dec

    @dec.setter
    def dec(self,dec):
        self._dec = dec


    @property
    def obj_diam(self):
        return self._obj_diam

    @obj_diam.setter
    def obj_diam(self,diam):
        self._obj_diam = diam

    @property
    def instrument(self):
        return self._instrument

    @property
    def instrument_cutout(self):
        return self._instrument_cutout

    @instrument.setter
    def instrument(self, instrument):
        if instrument in self.min_cutout.keys():
            self._instrument = instrument
            self._instrument_cutout = self.min_cutout[instrument]
        else:
            self._instrument = None
            self._instrument_cutout = None

