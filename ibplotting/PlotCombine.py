import pandas as pd
import plotnine as gg
import numpy as np
from plotnine import data as ggdata
from matplotlib import gridspec
import matplotlib.pyplot as plt

from ibplotting import ggPlots, PlotBasics


class PlotCombine(ggPlots.ggPlots):

    def __init__(self):
        super().__init__()
        self.sub_left = 0.12
        self.sub_right = 0.98
        self.sub_top = 0.98
        self.sub_bottom = 0.1

    @property
    def sub_left(self):
        return self._sub_left

    @sub_left.setter
    def sub_left(self, left):
        self._sub_left = left

    @property
    def sub_right(self):
        return self._sub_right

    @sub_right.setter
    def sub_right(self, right):
        self._sub_right = right

    @property
    def sub_top(self):
        return self._sub_top

    @sub_top.setter
    def sub_top(self, top):
        self._sub_top = top

    @property
    def sub_bottom(self):
        return self._sub_bottom

    @sub_bottom.setter
    def sub_bottom(self, bottom):
        self._sub_bottom = bottom


    @staticmethod
    def empty_plot():
        return (gg.ggplot() + gg.geom_blank(data=ggdata.diamonds) + gg.theme_void()).draw()

    def vertical_plot_combine_two(self, plot_up, plot_down):
        axis_config = {
            'fontname': self.config['font'],
            'fontsize': self.config['axis_title_size']
        }

        emptyplot = self.empty_plot()

        emptyplot.subplots_adjust(left=self.sub_left, right=self.sub_right, bottom=self.sub_bottom, top=self.sub_top)

        plot_up_out = plot_up.plot_out() + gg.theme(axis_title_x=gg.element_blank(),
                                                    axis_text_x=gg.element_blank(),
                                                    axis_ticks_major_x=gg.element_blank(),
                                                    axis_ticks_minor_x=gg.element_blank()
                                                    )

        plot_down_out = plot_down.plot_out() + gg.theme(panel_grid_major_y=gg.element_blank(),
                                                        panel_grid_minor_y=gg.element_blank()
                                                        )

        gs = gridspec.GridSpec(nrows=2, ncols=1, hspace=0, height_ratios=[0.8, 0.2])
        ax1 = emptyplot.add_subplot(gs[0, 0])
        ax1.set_ylabel(plot_up.config['ylabel'], **axis_config)
        ax2 = emptyplot.add_subplot(gs[1, 0])
        ax2.set_xlabel(plot_down.config['xlabel'], **axis_config)
        ax2.set_ylabel(plot_down.config['ylabel'], **axis_config)

        # Add subplots to the figure
        _ = plot_up_out._draw_using_figure(emptyplot, [ax1])
        _ = plot_down_out._draw_using_figure(emptyplot, [ax2])

        return emptyplot


    def inset_plot_combine_two(self, main_plot, in_plot, inset_conf=[0.65, 0.65, 0.25, 0.25]):
        axis_config = {
            'fontname': self.config['font'],
            'fontsize': self.config['axis_title_size']
        }

        emptyplot = self.empty_plot()

        emptyplot.subplots_adjust(left=self.sub_left, right=self.sub_right, bottom=self.sub_bottom, top=self.sub_top)

        plot_main_out = main_plot.plot_out()

        plot_in_out = in_plot.plot_out()

        gs = gridspec.GridSpec(nrows=1, ncols=1)#, hspace=0, height_ratios=[0.8, 0.2])
        ax1 = emptyplot.add_subplot(gs[0, 0])
        ax1.set_xlabel(main_plot.config['xlabel'], **axis_config)
        ax1.set_ylabel(main_plot.config['ylabel'], **axis_config)
        ax2 = emptyplot.add_axes(inset_conf)
        ax2.set_xlabel(in_plot.config['xlabel'], **axis_config)
        ax2.set_ylabel(in_plot.config['ylabel'], **axis_config)

        # Add subplots to the figure
        _ = plot_main_out._draw_using_figure(emptyplot, [ax1])
        _ = plot_in_out._draw_using_figure(emptyplot, [ax2])

        return emptyplot




    def vertical_plot_combine_three(self, plot_up, plot_mid, plot_down):

        axis_config = {
            'fontname': self.config['font'],
            'fontsize': self.config['axis_title_size']
        }

        emptyplot = self.empty_plot()

        emptyplot.subplots_adjust(left=self.sub_left, right=self.sub_right, bottom=self.sub_bottom, top=self.sub_top)

        plot_up_out = plot_up.plot_out() + gg.theme(axis_title_x=gg.element_blank(),
                                                    axis_text_x=gg.element_blank(),
                                                    axis_ticks_major_x=gg.element_blank(),
                                                    axis_ticks_minor_x=gg.element_blank()
                                                    )

        plot_mid_out = plot_mid.plot_out() + gg.theme(
                                                    axis_title_x=gg.element_blank(),
                                                    axis_text_x=gg.element_blank(),
                                                    axis_ticks_major_x=gg.element_blank(),
                                                    axis_ticks_minor_x=gg.element_blank()
                                                    )

        plot_down_out = plot_down.plot_out() + gg.theme(panel_grid_major_y=gg.element_blank(),
                                                        panel_grid_minor_y=gg.element_blank()
                                                        )

        gs = gridspec.GridSpec(nrows=3, ncols=1, hspace=0, height_ratios=[0.33, 0.33, 0.33])
        ax1 = emptyplot.add_subplot(gs[0, 0])
        ax1.set_ylabel(plot_up.config['ylabel'], **axis_config)
        ax2 = emptyplot.add_subplot(gs[1, 0])
        ax2.set_ylabel(plot_mid.config['ylabel'], **axis_config)
        ax3 = emptyplot.add_subplot(gs[2, 0])
        ax3.set_xlabel(plot_down.config['xlabel'], **axis_config)
        ax3.set_ylabel(plot_down.config['ylabel'], **axis_config)

        # Add subplots to the figure
        _ = plot_up_out._draw_using_figure(emptyplot, [ax1])
        _ = plot_mid_out._draw_using_figure(emptyplot, [ax2])
        _ = plot_down_out._draw_using_figure(emptyplot, [ax3])

        return emptyplot

    @staticmethod
    def save_plot(plot, path, heightim=16, widthim=20, units='cm', family='Courier', dpi=300):
        plot.savefig(fname=path, dpi=dpi)
