from importlib import reload
import pandas as pd
import plotnine as gg
import numpy as np
from ibastro import thermal_sed
import ibastro.physics as ibph
from ibplotting import PlotSed
from ibfitting import ThermalSed

ggPlots = reload(PlotSed)
ThermalSed = reload(ThermalSed)


class PlotFacetSed(PlotSed.PlotSed):

    def __init__(self):
        super().__init__()

    def facet_SEDs(self, fitted_data, models, page=None, nrocol=2, idcol='idPNMain', keeporder=True):

        facetgroup = idcol

        if keeporder:
            facetgroup = 'facetorder'
            self.base_data['facetorder'] = self.base_data.apply(lambda row: self.facetindex(row, page), axis=1)
            fitted_data['facetorder'] = fitted_data.apply(lambda row: self.facetindex(row, page), axis=1)

        radio_data = self.base_data.copy()

        self.set_limits_on_data(radio_data.freq, radio_data.flux, xfactor=(0.5, 2.), yfactor=(1, 2.))

        ids = fitted_data[idcol].unique()

        self.elements = gg.theme(strip_background=gg.element_blank(), strip_text_x=gg.element_blank())

        self.elements = gg.geom_point(gg.aes(x='freq', y='flux'),
                                      size=0.5,
                                      na_rm=True,
                                      show_legend=False)

        self.elements = gg.geom_errorbar(gg.aes(x='freq', ymin='flux_min', ymax='flux_max'),
                                         show_legend=False,
                                         width=.02,
                                         na_rm=True,
                                         size=0.5)

        for curr_id in ids:

            for mdl, plt_pars in models.items():
                if mdl == 'powerlaw':
                    use_pars = ['intersect', 'power']
                    fit_func = ibph.powr_law
                else:
                    use_pars = ['theta', 'freq_0', 'mu', 'Te', 'model']
                    fit_func = thermal_sed.flux_nu

                params = fitted_data.loc[(fitted_data[idcol] == curr_id) &
                                         (fitted_data['model'] == mdl),
                                         use_pars].to_dict('records')[0]


                x = self.freq_points(radio_data.freq)
                idlist = np.repeat(curr_id, len(x))
                if keeporder:
                    facetidlist = np.repeat(fitted_data[fitted_data[idcol] == curr_id][facetgroup], len(x))
                else:
                    facetidlist = np.repeat(curr_id, len(x))

                y = fit_func(x, **params)
                curr_fit = pd.DataFrame(list(zip(idlist, facetidlist, x, y)),
                                        columns=[idcol, facetgroup, 'x', 'y'])

                self.elements = gg.geom_line(curr_fit, gg.aes(x='x', y='y', group=facetgroup),
                                             inherit_aes=False,
                                             **plt_pars
                                             )

        textposy, textposx = self.corner_pos('tl')

        self.elements = gg.geom_text(gg.aes(x=textposx, y=textposy, label='imlabel', group=facetgroup),
                                     hjust='left',
                                     vjust='top',
                                     colour="black",
                                     size=11)
        self.elements = gg.facet_wrap('~{}'.format(facetgroup), ncol=nrocol)

        return self

    def corner_pos(self, crnr: str):
        """
        :param crnr: string of length 2 as combination of: t - top, b - bottom, l - left, r - right
        :return: x and y coordinate
        """
        if self.xlimits is None or self.ylimits is None:
            return None, None
        positions = {
            't': 10 ** self.ylimits[1] * 0.8,
            'b': 10 ** self.ylimits[0] * 1.5,
            'l': 10 ** self.xlimits[0] * 1.3,
            'r': 10 ** self.xlimits[1] * 0.7
        }
        return positions[crnr[0]], positions[crnr[1]]

    @staticmethod
    def facetindex(df, group):
        return np.where(group == df.idPNMain)[0][0]
