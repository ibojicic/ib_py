import ibplotting.markers as mrkr
import plotnine as gg
import pandas as pd
import numpy.random as nprand
from importlib import reload

reload(mrkr)

data = pd.DataFrame({'x':nprand.rand(10), 'y':nprand.rand(10)})
test_plot = gg.ggplot(data) + gg.geom_point(gg.aes(x='x', y='y'), shape=mrkr.arc(), size=50)