import click
from ibastro import fitslibs

@click.command()
@click.argument('infile', nargs=1)
@click.argument('reffile', nargs=1)
@click.argument('outfile', nargs=1)

#@click.option('coords', nargs=2)
#@click.option('--inpSep', '-e', default=',')

def cli(infile,reffile,outfile):
    try:
        fitslibs.askap_delete_extra_header(infile)
        fitslibs.askap_delete_extra_header(reffile)
    except:
        print('no extra header items')

    try:
        fitslibs.delete_extra_header(infile)
        fitslibs.delete_extra_header(reffile)
    except:
        print('no extra header items')

    fitslibs.reprojectOnTemplate(infile,outfile,reffile)