
import click
from ibastro import fitslibs

@click.command()
@click.argument('leftimage', nargs=1)
@click.argument('rightimage', nargs=1)
@click.argument('outfile', nargs=1)

#@click.option('coords', nargs=2)
#@click.option('--inpSep', '-e', default=',')

def cli(leftimage,rightimage,outfile):
    fitslibs.doSubtraction(leftimage=leftimage,rightmage=rightimage,subimage=outfile,ratio=1)