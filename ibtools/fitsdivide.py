
import click
from ibastro import fitslibs

@click.command()
@click.argument('dividend', nargs=1)
@click.argument('divisor', nargs=1)
@click.argument('outfile', nargs=1)

#@click.option('coords', nargs=2)
#@click.option('--inpSep', '-e', default=',')

def cli(dividend, divisor, outfile):
    fitslibs.quotientImage(image_list=[dividend,divisor], out_image=outfile)
