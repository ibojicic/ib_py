import click
from ibastro import coordtrans, montages, fitslibs
from ibcommon import strings


@click.command()
@click.argument('infile', nargs=1)
@click.argument('outfile', nargs=1)
@click.argument('center_x', nargs=1)
@click.argument('center_y', nargs=1)
@click.argument('size_x', nargs=1)
@click.argument('size_y', nargs=1)
@click.option('--galactic', '-g', is_flag=True, default=False, help="Use galactic coords")
@click.option('--header_no', '-n', default=0, help="Use header number")
def cli(infile, outfile, center_x, center_y, size_x, size_y, galactic, header_no):
    '''
    Make coutout from a fits image \n

    :param infile: Input file full path \n
    :param outfile: Output file full path \n
    :param center_x: Center X position (RAJ2000/Glon) in deg \n
    :param center_y: Center Y position (DEJ2000/Glat) in deg \n
    :param size_x: Size in X in arcsec \n
    :param size_y: Size in Y in arcsec \n
    :param galactic: Use galactic coords \n
    :param header_no: Use header no \n
    '''

    if header_no != 0:
        tmpimage = "{}_temp.fits".format(strings.id_generator())
        fitslibs.shiftto0header(infile, tmpimage, header_no)
        infile = tmpimage

    size_x = float(size_x)
    size_y = float(size_y)

    if galactic:
        center_x, center_y = coordtrans.gal2radec(center_x, center_y)

    montages.FITScutout(
        inImage=infile,
        outImage=outfile,
        Xcoord=center_x,
        Ycoord=center_y,
        box_x_coord=size_x,
        box_y_coord=size_y
    )
