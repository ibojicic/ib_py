import sys

import ibplotting.photometry_plots as cb_plots
from ibphotometry_old.PhotInOutClass import PhotInOut
from ibphotometry_old.AegeanObservationClass import AegeanObservation
from ibphotometry_old import AegeanPhotClass

from pprint import pprint

def main():

    method = 'aegean'

    phot_inout = PhotInOut(method)

    for curr_object in phot_inout.targets_dict:

        obs = AegeanObservation(method, curr_object, phot_inout.args_dict())
        photo = AegeanPhotClass.AegeanPhot(obs)
        photo.photometry()
        photo.make_residual()
        photo.set_db_results()
        phot_inout.write_to_photometry(photo.dbresults)
        cb_plots.make_fit_images(photo)





