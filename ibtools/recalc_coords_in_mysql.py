import pandas as pd
import click
from astropy import units as u
from astropy.coordinates import SkyCoord

from peeweedbmodels import dbMainPNData


@click.command()
@click.argument('db', nargs=1)
@click.argument('tbl', nargs=1)
@click.argument('intype', nargs=1)
@click.argument('outtype', nargs=1)
@click.argument('inpx', nargs=1)
@click.argument('inpy', nargs=1)
@click.argument('outx', nargs=1)
@click.argument('outy', nargs=1)
@click.argument('idcol', nargs=1)

def cli(db, tbl, intype, outtype, inpx, inpy, outx, outy, idcol):
    database = eval(db)
    table = getattr(database, tbl)

    out_col_X = getattr(table,outx)
    out_col_Y = getattr(table,outy)
    out_col_id = getattr(table,idcol)

    query = table.select()
    df_results = pd.DataFrame(list(query.dicts()))

    xinput = df_results[inpx]
    yinput = df_results[inpy]
    input_coords = SkyCoord(xinput, yinput, frame='fk4', unit=(u.hourangle, u.deg))
    output_coords = input_coords.fk5
    out_data = pd.DataFrame({
        'id' : list(df_results[idcol]),
        'DRAJ2000' : list(output_coords.ra.deg),
        'DDECJ2000': list(output_coords.dec.deg)}
            )

    for index, row in out_data.iterrows():
        print(row['id'],row['DRAJ2000'],row['DDECJ2000'])
        query = table.update(draj2000 = row['DRAJ2000'], ddecj2000 = row['DDECJ2000']).where(out_col_id == int(row['id']))
        query.execute()

