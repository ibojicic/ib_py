import sys

import ibplotting.photometry_plots as cb_plots

from ibphotometry_old.CasaObservationClass import CasaObservation
from ibphotometry_old.CasaPhotometryClass import CasaPhotometry
from ibphotometry_old.PhotInOutClass import PhotInOut

from pprint import pprint

def main():

    method = 'casa'
    
    phot_inout = PhotInOut(method)

    for curr_object in phot_inout.targets_dict:

        obs = CasaObservation(method, curr_object, phot_inout.args_dict())

        photo = CasaPhotometry(obs)
        print("Working on id={}".format(curr_object['name']))

        photo.gaussfit()

        photo.set_db_results()

        pprint(photo.fitresults)
        pprint(photo.bckgstats)

        photo.make_residual()
        cb_plots.make_fit_images(photo)

        phot_inout.write_to_photometry(photo.dbresults)
        print("finished observations")



