import click
from ibastro import fitslibs,coordtrans,montages
import numpy as np
import sys

@click.command()
@click.argument('infile', nargs=1)
@click.argument('outfile_prefix', nargs=1)
@click.argument('center_x', nargs=1, type = float)
@click.argument('center_y', nargs=1, type = float)
@click.argument('size_x', nargs=1, type = float)
@click.argument('size_y', nargs=1, type = float)
@click.argument('overlap', nargs=1, type = float)
@click.option('--galactic', '-g', is_flag=True, default=False)
@click.option('--limits_x', '-h', nargs = 2, type = float, default = (0.,360.))
@click.option('--limits_y', '-v', nargs = 2, type = float, default = (-90,90))


def cli(infile, outfile_prefix, center_x, center_y, size_x, size_y, overlap, galactic, limits_x, limits_y):

    for cen_y in np.arange(center_y%size_y + limits_y[0],size_y + limits_y[1],size_y):
        for cen_x in np.arange(center_x % size_x + limits_x[0],size_x +  limits_x[1], size_x):

            outfile = "{}_x{}_y{}.fits".format(outfile_prefix,cen_x,cen_y)
            try:
                do_cutout(infile,outfile,cen_x,cen_y,(size_x+overlap)*3600,(size_y+overlap)*3600,galactic)
            except:
                print('not covered')


def do_cutout(infile, outfile, center_x, center_y, size_x, size_y, galactic):

    if galactic:
        center_x, center_y = coordtrans.gal2radec(center_x, center_y)

    input_pars = {
        'inImage':infile,
        'outImage':outfile,
        'Xcoord':center_x,
        'Ycoord':center_y,
        'box_x_coord':size_x,
        'box_y_coord':size_y}

    try:
        try_montage = montages.FITScutout(**input_pars)
    except:
        print("no coverage")