import click
import os

os.environ['MPLCONFIGDIR'] = '/tmp'
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

plt.ioff()

import aplpy

from ibplotting.plotlibs import markCross

@click.command()
@click.argument('infiles', nargs=3)
@click.argument('outfile', nargs=1)
@click.argument('outfiletype', nargs=1)
@click.option('contourImage', '-c', nargs=1)
@click.option('contourLevels', '-l')
@click.option('strechFnc', '-s', default = "linear")
@click.option('strechLvl', '-v', nargs=2, default=(1,99))

# @click.option('--inpSep', '-e', default=',')

def cli(infiles, outfile, outfiletype, contourLevels, strechLvl = (1,99), contourImage = None, strechFnc = "linear"):
    InImages = {
        "R": infiles[0],
        "G": infiles[1],
        "B": infiles[2]
    }

    OutImages = {
        "main": {"filename": "{}.{}".format(outfile,outfiletype)}
    }

    perc_levels = {
        "R": {"min": strechLvl[0], "max": strechLvl[1]},
        "G": {"min": strechLvl[0], "max": strechLvl[1]},
        "B": {"min": strechLvl[0], "max": strechLvl[1]}
    }


    if contourLevels is not None:
        contourLevels = contourLevels.split(",")

    makeImage(
        addGrid=False,
        InImages=InImages,
        outImages=OutImages,
        rgbCube_name=outfile,
        tempRGB="tempRGB",
        imlevels="p",
        useSys="GAL",
        perc_levels=perc_levels,
        contourImage=contourImage,
        contourLevels=contourLevels,
        strechFnc=strechFnc
    )


def makeImage(rgbCube_name,
              perc_levels,
              InImages,
              outImages,
              tempRGB,
              addGrid=False,
              imlevels='v',
              doNorth=False,
              useSys="EQUJ",
              contourImage = None,
              contourLevels = None,
              strechFnc = "linear",
              addCrosses = None,
              **kwargs):

    fig = plt.figure(tight_layout=True)
    fig.subplots_adjust(bottom=0.1, left=0.1, top=0.12, right=0.12)

    aplpy.make_rgb_cube([InImages['R'],
                         InImages['G'],
                         InImages['B']],
                        rgbCube_name + ".fits",
                        system=useSys,
                        north=doNorth)

    streches = {
        "stretch_r":strechFnc,
        "stretch_g":strechFnc,
        "stretch_b":strechFnc
    }

    if imlevels == 'v':
        aplpy.make_rgb_image(rgbCube_name + ".fits", "{}.{}".format(tempRGB, "jpg"),
                             vmax_r=float(perc_levels['R']['max']),
                             vmax_g=float(perc_levels['G']['max']),
                             vmax_b=float(perc_levels['B']['max']),
                             vmin_r=float(perc_levels['R']['min']),
                             vmin_g=float(perc_levels['G']['min']),
                             vmin_b=float(perc_levels['B']['min']),
                             embed_avm_tags=False,
                             **streches)
    else:
        aplpy.make_rgb_image(rgbCube_name + ".fits", "{}.{}".format(tempRGB, "jpg"),
                             pmax_r=float(perc_levels['R']['max']),
                             pmax_g=float(perc_levels['G']['max']),
                             pmax_b=float(perc_levels['B']['max']),
                             pmin_r=float(perc_levels['R']['min']),
                             pmin_g=float(perc_levels['G']['min']),
                             pmin_b=float(perc_levels['B']['min']),
                             embed_avm_tags=False,
                             **streches)

    mainCompos = aplpy.FITSFigure(rgbCube_name + "_2d.fits", figure=fig)
    mainCompos.show_rgb("{}.{}".format(tempRGB, "jpg"))

    mainCompos.tick_labels.set_xformat('dd')
    mainCompos.tick_labels.set_yformat('dd')

    mainCompos.axis_labels.show()
    mainCompos.tick_labels.show()

    if addGrid: mainCompos.add_grid()

    if addCrosses is not None:
        symSize = 5000.
        crosspath = markCross(250.)
        for crossdata in addCrosses:
            mainCompos.show_markers(crossdata[0],
                                    crossdata[1],
                                    marker=crosspath,
                                    s=symSize,
                                    lw=1.,
                                    edgecolor=crossdata[3]
                                    )
            if crossdata[2] is not None:
                mainCompos.add_label(crossdata[0] + 250. / 3600.,
                                     crossdata[1] + 250. / 3600.,
                                     text=crossdata[2],
                                     size=16
                                     )


    if contourImage is not None and contourLevels is not None:
        mainCompos.show_contour(contourImage,levels=contourLevels,linewidths=0.5,smooth=1)


    fig.savefig(outImages['main']['filename'], dpi=360)

    plt.close()

    return True


