import sys

import ibplotting.photometry_plots as cb_plots

from ibphotometry_old.BlobObservationClass import BlobObservation
from ibphotometry_old.BlobPhotometryClass import BlobPhotometry
from ibphotometry_old.PhotInOutClass import PhotInOut

from pprint import pprint

def main():

    method = 'blob'

    phot_inout = PhotInOut(method)

    for curr_object in phot_inout.targets_dict:

        obs = BlobObservation(method, curr_object, phot_inout.args_dict())

        photo = BlobPhotometry(obs)
        print("Working on id={}".format(curr_object['name']))

        photo.blobit()

        photo.set_db_results()

        cb_plots.make_blob_images(photo)


        phot_inout.write_to_photometry(photo.dbresults)
        print("finished observations")
        print(photo.dbresults)



