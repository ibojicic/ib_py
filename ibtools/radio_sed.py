import click
import os
import pandas as pd
from ibfitting import PowerLaw
from pprint import pprint

os.environ['MPLCONFIGDIR'] = '/tmp'
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

plt.ioff()

'''
input file:
required:
id, freq(GHz), flux(mJy)

optional:
err_flux(mJy), reference(string), instrument(string), flag_flux(char:<,>,:)
'''

@click.command()
@click.argument('infile', nargs=1)
@click.argument('outfile', nargs=1)
# @click.option('contourImage', '-c', nargs=1)

def cli(infile, outfile, ):
    indata = pd.read_csv(infile)
    fit_params = {
        "model"  : 'powerlaw',
        "fittype": 'curvefit'
    }
    linfitter = PowerLaw.PowerLaw(fit_params)
    linfitter.set_data_pandas(indata, xcol='freq', ycol='flux', ycol_err='err_flux')
    linfitter.run_fit()


    pprint(linfitter.full_results)
