
import click
from ibastro import fitslibs

@click.command()
@click.argument('images', nargs=1)
@click.argument('freqs', nargs=1)
@click.argument('outfile', nargs=1)

#@click.option('coords', nargs=2)
#@click.option('--inpSep', '-e', default=',')

def cli(images, freqs, outfile):
    images = images.split(",")
    freqs = [float(i) for i in freqs.split(",")]
    fitslibs.spindexImage(images,freqs,outfile)
